FROM rust:1.51.0-buster

SHELL ["/bin/bash", "-c"]

RUN apt-get update && \
    apt-get install -y zip sudo

RUN useradd -m sar && \
    echo "sar ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
USER sar
WORKDIR /home/sar

RUN mkdir /home/sar/bin
ENV PATH /home/sar/bin:$PATH

# Install Z3
RUN git clone https://github.com/Z3Prover/z3.git && \
    cd z3 && \
    git checkout c0e74f946b28a83f94becab56a1565c49cb37141 && \
    python3 scripts/mk_make.py && \
    cd build && \
    make && \
    sudo make install && \
    ln -s /usr/bin/z3 /home/sar/bin/z3.latest
RUN wget https://github.com/Z3Prover/z3/releases/download/z3-4.8.7/z3-4.8.7-x64-ubuntu-16.04.zip && \
    unzip -p z3-4.8.7-x64-ubuntu-16.04.zip z3-4.8.7-x64-ubuntu-16.04/bin/z3 > /home/sar/bin/z3 && \
    chmod +x /home/sar/bin/z3 && \
    rm z3-4.8.7-x64-ubuntu-16.04.zip

# Install CVC4
RUN wget -O /home/sar/bin/cvc4 https://github.com/CVC4/CVC4/releases/download/1.8/cvc4-1.8-x86_64-linux-opt && \
    chmod +x /home/sar/bin/cvc4

# Install HoIce
RUN cargo install --git https://github.com/hopv/hoice --rev baf8050a023514c3cf638df126269e7b85860217

# Install Eldarica
RUN curl -s "https://get.sdkman.io" | bash && \
    source "/home/sar/.sdkman/bin/sdkman-init.sh" && \
    sdk install java 16.0.0.hs-adpt
RUN wget https://github.com/uuverifiers/eldarica/releases/download/v2.0.6/eldarica-bin-2.0.6.zip && \
    unzip eldarica-bin-2.0.6.zip && \
    ln -s /home/sar/eldarica/eld /home/sar/bin && \
    rm eldarica-bin-2.0.6.zip

# Install sarsmt
COPY --chown=sar:sar ./ /home/sar/sarsmt
RUN cd /home/sar/sarsmt && \
    cargo build --release
RUN ln -s /home/sar/sarsmt/reproduction.md /home/sar/README.md
