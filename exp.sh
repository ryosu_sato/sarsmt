#!/bin/bash

SCRIPT_DIR=$(dirname ${0})

### Executables
EXE="$SCRIPT_DIR/target/release/sarsmt"
Z3_REC="z3.latest"
CVC4="cvc4 --fmf-fun"
CVC4_ASSERT="cvc4 --quant-ind --quant-cf --conjecture-gen --conjecture-gen-per-round=3 --full-saturate-quant"

### Benchmarks
ROOT="$SCRIPT_DIR/benchmark"
DIRS="IsaPlanner SAR_SMT CHC"

### Settings of experiments
OUT="exp.csv"
if [ -n "$1" ]; then
   LIMIT=$1
else
    LIMIT=60
fi

if [ ! -x $EXE ]; then
    echo NOT FOUND: $EXE
    exit 1
fi

echo "# Experiments with"
if git rev-parse --git-dir > /dev/null 2>&1; then
   echo -n "COMMIT: "
   git rev-parse --short HEAD
fi
echo "LIMIT: $LIMIT seconds"
echo

echo "# Versions of tools"
echo -n "SAR: "
z3 --version
echo -n "SAR: "
hoice --version
echo -n "     with "
z3 --version
echo -n "SAR: "
eld -h | head -1
echo -n "REC: "
$Z3_REC --version
echo -n "REC: "
$CVC4 --version | head -1
echo


echo "======================================================================================================================================================================================================================================================="
echo "input,expect,Z3 time,Z3 result,HoIce time,HoIce result,Eldarica time,Eldarica result,input-rec,expect-rec,Z3-rec time,Z3-rec result,CVC4-rec time,CVC4-rec result,input-assert,expect-assert,Z3-assert time,Z3-assert result,CVC4-assert time,CVC4-assert result" | tee $OUT

function run () {
    START=`date +%s%3N`
    RESULT=`timeout $LIMIT $1 $2 2> /dev/null`
    STATUS="$?"
    if [ "$STATUS" = 124 ]; then
        echo -n ",-,Timeout" | tee -a $OUT
    elif [ "$STATUS" = 137 ]; then
        echo -n ",-,Out of Memory" | tee -a $OUT
    else
        RESULT=`echo "$RESULT" | tail -1`
        END=`date +%s%3N`
        TIME=`echo $(($END - $START))`
        echo -n ",$TIME,$RESULT" | tee -a $OUT
    fi
}

for i in $(cd $ROOT; find $DIRS -name '*.smt2' | sort); do
    FILE=$ROOT/$i
    EXPECT=`grep ';;; EXPECT:' $FILE | sed 's/^[ \t]*;;;[ \t]*EXPECT:[ \t]*//'`
    echo -n "$FILE,$EXPECT" | tee -a $OUT
    run "$EXE z3" $FILE
    run "$EXE hoice" $FILE
    run "$EXE eld" $FILE
    FILE_REC=${ROOT}_rec/${i#$ROOT}
    EXPECT_REC=`grep ';;; EXPECT:' $FILE_REC | sed 's/^[ \t]*;;;[ \t]*EXPECT:[ \t]*//'`
    echo -n ",$FILE_REC,$EXPECT_REC" | tee -a $OUT
    run "$Z3_REC" $FILE_REC
    run "$CVC4" $FILE_REC
    FILE_ASSERT=${ROOT}_assert/${i#$ROOT}
    EXPECT_ASSERT=`grep ';;; EXPECT:' $FILE_ASSERT | sed 's/^[ \t]*;;;[ \t]*EXPECT:[ \t]*//'`
    echo -n ",$FILE_ASSERT,$EXPECT_ASSERT" | tee -a $OUT
    run "$Z3_REC" $FILE_ASSERT
    run "$CVC4_ASSERT" $FILE_ASSERT
    echo | tee -a $OUT
done
