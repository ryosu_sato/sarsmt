#!/bin/bash


cargo build --release


echo "graph: "

list="$(find examples -maxdepth 1 | sed 1d)"
num=$(echo "$list" | wc -w)
count=0

echo "examples"
for dir in $(ls examples)
do
    ((count++))
    if [ $count -eq $num ]
    then
        head="└── "
        next="    "
    else
        head="├── "
        next="│   "
    fi

    echo "$head"$dir

    listt="$(find examples/$dir -maxdepth 1 | sed 1d)"
    numm=$(echo "$listt" | wc -w)
    countt=0

    for path in $(ls examples/$dir)
    do
        ((countt++))
        if [ $countt -eq $numm ]
        then
            headd="└── "
        else
            headd="├── "
        fi

        echo "$next$headd"$path

        target/release/sarsmt graph examples/$dir/$path
    done
done

echo ""





echo "z3: "

list="$(find examples -maxdepth 1 | sed 1d)"
num=$(echo "$list" | wc -w)
count=0

echo "examples"
for dir in $(ls examples)
do
    ((count++))
    if [ $count -eq $num ]
    then
        head="└── "
        next="    "
    else
        head="├── "
        next="│   "
    fi

    echo "$head"$dir

    listt="$(find examples/$dir -maxdepth 1 | sed 1d)"
    numm=$(echo "$listt" | wc -w)
    countt=0

    for path in $(ls examples/$dir)
    do
        ((countt++))
        if [ $countt -eq $numm ]
        then
            headd="└── "
            nextt="    "
        else
            headd="├── "
            nextt="│   "
        fi

        echo "$next$headd"$path

        target/release/sarsmt z3 examples/$dir/$path | while read line
        do
            if [ $(echo "$line" | grep clause | wc -w) -gt 0 ] ; then
                headdd="├── "
            else
                headdd="└ "
            fi

            echo "$next$nextt$headdd"$line
        done
    done
done

echo ""





echo "hoice: "

list="$(find examples -maxdepth 1 | sed 1d)"
num=$(echo "$list" | wc -w)
count=0

echo "examples"
for dir in $(ls examples)
do
    ((count++))
    if [ $count -eq $num ]
    then
        head="└── "
        next="    "
    else
        head="├── "
        next="│   "
    fi

    echo "$head"$dir

    listt="$(find examples/$dir -maxdepth 1 | sed 1d)"
    numm=$(echo "$listt" | wc -w)
    countt=0

    for path in $(ls examples/$dir)
    do
        ((countt++))
        if [ $countt -eq $numm ]
        then
            headd="└── "
            nextt="    "
        else
            headd="├── "
            nextt="│   "
        fi

        echo "$next$headd"$path

        target/release/sarsmt hoice examples/$dir/$path | while read line
        do
            if [ $(echo "$line" | grep clause | wc -w) -gt 0 ] ; then
                headdd="├── "
            else
                headdd="└ "
            fi

            echo "$next$nextt$headdd"$line
        done
    done
done






echo ""
echo "eld: "

list="$(find examples -maxdepth 1 | sed 1d)"
num=$(echo "$list" | wc -w)
count=0

echo "examples"
for dir in $(ls examples)
do
    ((count++))
    if [ $count -eq $num ]
    then
        head="└── "
        next="    "
    else
        head="├── "
        next="│   "
    fi

    echo "$head"$dir

    listt="$(find examples/$dir -maxdepth 1 | sed 1d)"
    numm=$(echo "$listt" | wc -w)
    countt=0

    for path in $(ls examples/$dir)
    do
        ((countt++))
        if [ $countt -eq $numm ]
        then
            headd="└── "
            nextt="    "
        else
            headd="├── "
            nextt="│   "
        fi

        echo "$next$headd"$path

        target/release/sarsmt eld examples/$dir/$path | while read line
        do
            if [ $(echo "$line" | grep clause | wc -w) -gt 0 ] ; then
                headdd="├── "
            else
                headdd="└ "
            fi

            echo "$next$nextt$headdd"$line
        done
    done
done

echo ""
