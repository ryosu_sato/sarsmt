#!/bin/bash

if [ -n "$1" ]; then
    FILE=$1
else
    FILE=exp.csv
fi

echo ',Benchmark,IsaPlanner,SAR_SMT,CHC,All'

echo -n ',#Instances,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
}
{
  if($1 ~ /IsaPlanner/) {
    if($2 == "sat")
      ip_valid += 1
    else if ($2 == "unsat")
      ip_invalid += 1
  } else if($1 ~ /SAR_SMT/) {
    if($2 == "sat")
      smt_valid += 1
    else if ($2 == "unsat")
      smt_invalid += 1
  } else if($1 ~ /CHC/) {
    if($2 == "sat")
      chc_valid += 1
    else if ($2 == "unsat")
      chc_invalid += 1
  }
  if($2 == "sat")
    all_valid += 1
  else if ($2 == "unsat")
    all_invalid += 1
}
END {
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid ")"
}'

echo -n 'Ours-Spacer,#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($4 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($4 == "sat")
        ip_valid += 1
      else if ($4 == "unsat")
        ip_invalid += 1
      if($6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($4 == "sat")
        smt_valid += 1
      else if ($4 == "unsat")
        smt_invalid += 1
      if($6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($4 == "sat")
        chc_valid += 1
      else if ($4 == "unsat")
        chc_invalid += 1
      if($6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        chc_unique += 1
    }
    if($4 == "sat")
      all_valid += 1
    else if ($4 == "unsat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'Ours-Spacer,Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($4 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $3
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $3
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $3
    }
    all_num += 1
    all_time += $3
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'Ours-HoIce,#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($6 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($6 == "sat")
        ip_valid += 1
      else if ($6 == "unsat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($6 == "sat")
        smt_valid += 1
      else if ($6 == "unsat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($6 == "sat")
        chc_valid += 1
      else if ($6 == "unsat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        chc_unique += 1
    }
    if($6 == "sat")
      all_valid += 1
    else if ($6 == "unsat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'Ours-HoIce,Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($6 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $5
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $5
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $5
    }
    all_num += 1
    all_time += $5
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'Ours-Eldarica,#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($8 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($8 == "sat")
        ip_valid += 1
      else if ($8 == "unsat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($8 == "sat")
        smt_valid += 1
      else if ($8 == "unsat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($8 == "sat")
        chc_valid += 1
      else if ($8 == "unsat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        chc_unique += 1
    }
    if($8 == "sat")
      all_valid += 1
    else if ($8 == "unsat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'Ours-Eldarica,Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($8 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $7
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $7
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $7
    }
    all_num += 1
    all_time += $7
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'Z3 (rec),#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($12 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($12 == "unsat")
        ip_valid += 1
      else if ($12 == "sat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($12 == "unsat")
        smt_valid += 1
      else if ($12 == "sat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($12 == "unsat")
        chc_valid += 1
      else if ($12 == "sat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        chc_unique += 1
    }
    if($12 == "unsat")
      all_valid += 1
    else if ($12 == "sat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'Z3 (rec),Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($12 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $11
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $11
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $11
    }
    all_num += 1
    all_time += $11
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'CVC4 (rec),#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($14 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($14 == "unsat")
        ip_valid += 1
      else if ($14 == "sat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($14 == "unsat")
        smt_valid += 1
      else if ($14 == "sat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($14 == "unsat")
        chc_valid += 1
      else if ($14 == "sat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $18 !~ /sat$/ && $20 !~ /sat$/)
        chc_unique += 1
    }
    if($14 == "unsat")
      all_valid += 1
    else if ($14 == "sat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'CVC4 (rec),Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($14 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $13
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $13
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $13
    }
    all_num += 1
    all_time += $13
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'Z3 (assert),#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($18 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($18 == "unsat")
        ip_valid += 1
      else if ($18 == "sat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($18 == "unsat")
        smt_valid += 1
      else if ($18 == "sat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($18 == "unsat")
        chc_valid += 1
      else if ($18 == "sat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/)
        chc_unique += 1
    }
    if($18 == "unsat")
      all_valid += 1
    else if ($18 == "sat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'Z3 (assert),Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($18 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $17
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $17
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $17
    }
    all_num += 1
    all_time += $17
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'



echo -n 'CVC4 (assert),#Solved,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_valid = 0
  ip_invalid = 0
  smt_valid = 0
  smt_invalid = 0
  chc_valid = 0
  chc_invalid = 0
  all_valid = 0
  all_invalid = 0
  ip_unique = 0
  smt_unique = 0
  chc_unique = 0
}
{
  if($20 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      if($20 == "unsat")
        ip_valid += 1
      else if ($20 == "sat")
        ip_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/)
        ip_unique += 1
    } else if($1 ~ /SAR_SMT/) {
      if($20 == "unsat")
        smt_valid += 1
      else if ($20 == "sat")
        smt_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/)
        smt_unique += 1
    } else if($1 ~ /CHC/) {
      if($20 == "unsat")
        chc_valid += 1
      else if ($20 == "sat")
        chc_invalid += 1
      if($4 !~ /sat$/ && $6 !~ /sat$/ && $8 !~ /sat$/ && $12 !~ /sat$/ && $14 !~ /sat$/ && $18 !~ /sat$/)
        chc_unique += 1
    }
    if($20 == "unsat")
      all_valid += 1
    else if ($20 == "sat")
      all_invalid += 1
  }
}
END {
  all_unique = ip_unique + smt_unique + chc_unique
  print ip_valid +ip_invalid  "(" ip_valid  "/" ip_invalid  "/" ip_unique  ")," \
        smt_valid+smt_invalid "(" smt_valid "/" smt_invalid "/" smt_unique ")," \
        chc_valid+chc_invalid "(" chc_valid "/" chc_invalid "/" chc_unique ")," \
        all_valid+all_invalid "(" all_valid "/" all_invalid "/" all_unique ")"
}'
echo -n 'CVC4 (assert),Average time,'
tail -n +2 $FILE | awk -F, '
BEGIN {
  ip_num = 0
  ip_time = 0
  smt_num = 0
  smt_time = 0
  chc_num = 0
  chc_time = 0
  all_num = 0
  all_time = 0
}
{
  if($20 ~ /sat$/) {
    if($1 ~ /IsaPlanner/) {
      ip_num += 1
      ip_time += $19
    } else if($1 ~ /SAR_SMT/) {
      smt_num += 1
      smt_time += $19
    } else if($1 ~ /CHC/) {
      chc_num += 1
      chc_time += $19
    }
    all_num += 1
    all_time += $19
  }
}
END {
  print ip_time  / ip_num "," \
        smt_time / smt_num "," \
        chc_time / chc_num "," \
        all_time / all_num
}'
