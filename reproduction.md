# Instructions for reproducing the experimental results of the paper

## Running Benchmark Script

Run the following script for the benchmark sets:

  $ sarsmt/exp.sh
  (Takes about 5 hours)

The results are printed on the standard output and also outputed to the file "./exp.csv"
This results correspond to Tables 4 and 5 in the paper.
The default timeout is set to 60 seconds.
If you prefer to use another timeout, pass the timeout in seconds to the script as an argument like:

  $ sarsmt/exp.sh 1
  (Takes about 10 minutes)

The summary of the results represented in Table 1 is calculated by the following script that refers to "./exp.csv".

  $ sarsmt/summary.sh


## Implementation

Our tool, `SAR_SMT`, is implemented in [Rust](https://www.rust-lang.org/).
The source code and README.md of the tool is located in /home/sar/sarsmt.


## Benchmarks

All the benchmarks are located in `/home/sar/sarsmt/benchmark*/`.
Input files for our tool is located in `/home/sar/sarsmt/benchmark/`,
and the input files for the other SMT solvers are located in `/home/sar/sarsmt/benchmark_rec/` and `/home/sar/sarsmt/benchmark_assert/`.
If you want to add a new instance `/home/sar/sarsmt/benchmark/SAR_SMT/XXX.smt2`,
you have to add `/home/sar/sarsmt/benchmark_rec/SAR_SMT/XXX.smt2` and `/home/sar/sarsmt/benchmark_assert/SAR_SMT/XXX.smt2` too.
