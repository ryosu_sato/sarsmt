;;; INFO: prefix (x::xs) ys => ys <> []
;;; EXPECT: sat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)
(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (|prefix| (insert x0 X0) Y0)
            (not (= nil Y0))
        )
    )
)

(check-sat)
