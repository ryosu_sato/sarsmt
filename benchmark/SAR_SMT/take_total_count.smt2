;;; INFO: ys = take_total n xs => count x xs <= count x ys
;;; EXPECT: sat

(set-logic HORN)

(define-fun |count|
    ((c0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

; take_total n0 Y0 = X0
(define-fun |take_total|
    ((n0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (<= l_0 0)
                                  (pad l_2)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (or (and (<= l_0 0)
                                      (not (pad l_2)))
                                 (and (not (pad l_2))
                                      (pad l_3))))
                    (q_1 q_1 true)
                )
            )
        )
    )
)

(assert
    (forall ((c0 Int) (c1 Int) (n0 Int) (x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|take_total| n0 X0 Y0)
                (|count| c0 x0 X0)
                (|count| c1 x0 Y0)
            )
            (<= c0 c1)
        )
    )
)

(check-sat)
