;;; INFO: sorted (x::xs) => x <= nth xs i
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |nth|
    ((i0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (= l_2 x_0)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1 q_2))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (not (= l_2 x_0))))
                    (q_1 q_1 true)
                    (q_0 q_2 (< l_0 0))
                    (q_2 q_2 true)
                )
            )
        )
    )
)

(assert
    (forall ((i0 Int) (x0 Int) (y0 Int) (X0 (List Int)))
        (=>
            (and
                (|sorted| (insert x0 X0))
                (|nth| i0 y0 X0)
            )
            (<= x0 y0)
        )
    )
)

(check-sat)
