;;; INFO: length [x;y] >= 3
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (x0 Int) (x1 Int))
        (=>
            (|length| n0 (insert x0 (insert x1 nil)))
            (>= n0 3)
        )
    )
)

(check-sat)
