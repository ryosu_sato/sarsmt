;;; INFO: forall ((<=) x) /\ fold (+) 0 xs >= x * length xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(define-fun |sumfold|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(define-fun |gte|
    ((x0 Int) (X0 (List Int))) Bool
    (
        (
            (x0)
            (X0)
        )
        (
            (q_0)
            (q_0)
        )
        (
            (q_0 q_0 (>= l_0 x_0))
        )
    )
)

(assert
    (forall ((x0 Int) (n0 Int) (s0 Int) (X0 (List Int)))
        (=>
            (and
                (|gte| x0 X0)
                (|length| n0 X0)
                (|sumfold| s0 X0)
            )
            (>= s0 (* x0 n0))
        )
    )
)

(check-sat)
; unsolvable
