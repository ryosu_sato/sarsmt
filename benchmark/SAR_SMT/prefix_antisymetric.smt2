;;; INFO: prefix xs ys /\ prefix ys xs => xs = ys
;;; EXPECT: sat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(assert
    (forall ((X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)))
        (=>
            (and
                (|prefix| X0 Y0)
                (|prefix| Y0 X0)
            )
            (= X0 Y0)
        )
    )
)

(check-sat)
