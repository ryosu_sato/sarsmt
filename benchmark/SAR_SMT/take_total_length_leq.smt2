;;; INFO: xs = take_total n ys /\ n1 = length xs /\ n2 = length ys => n1 <= n2
;;; EXPECT: sat

(set-logic HORN)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

; take_total n0 Y0 = X0
(define-fun |take_total|
    ((n0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (<= l_0 0)
                                  (pad l_2)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (or (and (<= l_0 0)
                                      (not (pad l_2)))
                                 (and (not (pad l_2))
                                      (pad l_3))))
                    (q_1 q_1 true)
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (n1 Int) (n2 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|take_total| n0 X0 Y0)
                (|length| n1 X0)
                (|length| n2 Y0)
            )
            (<= n1 n2)
        )
    )
)

(check-sat)
