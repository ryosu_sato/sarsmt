;;; INFO: n > 0 /\ ys = take (n-1) xs = x::ys = take n (x::xs)
;;; EXPECT: sat

(set-logic HORN)

; take n0 Y0 = X0
(define-fun |take|
    ((n0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (= l_0 0)
                                  (pad l_2)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (= l_0 0)
                                  (not (pad l_2))))
                    (q_1 q_1 true)
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (x0 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)))
        (=>
            (and
                (> n0 0)
                (|take| (- n0 1) X0 Y0)
            )
            (|take| n0 (insert x0 X0) (insert x0 Y0))
        )
    )
)

(check-sat)
