;;; INFO: ys = scan (+) 0 xs /\ forall ((<=) 0) xs => forall ((<=) 0) ys
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |forall_pos|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 0))
        )
    )
)

(define-fun |scan_sum|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
       (() (Y0 (tail Y0) X0))
       ((q_0) (q_0))
       (
           (q_0 q_0 (or (= l_0 (+ l_1 l_2))
                        (and (= l_0 l_2) (pad l_1))))
       )
    )
)

(assert
   (forall ((X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|scan_sum| X0 Y0)
                (|forall_pos| Y0)
            )
            (|forall_pos| X0)
        )
    )
)

(check-sat)
