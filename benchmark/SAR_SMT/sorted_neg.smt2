;;; INFO: not (sorted (x::xs) /\ exists ((>) 0) xs /\ x >= 0)
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0) (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |exists_neg|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (>= l_0 0))
            (q_0 q_1 (< l_0 0))
            (q_1 q_1 true)
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)))
        (=>
            (and
                (|sorted| (insert x0 X0))
                (|exists_neg| X0)
                (>= x0 0)
            )
            false
        )
    )
)

(check-sat)
