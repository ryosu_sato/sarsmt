;;; INFO: ys = scan (+) 0 xs => length xs = length ys
;;; EXPECT: sat

(set-logic HORN)

(define-fun |scan_sum|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
       (() (Y0 (tail Y0) X0))
       ((q_0) (q_0))
       (
           (q_0 q_0 (or (= l_0 (+ l_1 l_2))
                        (and (= l_0 l_2) (pad l_1))))
       )
    )
)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
   (forall ((n0 Int) (n1 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|scan_sum| X0 Y0)
                (|length| n0 X0)
                (|length| n1 Y0)
            )
            (= n0 n1)
        )
    )
)

(check-sat)
