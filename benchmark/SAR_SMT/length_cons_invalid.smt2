;;; INFO: length (x::xs) = length x0
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (n1 Int) (x0 Int) (X0 (List Int)))
        (=>
            (and
                (|length| n0 (insert x0 X0))
                (|length| n1 X0)
            )
            (= n0 n1)
        )
    )
)

(check-sat)
