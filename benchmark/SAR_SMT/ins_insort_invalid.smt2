;;; INFO: ins x xs = insort x xs
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |ins|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        ((x0) ((insert 0 X0) X0 Y0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (and (= l_1 l_2) (<= l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (< x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(define-fun |insort|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        ((x0) ((insert 0 X0) X0 Y0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (and (= l_1 l_2) (< l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (<= x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)))
        (=>
            (and
                (|ins| x0 X0 Y0)
                (|insort| x0 X0 Z0)
            )
            (= Y0 Z0)
        )
    )
)

(check-sat)
