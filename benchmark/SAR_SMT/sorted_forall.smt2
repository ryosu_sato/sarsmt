;;; INFO: sorted (x::xs) => forall ((<=) x) xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |forall_ge|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 x_0))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)))
        (=>
            (|sorted| (insert x0 X0))
            (|forall_ge| x0 X0)
        )
    )
)

(check-sat)
