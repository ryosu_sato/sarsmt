;;; INFO: x <> y => count x xs = count x (y::xs)
;;; EXPECT: sat

(set-logic HORN)

(define-fun |count|
    ((c0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (x0 Int) (c0 Int) (X0 (List Int)))
        (=>
            (and
                (not (= n0 x0))
                (|count| c0 n0 X0)
            )
            (|count| c0 n0 (insert x0 X0))
        )
    )
)

(check-sat)
