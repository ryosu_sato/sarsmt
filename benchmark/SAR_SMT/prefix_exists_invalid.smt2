;;; INFO: prefix xs ys /\ exists ((=) 0) ys => exists ((=) 0) xs
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(define-fun |haszero|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (not (= l_0 0)))
            (q_0 q_1 (= l_0 0))
            (q_1 q_1 true)
        )
    )
)

(assert
    (forall ((X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|prefix| X0 Y0)
                (|haszero| Y0)
            )
            (|haszero| X0)
        )
    )
)

(check-sat)
