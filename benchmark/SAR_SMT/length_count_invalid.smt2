;;; INFO: length xs > count x xs
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(define-fun |count|
    ((c0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1)))
                                 (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((X0 (List Int)) (n0 Int) (c0 Int) (x0 Int))
        (=>
            (and
                (|length| n0 X0)
                (|count| c0 x0 X0)
            )
            (> n0 c0)
        )
    )
)

(check-sat)
