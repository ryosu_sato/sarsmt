;;; INFO: prefix xs (x::xs) => forall ((=) x) xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(define-fun |forall_eq|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (= l_0 x_0))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (|prefix| X0 (insert x0 X0))
            (|forall_eq| x0 X0)
        )
    )
)

(check-sat)
