;;; INFO: ys = [x] => last ys = x
;;; EXPECT: sat

(set-logic HORN)

(define-fun |last|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0 (insert x0 nil)))
        ((q_0) (q_1 q_2))
        (
            (q_0 q_0 true)
            (q_0 q_1 (= x_0 l_0))
            (q_0 q_2 (and (pad l_0) (= l_1 0))) ; case X0 = []
        )
    )
)

(assert
    (forall ((x0 Int) (Y0 (List Int)) (X0 (List Int)))
        (=>
            (and
                (= Y0 (insert x0 X0))
                (= X0 nil)
            )
            (|last| x0 Y0)
        )
    )
)

(check-sat)
