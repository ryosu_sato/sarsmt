;;; INFO: count x xs = count x (insert y xs)
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |count|
    ((c0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((c0 Int) (c1 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|count| c0 x0 X0)
                (|count| c1 x0 (insert y0 X0))
            )
            (= c0 c1)
        )
    )
)

(check-sat)
