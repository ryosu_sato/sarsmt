;;; INFO: ys = take n xs /\ exists ((=) 0) xs => exists ((=) 0) ys
;;; EXPECT: sat

(set-logic HORN)

(define-fun |haszero|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (not (= l_0 0)))
            (q_0 q_1 (= l_0 0))
            (q_1 q_1 true)
        )
    )
)

; take n0 Y0 = X0
(define-fun |take|
    ((n0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (= l_0 0)
                                  (pad l_2)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (= l_0 0)
                                  (not (pad l_2))))
                    (q_1 q_1 true)
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|take| n0 X0 Y0)
                (|haszero| X0)
            )
            (|haszero| Y0)
        )
    )
)

(check-sat)
