;;; INFO: ys = take_total n xs /\ forall ((<=) 0) xs => fold (+) 0 ys <= fold (+) 0 xs
;;; EXPECT: sat

(set-logic HORN)

; take_total n0 Y0 = X0
(define-fun |take_total|
    ((n0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (and (<= l_0 0)
                                  (pad l_2)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EZ0 (List Int)))
            (
                (() ((insert n0 EZ0) EZ0 X0 Y0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0)
                                  (= l_0 (+ l_1 1))
                                  (or (= l_2 l_3)
                                      (and (pad l_2) (pad l_3)))))
                    (q_0 q_1 (or (and (<= l_0 0)
                                      (not (pad l_2)))
                                 (and (not (pad l_2))
                                      (pad l_3))))
                    (q_1 q_1 true)
                )
            )
        )
    )
)

(define-fun |forall_pos|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 0))
        )
    )
)

(define-fun |sumfold|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (n1 Int) (n2 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|take_total| n2 X0 Y0)
                (|forall_pos| Y0)
                (|sumfold| n0 X0)
                (|sumfold| n1 Y0)
            )
            (<= n0 n1)
        )
    )
)

(check-sat)
