;;; EXPECT: sat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(define-fun |nth|
    ((i0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (= l_2 x_0)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1 q_2))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (not (= l_2 x_0))))
                    (q_1 q_1 true)
                    (q_0 q_2 (< l_0 0))
                    (q_2 q_2 true)
                )
            )
        )
    )
)

(assert
    (forall ((i0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|prefix| X0 Y0)
                (|nth| i0 x0 X0)
                (|nth| i0 y0 Y0)
            )
            (= x0 y0)
        )
    )
)

(check-sat)
