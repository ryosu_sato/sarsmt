;;; INFO: sorted (x::xs) /\ 0 <= x => forall ((<=) 0) xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0) (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |forall_pos|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 0))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)))
        (=>
            (and
                (|sorted| (insert x0 X0))
                (>= x0 0)
            )
            (|forall_pos| X0)
        )
    )
)

(check-sat)
