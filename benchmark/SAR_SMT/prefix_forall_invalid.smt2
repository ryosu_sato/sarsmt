;;; INFO: prefix xs ys /\ forall ((<=) 0) xs => forall ((<=) 0) ys
;;; EXPECT: unsat

(set-logic HORN)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(define-fun |forall_pos|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 0))
        )
    )
)

(assert
    (forall ((X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|prefix| X0 Y0)
                (|forall_pos| X0)
            )
            (|forall_pos| Y0)
        )
    )
)

(check-sat)
