;;; INFO: sorted ys /\ prefix xs ys => sorted xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(assert
    (forall ((X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|sorted| Y0)
                (|prefix| X0 Y0)
            )
            (|sorted| X0)
        )
    )
)

(check-sat)
