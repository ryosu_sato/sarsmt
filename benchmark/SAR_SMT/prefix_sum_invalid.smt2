;;; INFO: prefix xs ys => fold (+) 0 xs <= fold (+) 0 ys
;;; EXPECT: unsat

(set-logic HORN)

; X0 is a prefix of Y0
(define-fun |prefix|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (or (= l_0 l_1) (pad l_0)))
        )
    )
)

(define-fun |forall_pos|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0) (q_0))
        (
            (q_0 q_0 (>= l_0 0))
        )
    )
)

(define-fun |sumfold|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (= l_0 (+ l_1 l_2)))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (n1 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|prefix| X0 Y0)
                (|sumfold| n0 X0)
                (|sumfold| n1 Y0)
            )
            (<= n0 n1)
        )
    )
)

(check-sat)
