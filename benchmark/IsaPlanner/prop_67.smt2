;;; INFO: len (butlast xs) = len xs - 1 (* 0-1 = 0 *)
;;; EXPECT: sat

(set-logic HORN)

; Y0 = butlast X0
(define-fun |butlast|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0 (insert 0 nil)))
        ((q_0) (q_1))
        (
            (q_0 q_0 (= l_0 l_1))
            (q_0 q_1 (pad l_1))
        )
    )
)

(define-fun |length|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (() ((insert n0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|butlast| X0 Y0)
                (|length| n0 Y0)
                (|length| n1 X0)
            )
            (or (= n0 (- n1 1)) (and (= n0 0) (= n1 0)))
        )
    )
)

(check-sat)
