;;; INFO: xs = [] => last (x:xs) = x
;;; EXPECT: sat

(set-logic HORN)

(define-fun |last|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0 (insert x0 nil)))
        ((q_0) (q_1 q_2))
        (
            (q_0 q_0 true)
            (q_0 q_1 (= x_0 l_0))
            (q_0 q_2 (and (pad l_0) (= l_1 0))) ; case X0 = []
        )
    )
)

(assert
    (forall ((x0 Int) (y0 Int) (X0 (List Int)))
        (=>
            (and
                (= X0 nil)
                (|last| y0 (insert x0 X0))
            )
            (= x0 y0)
        )
    )
)

(check-sat)
