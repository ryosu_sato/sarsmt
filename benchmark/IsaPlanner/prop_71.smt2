;;; INFO: x <> y => mem x (ins y xs) = mem x xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |elem|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (not (= l_0 x_0)))
            (q_0 q_1 (= l_0 x_0))
            (q_1 q_1 true)
        )
    )
)

(define-fun |ins|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        ((x0) ((insert 0 X0) X0 Y0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (and (= l_1 l_2) (<= l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (< x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (!= x0 y0)
                (|ins| y0 X0 Y0)
                (|elem| x0 X0)
            )
            (|elem| x0 Y0)
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (!= x0 y0)
                (|ins| y0 X0 Y0)
                (|elem| x0 Y0)
            )
            (|elem| x0 X0)
        )
    )
)

(check-sat)
