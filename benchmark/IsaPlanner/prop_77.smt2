;;; INFO: sorted xs /\ ys = insort x xs => sorted ys
;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0) (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |insort|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        ((x0) ((insert 0 X0) X0 Y0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (and (= l_1 l_2) (< l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (<= x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|sorted| X0)
                (|insort| x0 X0 Y0)
            )
            (|sorted| Y0)
        )
    )
)

(check-sat)
