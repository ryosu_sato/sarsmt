;;; INFO: n = x => 1 + count n xs = count n (x::xs)
;;; EXPECT: sat

(set-logic HORN)

(define-fun |count|
    ((c0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1)))
                                 (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert c0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (or (and (= x_0 l_2) (= l_0 (+ l_1 1))) (and (!= x_0 l_2) (= l_0 l_1))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(assert
    (forall ((n0 Int) (x0 Int) (X0 (List Int)) (c1 Int) (c2 Int))
        (=>
            (and
                (= n0 x0)
                (|count| c1 n0 X0)
                (|count| c2 n0 (insert x0 X0))
            )
            (= (+ 1 c1) c2)
        )
    )
)

(check-sat)
