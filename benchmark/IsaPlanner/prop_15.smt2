;;; INFO: length (ins x xs) = 1 + length xs
;;; EXPECT: sat

(set-logic HORN)

(define-fun |len|
    ((n0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (= l_0 0) (pad l_2)))
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                (
                    ()
                    ((insert n0 EY0) EY0 X0)
                )
                (
                    (q_0)
                    (q_1)
                )
                (
                    (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
                    (q_0 q_1 (and (!= l_0 0) (pad l_2)))
                )
            )
        )
    )
)

(define-fun |ins|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (
            (x0)
            ((insert 0 X0) X0 Y0)
        )
        (
            (q_0)
            (q_1)
        )
        (
            (q_0 q_0 (and (= l_1 l_2) (<= l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (< x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(assert
    (forall ((x0 Int) (n1 Int) (n2 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|ins| x0 X0 Y0)
                (|len| n1 X0)
                (|len| n2 Y0)
            )
            (= (+ n1 1) n2)
        )
    )
)

(check-sat)
