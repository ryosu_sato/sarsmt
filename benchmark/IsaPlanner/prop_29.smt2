;;; INFO: mem x (ins1 x xs)
;;; EXPECT: sat

(set-logic HORN)

(define-fun |elem|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (not (= l_0 x_0)))
            (q_0 q_1 (= l_0 x_0))
            (q_1 q_1 true)
        )
    )
)

(define-fun |ins1|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        ((x0) (X0 Y0))
        ((q_0) (q_1))
        (
            (q_0 q_0 (and (= l_0 l_1) (!= x_0 l_0)))
            (q_0 q_1 (or (and (= l_0 l_1) (= l_0 x_0)) (and (= l_1 x_0) (pad l_0))))
            (q_1 q_1 (= l_0 l_1))
        )
    )
)

(assert
    (forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (|ins1| x0 X0 Y0)
            (|elem| x0 Y0)
        )
    )
)

(check-sat)
