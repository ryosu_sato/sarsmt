;;; EXPECT: sat

(set-logic HORN)

(define-fun |sum|
    ((X0 (List Int)) (Y0 (List Int)) (Z0 (List Int))) Bool
    (
        (() (X0 Y0 Z0))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (= (+ l_0 l_1) l_2))
        )
    )
)

(define-fun |nonzero|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (= l_0 0))
            (q_0 q_1 (!= l_0 0))
            (q_1 q_1 true)
        )
    )
)

(assert
  (forall ((d0 Int))
    (=>
      true
      (|sum| nil nil nil)
    )
  )
)

(assert
  (forall ((x0 Int) (y0 Int) (z0 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)))
    (=>
      (and
        (= (+ x0 y0) z0)
        (|sum| X0 Y0 Z0)
      )
      (|sum| (insert x0 X0) (insert y0 Y0) (insert z0 Z0))
    )
  )
)

(assert
  (forall ((x0 Int) (X0 (List Int)))
    (=>
      (not (= x0 0))
      (|nonzero| (insert x0 X0))
    )
  )
)

(assert
  (forall ((x0 Int) (X0 (List Int)))
    (=>
      (|nonzero| X0)
      (|nonzero| (insert x0 X0))
    )
  )
)

(assert
  (forall ((X0 (List Int)))
    (=>
      (and
        (|nonzero| X0)
        (|sum| X0 X0 X0)
      )
      false
    )
  )
)

(check-sat)
