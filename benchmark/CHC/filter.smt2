;;; EXPECT: sat


(set-logic HORN)

(define-fun |prefilter|
  ((X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postfilter|
  ((X0 (List Int)) (Y0 (List Int))) Bool
  (
    (() (X0 Y0))
    ((q_0) (q_0))
    (
      (q_0 q_0 (not (pad l_0)))
      (q_0 q_1 (pad l_0))
    )
  )
)


(define-fun |prelength|
  ((X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postlength|
  ((n0 Int) (X0 (List Int))) Bool
  (
    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (= l_0 0) (pad l_2)))
        )
      )
    )

    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (!= l_0 0) (pad l_2)))
        )
      )
    )
  )
)


;; filter
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prefilter| X0)
          (= X0 nil))
     (|postfilter| X0 nil))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prefilter| X0)
          (= X0 nil))
     (|postfilter| X0 nil))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prefilter| X0)
          (= X0 (insert y0 Y0)))
     (|prefilter| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prefilter| X0)
          (= X0 (insert y0 Y0))
          (|postfilter| Y0 Z0)
          (!= b0 0))
     (|postfilter| X0 (insert y0 Z0)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prefilter| X0)
          (= X0 (insert y0 Y0))
          (|postfilter| Y0 Z0)
          (= b0 0))
     (|postfilter| X0 Z0))))

;; length
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 nil))
     (|postlength| 0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 (insert y0 Y0)))
     (|prelength| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0) (= X0 (insert y0 Y0)) (|postlength| n0 Y0))
     (|postlength| (+ 1 n0) X0))))

;; main
(assert
  (forall ((X0 (List Int)))
    (=>
      true
      (|prefilter| X0))))

(assert
  (forall ((n0 Int) (X0 (List Int)))
    (=>
      (|postfilter| X0 Y0)
      (|prelength| X0))))

(assert
  (forall ((n0 Int) (X0 (List Int)))
    (=>
      (and (|postfilter| X0 Y0)
           (|postlength| n0 X0))
      (|prelength| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|postfilter| X0 Y0)
          (|postlength| n0 X0)
          (|postlength| m0 Y0))
     (>= n0 m0))))

(check-sat)
