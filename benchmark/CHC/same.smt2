;;; EXPECT: sat

(set-logic HORN)

(define-fun |same|
    ((x0 Int) (X0 (List Int))) Bool
    (
        (
            (x0)
            (X0)
        )
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (= x_0 l_0))
        )
    )
)

(define-fun |has|
    ((x0 Int) (X0 (List Int))) Bool
    (
        ((x0) (X0))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (not (= x_0 l_0)))
            (q_0 q_1 (= x_0 l_0))
            (q_1 q_1 true)
        )
    )
)

(assert
	(forall ((x0 Int))
		(=>
			true
			(|same| x0 nil)
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			(|same| x0 X0)
			(|same| x0 (insert x0 X0))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			true
			(|has| x0 (insert x0 X0))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)))
		(=>
			(|has| x0 X0)
			(|has| x0 (insert y0 X0))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			(and
				(|same| x0 X0)
				(|has| (+ x0 1) X0)
			)
			false
		)
	)
)

(check-sat)
