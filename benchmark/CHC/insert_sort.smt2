;;; EXPECT: sat

;; let rec sorted xs =
;;   match xs with
;;   | [] -> true
;;   | [x] -> true
;;   | y1::y2::ys -> y1 <= y2 && sorted (y2::ys)
;;
;; let insert x xs =
;;   match xs with
;;   | [] -> [x]
;;   | y::ys ->
;;       if x <= y then x::xs
;;       else
;;         let zs = insert x ys in
;;         y::zs
;;
;; let rec insert_sort xs =
;;   match xs with
;;   | [] -> []
;;   | y::ys -> insert y @@ insert_sort ys
;;
;; let main xs =
;;   let ys = insert_sort xs in
;;   assert (sorted ys)

(set-logic HORN)

(define-fun |preinsert|
  ((x0 Int) (X0 (List Int))) Bool
  (
     (() (X0 (tail X0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |postinsert|
  ((x0 Int) (Y0 (List Int)) (X0 (List Int))) Bool
  (
     ((x0) (X0 (tail X0) (insert 0 nil) Y0))
     ((q_0) (q_0))
     (
       (q_0 q_0 (or (and (not (pad l_2)) (or (<= x_0 l_0) (<= l_3 l_0)) (not (> l_0 l_1)))
                    (and (pad l_2) (not (> l_0 l_1)))))
     )
  )
)

(define-fun |preinsertsort|
  ((X0 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postinsertsort|
  ((Y0 (List Int)) (X0 (List Int))) Bool
  (
     (() (X0 (tail X0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |presorted|
  ((X0 (List Int))) Bool
  (
     (() (X0))
     ((q_0) (q_0))
     (
       (q_0 q_0 true)
     )
  )
)

(define-fun |postsorted|
  ((b0 Int) (X0 (List Int))) Bool
  (
    ((b0) (X0 (tail X0) (insert 0 nil)))
    ((q_0) (q_1))
    (
      (q_0 q_0 (not (> l_0 l_1)))
      (q_0 q_1 (or (!= x_0 0) (> l_0 l_1)))
      (q_1 q_1 true)
    )
  )
)

;; sorted
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 nil)
         (!= x0 0))
    (|postsorted| x0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 nil))
         (!= x0 0))
    (|postsorted| x0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert y1 (insert y2 Y0)))
         (<= y1 y2))
    (|presorted| (insert y2 Y0)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 (insert x2 Y0)))
         (<= x1 x2)
         (|postsorted| b0 (insert x2 Y0)))
    (|postsorted| b0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 (insert x2 Y0)))
         (not (<= x1 x2)))
    (|postsorted| 0 X0))))

;; insert
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsert| x1 X0)
         (= X0 nil))
    (|postinsert| x1 X0 (insert x1 nil)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsert| x1 X0)
         (= X0 (insert y1 Y0))
         (<= x1 y1))
    (|postinsert| x1 X0 (insert x1 X0)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsert| x1 X0)
         (= X0 (insert y1 Y0))
         (not (<= x1 y1))
         (|postinsert| x1 Y0 Z0))
    (|postinsert| x1 X0 (insert y1 Z0)))))

;; insert_sort
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsertsort| X0) (= X0 nil))
    (|postinsertsort| X0 nil))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsertsort| X0) (= X0 (insert y1 Y0)))
    (|preinsertsort| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsertsort| X0) (= X0 (insert y1 Y0)) (|postinsertsort| Y0 Z0))
    (|preinsert| y1 Z0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|preinsertsort| X0) (= X0 (insert y1 Y0)) (|postinsertsort| Y0 Z0) (|postinsert| y1 Z0 W0))
    (|postinsertsort| X0 Z0))))

;; main
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    true
    (|preinsertsort| X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|postinsertsort| X0 Y0))
    (|presorted| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|postinsertsort| X0 Y0) (|postsorted| b0 Y0))
    (!= b0 0))))

(check-sat)
