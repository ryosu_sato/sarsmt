;;; EXPECT: sat

;; let rec make n =
;;   if n = 0 then
;;     []
;;   else
;;     n :: make (n-1)
;;
;; let rec length xs =
;;   match xs with
;;   | [] -> 0
;;   | y::ys -> 1 + lenth xs
;;
;; let main n =
;;   let xs = make n in
;;   let m = length xs in
;;   assert (n = m)

(set-logic HORN)

(define-fun |premake|
  ((n0 Int)) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postmake|
  ((n0 Int) (X0 (List Int))) Bool
  (
    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (= l_0 0) (pad l_2)))
        )
      )
    )

    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (!= l_0 0) (pad l_2)))
        )
      )
    )
  )
)

(define-fun |prelength|
  ((X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postlength|
  ((n0 Int) (X0 (List Int))) Bool
  (
    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (= l_0 0) (pad l_2)))
        )
      )
    )

    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (!= l_0 0) (pad l_2)))
        )
      )
    )
  )
)

;; make
(assert
  (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premake| n0)
           (= n0 0))
      (|postmake| n0 nil))))

(assert
  (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premake| n0)
           (not (= n0 0)))
      (|premake| (- n0 1)))))

(assert
  (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premake| n0)
           (!= n0 0)
           (|postmake| (- n0 1) X0))
      (|postmake| n0 (insert n0 X0)))))

;; length
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 nil))
     (|postlength| 0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 (insert y0 Y0)))
     (|prelength| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0) (= X0 (insert y0 Y0)) (|postlength| n0 Y0))
     (|postlength| (+ 1 n0) X0))))

;; main
(assert
  (forall ((n0 Int))
    (=>
      true
      (|premake| n0))))

(assert
  (forall ((n0 Int) (X0 (List Int)))
    (=>
      (|postmake| n0 X0)
      (|prelength| X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|postmake| n0 X0) (|postlength| m0 X0))
     (= n0 m0))))

(check-sat)
