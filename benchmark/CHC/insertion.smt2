;;; EXPECT: sat

(set-logic HORN)

(define-fun |insertion|
    ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
    (
        (
            (x0)
            ((insert 0 X0) X0 Y0)
        )
        (
            (q_0)
            (q_1)
        )
        (
            (q_0 q_0 (and (= l_1 l_2) (< l_1 x_0)))
            (q_0 q_1 (and (= l_2 x_0) (or (<= x_0 l_1) (pad l_1))))
            (q_1 q_1 (= l_0 l_2))
        )
    )
)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |unsorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (not (> l_0 l_1)))
            (q_0 q_1 (> l_0 l_1))
            (q_1 q_1 true)
        )
    )
)

(assert
	(forall ((x0 Int))
		(=>
			true
			(|insertion| x0 nil (insert x0 nil))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)))
		(=>
			(<= x0 y0)
			(|insertion| x0 (insert y0 X0) (insert x0 (insert y0 X0)))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(and
				(> x0 y0)
				(|insertion| x0 X0 Y0)
			)
			(|insertion| x0 (insert y0 X0) (insert y0 Y0))
		)
	)
)

(assert
	(forall ((d0 Int))
		(=>
			true
			(|sorted| nil)
		)
	)
)

(assert
	(forall ((x0 Int))
		(=>
			true
			(|sorted| (insert x0 nil))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)))
		(=>
			(and
				(<= x0 y0)
				(|sorted| (insert y0 X0))
			)
			(|sorted| (insert x0 (insert y0 X0)))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)))
		(=>
			(> x0 y0)
			(|unsorted| (insert x0 (insert y0 X0)))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			(|unsorted| X0)
			(|unsorted| (insert x0 X0))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(and
				(|sorted| X0)
				(|insertion| x0 X0 Y0)
				(|unsorted| Y0)
			)
			false
		)
	)
)

(check-sat)
