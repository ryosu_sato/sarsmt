;;; EXPECT: sat

(set-logic HORN)

(define-fun |leq|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (<= l_0 l_1))
        )
    )
)

(assert
    (forall ((d0 Int))
        (=>
            true
            (|leq| nil nil)
        )
    )
)

(assert
    (forall ((x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (<= x0 y0)
                (|leq| X0 Y0)
            )
            (|leq| (insert x0 X0) (insert y0 Y0))
        )
    )
)

(assert
    (forall ((X0 (List Int)) (Y0 (List Int)))
        (=>
            (and
                (|leq| X0 Y0)
                (|leq| Y0 X0)
                (not (= X0 Y0))
            )
            false
        )
    )
)

(check-sat)
