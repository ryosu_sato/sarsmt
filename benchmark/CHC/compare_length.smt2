;;; EXPECT: sat

(set-logic HORN)

(define-fun |len_eq|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (and (not (pad l_0)) (not (pad l_1))))
        )
    )
)

(define-fun |len_gt|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (and (not (pad l_0)) (not (pad l_1))))
            (q_0 q_1 (pad l_1))
            (q_1 q_1 true)
        )
    )
)

(define-fun |len_lt|
    ((X0 (List Int)) (Y0 (List Int))) Bool
    (
        (() (X0 Y0))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (and (not (pad l_0)) (not (pad l_1))))
            (q_0 q_1 (pad l_0))
            (q_1 q_1 true)
        )
    )
)

(assert
	(forall ((d0 Int))
		(=>
			true
			(|len_eq| nil nil)
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(|len_eq| X0 Y0)
			(|len_eq| (insert x0 X0) (insert y0 Y0))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(|len_eq| X0 Y0)
			(|len_gt| (insert x0 X0) Y0)
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(|len_gt| X0 Y0)
			(|len_gt| (insert x0 X0) Y0)
		)
	)
)

(assert
	(forall ((y0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(|len_eq| X0 Y0)
			(|len_lt| X0 (insert y0 Y0))
		)
	)
)

(assert
	(forall ((y0 Int) (X0 (List Int)) (Y0 (List Int)))
		(=>
			(|len_lt| X0 Y0)
			(|len_lt| X0 (insert y0 Y0))
		)
	)
)

(assert
	(forall ((X0 (List Int)) (Y0 (List Int)))
		(=>
			(and
				(|len_gt| X0 Y0)
				(|len_lt| X0 Y0)
			)
			false
		)
	)
)

(check-sat)
