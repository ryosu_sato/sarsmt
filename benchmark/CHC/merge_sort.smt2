;;; EXPECT: sat

;; let rec merge xs ys =
;;   match xs, ys with
;;   | _, [] -> xs
;;   | [], _ -> ys
;;   | x::xs2, y::ys2 ->
;;       if x <= y then
;;         let zs = merge xs2 ys in
;;         x::zs
;;       else
;;         let zs = merge xs ys2 in
;;         y::zs
;;
;; let rec split xs k =
;;   match xs with
;;   | [] -> k [] []
;;   | [x] -> k [x] []
;;   | y1::y2::ys ->
;;       let scont zs1 zs2 = k (y1::zs1) (y2::zs2) in
;;       split ys scont
;;
;; let rec merge_sort xs =
;;   match xs with
;;   | [] -> []
;;   | [x] -> [x]
;;   | _ ->
;;       let mscont ys1 ys2 =
;;         let zs1 = merge_sort ys1 in
;;         let zs2 = merge_sort ys2 in
;;         merge zs1 zs2
;;       in
;;       split xs mscont
;;
;; let rec sorted xs =
;;   match xs with
;;   | [] -> true
;;   | [x] -> true
;;   | y1::y2::ys -> y1 <= y2 && sorted (y2::ys)
;;
;; let main xs =
;;   let ys = merge_sort xs in
;;   assert (sorted ys)

(set-logic HORN)

(define-fun |premerge|
  ((X0 (List Int)) (Y0 (List Int))) Bool
  (
     (() (X0 (tail X0) Y0 (tail Y0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (and (not (> l_0 l_1)) (not (> l_2 l_3))))
     )
  )
)

(define-fun |postmerge|
  ((X0 (List Int)) (Y0 (List Int)) (Z0 (List Int))) Bool
  (
     (() (Z0 (tail Z0) (insert 0 nil) X0 Y0))
     ((q_0) (q_0))
     (
       (q_0 q_0 (or (and (not (pad l_2))
                         (pad l_0)
                         (pad l_3)
                         (pad l_4))
                    (and (not (pad l_2))
                         (or (<= l_3 l_0) (<= l_4 l_0))
                         (not (> l_0 l_1)))
                    (and (pad l_2)
                         (not (> l_0 l_1)))))
     )
  )
)


(define-fun |presorted|
  ((X0 (List Int))) Bool
  (
     (() (X0))
     ((q_0) (q_0))
     (
       (q_0 q_0 true)
     )
  )
)

(define-fun |postsorted|
  ((b0 Int) (X0 (List Int))) Bool
  (
    ((b0) (X0 (tail X0) (insert 0 nil)))
    ((q_0) (q_1))
    (
      (q_0 q_0 (not (> l_0 l_1)))
      (q_0 q_1 (or (!= x_0 0) (> l_0 l_1)))
      (q_1 q_1 true)
    )
  )
)


(define-fun |presplit|
  ((X0 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postsplit|
  ((X0 (List Int)) (Y0 (List Int))) Bool
  (
     (() (Y0 (tail Y0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |prek|
  ((X0 (List Int)) (Y0 (List Int)) (Y1 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postk|
  ((X0 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Z0 (List Int))) Bool
  (
     (() (Z0 (tail Z0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |prescont|
  ((y1 Int) (y2 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)) (Z1 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postscont|
  ((y1 Int) (y2 Int) (X0 (List Int)) (Y0 (List Int)) (Z0 (List Int)) (Z1 (List Int)) (Z2 (List Int))) Bool
  (
     (() (Z2 (tail Z2)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |premergesort|
  ((X0 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postmergesort|
  ((X0 (List Int)) (Y0 (List Int))) Bool
  (
     (() (Y0 (tail Y0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

(define-fun |premscont|
  ((y1 Int) (y2 Int) (X0 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Y2 (List Int))) Bool
  (
     (() ())
     ((q_0) (q_0))
     ()
  )
)

(define-fun |postmscont|
  ((y1 Int) (y2 Int) (X0 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Y2 (List Int)) (Z0 (List Int))) Bool
  (
     (() (Z0 (tail Z0)))
     ((q_0) (q_0))
     (
       (q_0 q_0 (not (> l_0 l_1)))
     )
  )
)

;; merge
(assert
  (forall ((X0 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premerge| X0 Y0)
           (= Y0 nil))
      (|postmerge| X0 Y0 X0))))

(assert
  (forall ((x0 Int) (y0 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premerge| X0 Y0) (= Y0 (insert y0 Y1)) (= X0 nil))
      (|postmerge| X0 Y0 Y0))))

(assert
  (forall ((x0 Int) (y0 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premerge| X0 Y0) (= X0 (insert x0 X1)) (= Y0 (insert y0 Y1)) (<= x0 y0))
      (|premerge| X1 Y0))))

(assert
  (forall ((x0 Int) (y0 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Z0 (List Int)))
    (=>
      (and (|premerge| X0 Y0) (= X0 (insert x0 X1)) (= Y0 (insert y0 Y1)) (<= x0 y0) (|postmerge| X1 Y0 Z0))
      (|postmerge| X0 Y0 (insert x0 Z0)))))

(assert
  (forall ((x0 Int) (y0 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premerge| X0 Y0) (= X0 (insert x0 X1)) (= Y0 (insert y0 Y1)) (not (<= x0 y0)))
      (|premerge| X0 Y1))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Z0 (List Int)))
    (=>
      (and (|premerge| X0 Y0) (= X0 (insert x0 X1)) (= Y0 (insert y0 Y1)) (not (<= x0 y0)) (|postmerge| X0 Y1 Z0))
      (|postmerge| X0 Y0 (insert y0 Z0)))))


;; split
(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 nil))
      (|prek| X0 nil nil))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 nil) (|postk| X0 nil nil Y0))
      (|postsplit| X0 Y0))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert x0 nil)))
      (|prek| X0 (insert x0 nil) nil))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert x0 nil)) (|postk| X0 (insert x0 nil) nil Y0))
      (|postsplit| X0 Y0))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))))
      (|presplit| Y0))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))) (|prek| X0 Z1 Z2))
      (|prescont| y1 y2 X0 Y0 Z1 Z2))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))) (|prek| X0 Z1 Z2) (|postscont| y1 y2 X0 Y0 Z1 Z2 W0))
      (|postk| X0 Z1 Z2 W0))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Z0 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))) (|postsplit| Y0 Z0))
      (|postsplit| X0 Z0))))

;; scont
(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))) (|prescont| y1 y2 X0 Y0 Z1 Z2))
      (|prek| X0 (insert y1 Z1) (insert y2 Z2)))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|presplit| X0) (= X0 (insert y1 (insert y2 Y0))) (|prescont| y1 y2 X0 Y0 Z1 Z2) (|postk| X0 (insert y1 Z1) (insert y2 Z2) W0))
      (|postscont| y1 y2 X0 Y0 Z1 Z2 W0))))


;; merge_sort
(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 nil))
      (|postmergesort| X0 nil))))

(assert
  (forall ((x0 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 nil)))
      (|postmergesort| X0 (insert x0 nil)))))

(assert
  (forall ((x0 Int) (x1 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))))
      (|presplit| X0))))

(assert
  (forall ((x0 Int) (x1 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Y2 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|prek| X0 Y1 Y2))
      (|premscont| x0 x1 X0 Y0 Y1 Y2))))

(assert
  (forall ((x0 Int) (x1 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|prek| X0 Y1 Y2) (|postmscont| y1 y2 X0 Y0 Y1 Y2 Z0))
      (|postk| X0 Y1 Y2 Z0))))

(assert
  (forall ((x0 Int) (x1 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|postsplit| Y0 Z0))
      (|postmergesort| X0 Z0))))

;; mscont
(assert
  (forall ((x0 Int) (x1 Int) (y0 Int) (y1 Int) (y2 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|premscont| x0 x1 X0 X1 Y1 Y2))
      (|premergesort| Y1))))

(assert
  (forall ((x0 Int) (x1 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Z1 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|premscont| x0 x1 X0 X1 Y1 Y2) (|postmergesort| Y1 Z1))
      (|premergesort| Y2))))

(assert
  (forall ((x0 Int) (x1 Int) (y0 Int) (X0 (List Int)) (X1 (List Int)) (Y0 (List Int)) (Y1 (List Int)) (Z1 (List Int)) (Z2 (List Int)))
    (=>
      (and (|premergesort| X0) (= X0 (insert x0 (insert x1 X1))) (|premscont| x0 x1 X0 X1 Y1 Y2) (|postmergesort| Y1 Z1) (|postmergesort| Y2 Z2))
      (|premerge| Z1 Z2))))


;; sorted
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 nil)
         (!= x0 0))
    (|postsorted| x0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (x1 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 nil))
         (!= x0 0))
    (|postsorted| x0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert y1 (insert y2 Y0)))
         (<= y1 y2))
    (|presorted| (insert y2 Y0)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 (insert x2 Y0)))
         (<= x1 x2)
         (|postsorted| b0 (insert x2 Y0)))
    (|postsorted| b0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|presorted| X0)
         (= X0 (insert x1 (insert x2 Y0)))
         (not (<= x1 x2)))
    (|postsorted| 0 X0))))


;; main
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    true
    (|premergesort| X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|postmergesort| X0 Y0))
    (|presorted| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
    (and (|postmergesort| X0 Y0) (|postsorted| b0 Y0))
    (!= b0 0))))

(check-sat)
