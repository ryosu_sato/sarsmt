;;; EXPECT: sat

(set-logic HORN)

(define-fun |fib|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0) (tail (tail X0))))
        ((q_0)
        (q_2))
        (
            (q_0 q_0 (= l_0 (+ l_1 l_2)))
            (q_0 q_1 (= l_0 1))
            (q_1 q_2 (= l_0 1))
        )
    )
)

(define-fun |len3x|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0)
        (q_0))
        (
            (q_0 q_1 true)
            (q_1 q_2 true)
            (q_2 q_0 true)
        )
    )
)

(assert
	(forall ((d0 Int))
		(=>
			true
			(|fib| (insert 1 (insert 1 nil)))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (z0 Int) (X0 (List Int)))
		(=>
			(and
				(= x0 (+ y0 z0))
				(|fib| (insert y0 (insert z0 X0)))
			)
			(|fib| (insert x0 (insert y0 (insert z0 X0))))
		)
	)
)

(assert
	(forall ((d0 Int))
		(=>
			true
			(|len3x| nil)
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (z0 Int) (X0 (List Int)))
		(=>
			(|len3x| X0)
			(|len3x| (insert x0 (insert y0 (insert z0 X0))))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			(and
				(|fib| (insert x0 X0))
				(|len3x| (insert x0 X0))
				(= 1 (mod x0 2))
			)
			false
		)
	)
)

(check-sat)
