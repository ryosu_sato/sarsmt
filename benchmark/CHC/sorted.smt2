;;; EXPECT: sat

(set-logic HORN)

(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0)
        (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |haszero|
    ((X0 (List Int))) Bool
    (
        (() (X0))
        ((q_0)
        (q_1))
        (
            (q_0 q_0 (not (= l_0 0)))
            (q_0 q_1 (= l_0 0))
            (q_1 q_1 true)
        )
    )
)

(assert
	(forall ((d0 Int))
		(=>
			true
			(|sorted| nil)
		)
	)
)

(assert
	(forall ((x0 Int))
		(=>
			true
			(|sorted| (insert x0 nil))
		)
	)
)

(assert
	(forall ((x0 Int) (y0 Int) (X0 (List Int)))
		(=>
			(and
				(<= x0 y0)
				(|sorted| (insert y0 X0))
			)
			(|sorted| (insert x0 (insert y0 X0)))
		)
	)
)

(assert
	(forall ((X0 (List Int)))
		(=>
			true
			(|haszero| (insert 0 X0))
		)
	)
)

(assert
	(forall ((x0 Int) (X0 (List Int)))
		(=>
			(|haszero| X0)
			(|haszero| (insert x0 X0))
		)
	)
)

(assert
	(forall ((X0 (List Int)))
		(=>
			(and
				(|sorted| (insert 1 X0))
				(|haszero| X0)
			)
			false
		)
	)
)

(check-sat)
