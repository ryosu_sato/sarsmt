;;; EXPECT: sat

;; let rec snoc xs x =
;;   match xs with
;;   | [] -> [x]
;;   | y::ys ->
;;       let zs = snoc ys x in
;;       y::zs
;;
;; let rec reverse xs =
;;   match xs with
;;   | [] -> []
;;   | y::ys ->
;;       let zs = reverse ys in
;;       snoc zs y
;;
;; let rec length xs =
;;   match xs with
;;   | [] -> 0
;;   | y::ys -> 1 + lenth xs
;;
;; let main xs =
;;   let ys = reverse xs in
;;   let n = length xs in
;;   let m = length ys in
;;   assert (n = m)

(set-logic HORN)

(define-fun |presnoc|
  ((x0 Int) (X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postsnoc|
  ((x0 Int) (X0 (List Int)) (Y0 (List Int))) Bool
  (
    (() (X0 Y0))
    ((q_0) (q_1))
    (
      (q_0 q_0 (not (pad l_0)))
      (q_0 q_1 (pad l_0))
    )
  )
)


(define-fun |prereverse|
  ((X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postreverse|
  ((X0 (List Int)) (Y0 (List Int))) Bool
  (
    (() (X0 Y0))
    ((q_0) (q_0))
    (
      (q_0 q_0 (and (not (pad l_0)) (not (pad l_1))))
    )
  )
)


(define-fun |prelength|
  ((X0 (List Int))) Bool
  (
    (() ())
    ((q_0) (q_0))
    ()
  )
)

(define-fun |postlength|
  ((n0 Int) (X0 (List Int))) Bool
  (
    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (= l_0 0) (pad l_2)))
        )
      )
    )

    (exists ((EY0 (List Int)))
      (
        (() ((insert n0 EY0) EY0 X0))
        ((q_0) (q_1))
        (
          (q_0 q_0 (and (= l_0 (+ l_1 1)) (not (pad l_2))))
          (q_0 q_1 (and (!= l_0 0) (pad l_2)))
        )
      )
    )
  )
)


;; snoc
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|presnoc| x0 X0)
          (= X0 nil))
     (|postsnoc| x0 X0 (insert x0 nil)))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|presnoc| x0 X0)
          (= X0 (insert y0 Y0)))
     (|presnoc| x0 Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|presnoc| x0 X0) (= X0 (insert y0 Y0)) (|postsnoc| x0 Y0 Z0))
     (|postsnoc| x0 X0 (insert y0 Z0)))))


;; reverse
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prereverse| X0)
          (= X0 nil))
     (|postreverse| X0 nil))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prereverse| X0) (= X0 (insert y0 Y0)))
     (|prereverse| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prereverse| X0) (= X0 (insert y0 Y0)) (|postreverse| Y0 Z0))
     (|presnoc| y0 Z0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prereverse| X0)
          (= X0 (insert y0 Y0))
          (|postreverse| Y0 Z0)
          (|postsnoc| y0 Z0 Z1))
     (|postreverse| X0 Z1))))

;; length
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 nil))
     (|postlength| 0 X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0)
          (= X0 (insert y0 Y0)))
     (|prelength| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|prelength| X0) (= X0 (insert y0 Y0)) (|postlength| n0 Y0))
     (|postlength| (+ 1 n0) X0))))

;; main
(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     true
     (|prereverse| X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (|postreverse| X0 Y0)
     (|prelength| X0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|postreverse| X0 Y0) (|postlength| n0 X0))
     (|prelength| Y0))))

(assert
 (forall ((b0 Int) (x0 Int) (y0 Int) (X0 (List Int)) (Y0 (List Int)))
   (=>
     (and (|postreverse| X0 Y0) (|postlength| n0 X0) (|postlength| m0 Y0))
     (= n0 m0))))

(check-sat)
