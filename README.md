# SAR-SMT

SAR-SMT is a SMT-solver written in [Rust](https://www.rust-lang.org/).

Given predicates represented by symbolic automatic relations and first-order formulas containing these predicates, SAR-SMT checks the satisfiability of the formulas.
The formulas have to be expressed in the theory of LIA + list.

## Build

You have to install [Z3](https://github.com/Z3Prover/z3) beforehand since SAR-SMT is dependent on it.

In this directory run the following command.

```
cargo build --release
```

The binary file will made in the directory `target/release/`.

## Usage

SAR-SMT uses either Z3, [HoIce](https://github.com/hopv/hoice), or [Eldarica](https://github.com/uuverifiers/eldarica) as a back-end solver.
You have to install the one of them you want.

You can rum `sarsmt` with a back-end CHC solver and a input file as follows.

```
sarsmt <backend_solver> <input_file>
```

## Language

The language is basically a subset of [SMT-LIB 2.6](http://smtlib.cs.uiowa.edu/) format.
In addition, SAR-SMT supports the definition of predicates represented by SARs.

The input file should be like the follwoing.

```
(set-logic HORN)

<predicate_definition>*

<assertion>*

(check-sat)
```

### Predicate Definition

You can define predicate which are &Delta;<sub>0</sub><sup>sar</sup>-formulas or &Delta;<sub>1</sub><sup>sar</sup>-formulas.

- Definition of &Delta;<sub>0</sub><sup>sar</sup>-formula

```
(define-fun <pred_name>
    (<int_var>* <list_var>*) Bool
    <sar_def>
)
```

- Definition of &Delta;<sub>1</sub><sup>sar</sup>-formula

```
(define-fun <pred_name>
    (<int_var>* <list_var>*) Bool
    (
        (exists (<eq_var>*) <sar_def>)
        (exists (<eq_var>*) <sar_def>)
    )
)
```

The `<pred_name>` has to start and end with the character `|`.

The &Delta;<sub>1</sub><sup>sar</sup>-formulas are defined by the pairs of &Sigma;<sub>1</sub><sup>sar</sup>-formulas.

Note: When defining &Delta;<sub>1</sub><sup>sar</sup>-formula, make sure that the latter &Sigma;<sub>1</sub><sup>sar</sup>-formula is actually the negation of the former, since SAR-SMT doesn't check that.

#### Definition of SARs

The definition of symbolic automatic relations is like:

```
(
    ((<int_term>*) (<list_term>*))
    ((<initial_state>*) (<final_state>*))
    (
        (<source_state> <target_state> <label>)*
    )
)
```

The `<int_term>`s are integer terms set to the parameters of SARs and `<list_term>`s are strings SARs read.
The triples `(<source_state> <target_state> <label>)` determine transitions.

#### Example of Predicate Definition

For instance, the predicates *sorted* and *nth* are defined as follows.

```
(define-fun |sorted|
    ((X0 (List Int))) Bool
    (
        (() (X0 (tail X0)))
        ((q_0) (q_0))
        (
            (q_0 q_0 (not (> l_0 l_1)))
        )
    )
)

(define-fun |nth|
    ((i0 Int) (x0 Int) (X0 (List Int))) Bool
    (
        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (= l_2 x_0)))
                    (q_1 q_1 true)
                )
            )
        )

        (exists ((EY0 (List Int)))
            (
                ((x0) ((insert i0 EY0) EY0 X0))
                ((q_0) (q_1 q_2))
                (
                    (q_0 q_0 (and (> l_0 0) (= l_0 (+ l_1 1))))
                    (q_0 q_1 (and (= l_0 0) (not (= l_2 x_0))))
                    (q_1 q_1 true)
                    (q_0 q_2 (< l_0 0))
                    (q_2 q_2 true)
                )
            )
        )
    )
)
```

The `<int_var>`s (resp. `<list_var>`s) are in the form of `[a-z]<num>` (resp. `[A-Z]<num>`).
The variables existentially quantified are in the form of `E[A-Z]<num>`.

The variable of the form `x_`*i* (resp. `l_`*i*) corresponds to *i*-th `int_term` (resp. `list_term`).

### Assertion

```
(assert
	(forall ( <uq_var>+ ) <formula>)
)
```

The `<uq_var>`s are either `<int_var>`s or `<list_var>`s.

The `<formula>` is a quantifier-free formula.
The operations you can use are following:

- operations for integer arithmetic
- constructors `nil` and `insert`
- destructors `head` and `tail`
- the equality of list

## Output

SAR-SMT checks whether the definitions of functions satisfy the formulas.
It outputs `sat` when the all formulas are satisfied.
