;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun ins (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (ins x (cons y ys))
                   (ite (< x y)
                        (cons x (cons y ys))
                        (cons y (ins x ys))))))

(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (= X0 nil)
             (not (= (ins x0 X0) (cons x0 nil)))))

(check-sat)
