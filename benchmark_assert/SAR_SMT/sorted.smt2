;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun nth (Int Lst Int) Bool)
(assert (forall ((n Int) (x Int)) (not (nth n nil x))))
(assert (forall ((x Int)) (nth 0 (cons x nil) x)))
(assert (forall ((n Int) (x Int) (y Int) (xs Lst))
                (=> (nth (- n 1) xs y)
                   (nth n (cons x xs) y))))


(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun i0 () Int)

(assert (and (sorted X0)
             (>= (head X0) 0)
             (nth i0 X0 x0)
             (not (>= x0 0))))

(check-sat)
