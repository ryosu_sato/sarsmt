;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun count (Int Lst) Int)
(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))


(declare-fun n0 () Int)
(declare-fun x0 () Int)

(assert (and (= n0 (count x0 nil))
             (not (= n0 0))))

(check-sat)
