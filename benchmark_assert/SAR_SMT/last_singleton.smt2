;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun last (Lst) Int)
(assert (forall ((x Int))
                (= (last nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (last (cons y nil)) y)))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (last (cons y (cons z xs))) (last (cons z xs)))))


(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (cons x0 X0))
             (= X0 nil)
             (not (= x0 (last Y0)))))

(check-sat)
