;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun count (Int Lst) Int)
(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))

(declare-fun c0 () Int)
(declare-fun c1 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)

(assert (and (not (= x0 y0))
             (= c0 (count x0 X0))
             (= c1 (count x0 (cons y0 X0)))
             (not (= c0 c1))))

(check-sat)
