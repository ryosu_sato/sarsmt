;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun take_total (Int Lst) Lst)
(assert (forall ((x Int)) (= (take_total x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take_total n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take_total (- n 1) xs))))))


(declare-fun count (Int Lst) Int)
(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))


(declare-fun c0 () Int)
(declare-fun c1 () Int)
(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (take_total n0 X0))
             (= c0 (count x0 X0))
             (= c1 (count x0 Y0))
             (not (<= c0 c1))))

(check-sat)
