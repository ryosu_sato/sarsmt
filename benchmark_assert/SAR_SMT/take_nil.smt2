;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun take (Int Lst) Lst)
(assert (forall ((x Int)) (= (take x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take (- n 1) xs))))))


(declare-fun n0 () Int)
(declare-fun X0 () Lst)

(assert (and (= X0 nil)
             (not (= nil (take n0 X0)))))

(check-sat)
