;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun count (Int Lst) Int)
(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))



(declare-fun c0 () Int)
(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (= n0 x0)
             (= c0 (count n0 X0))
             (not (= (+ c0 1) (count n0 (cons x0 X0))))))

(check-sat)
