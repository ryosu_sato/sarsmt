;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun forall_pos (Lst) Bool)
(assert (forall ((x Int)) (forall_pos nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_pos (cons x xs)) (and (>= x 0) (forall_pos xs)))))


(declare-fun scan_sum (Lst) Lst)
(assert (= (scan_sum nil) nil))
(assert (forall ((x Int))
                (= (scan_sum (cons x nil)) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (=> (= ys (scan_sum (cons y xs)))
                    (= (scan_sum (cons x (cons y xs)))
                       (cons (+ y (head ys)) ys)))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))


(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (scan_sum X0))
             (forall_pos X0)
             (not (forall_pos Y0))))

(check-sat)
