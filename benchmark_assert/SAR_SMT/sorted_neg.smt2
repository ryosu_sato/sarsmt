;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))


(declare-fun exists_neg (Lst) Bool)
(assert (forall ((x Int)) (not (exists_neg nil))))
(assert (forall ((x Int) (xs Lst) (ys Lst))
                (= (exists_neg (cons x xs)) (or (< x 0) (exists_neg xs)))))


(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (sorted (cons x0 X0))
             (exists_neg X0)
             (>= x0 0)))

(check-sat)
