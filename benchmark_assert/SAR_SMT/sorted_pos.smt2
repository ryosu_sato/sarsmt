;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun forall_pos (Lst) Bool)
(assert (forall ((x Int)) (forall_pos nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_pos (cons x xs)) (and (>= x 0) (forall_pos xs)))))

(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (sorted (cons x0 X0))
             (>= x0 0)
             (not (forall_pos X0))))

(check-sat)
