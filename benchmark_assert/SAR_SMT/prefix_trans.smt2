;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun prefix (Lst Lst) Bool)
(assert (forall ((xs Lst))
                (prefix nil xs)))
(assert (forall ((x Int) (xs Lst))
                (not (prefix (cons x xs) nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (prefix (cons x xs) (cons y ys))
                   (and (= x y) (prefix xs ys)))))



(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)

(assert (and (prefix X0 Y0)
             (prefix Y0 Z0)
             (not (prefix X0 Z0))))

(check-sat)
