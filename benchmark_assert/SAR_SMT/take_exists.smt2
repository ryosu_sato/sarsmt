;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun take (Int Lst) Lst)
(assert (forall ((x Int)) (= (take x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take (- n 1) xs))))))

(declare-fun haszero (Lst) Bool)
(assert (forall ((x Int)) (not (haszero nil))))
(assert (forall ((x Int) (xs Lst) (ys Lst))
                (= (haszero (cons x xs)) (or (= x 0) (haszero xs)))))


(declare-fun n0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= X0 (take n0 Y0))
             (haszero X0)
             (not (haszero Y0))))

(check-sat)
