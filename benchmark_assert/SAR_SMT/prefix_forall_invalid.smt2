;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun prefix (Lst Lst) Bool)
(assert (forall ((xs Lst))
                (prefix nil xs)))
(assert (forall ((x Int) (xs Lst))
                (not (prefix (cons x xs) nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (prefix (cons x xs) (cons y ys))
                   (and (= x y) (prefix xs ys)))))

(declare-fun forall_pos (Lst) Bool)
(assert (forall ((x Int)) (forall_pos nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_pos (cons x xs)) (and (>= x 0) (forall_pos xs)))))


(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (prefix X0 Y0)
             (forall_pos X0)
             (not (forall_pos Y0))))

(check-sat)
