;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun take_total (Int Lst) Lst)
(assert (forall ((x Int)) (= (take_total x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take_total n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take_total (- n 1) xs))))))


(declare-fun forall_pos (Lst) Bool)
(assert (forall ((x Int)) (forall_pos nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_pos (cons x xs)) (and (>= x 0) (forall_pos xs)))))


(declare-fun sumfold (Lst) Int)
(assert (forall ((x Int))
                (= (sumfold nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (sumfold (cons y ys))
                   (+ y (sumfold ys)))))


(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun n2 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (take_total n2 X0))
             (forall_pos X0)
             (= n0 (sumfold Y0))
             (= n1 (sumfold X0))
             (not (<= n0 n1))))

(check-sat)
