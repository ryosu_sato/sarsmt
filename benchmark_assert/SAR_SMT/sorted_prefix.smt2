;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun prefix (Lst Lst) Bool)
(assert (forall ((xs Lst))
                (prefix nil xs)))
(assert (forall ((x Int) (xs Lst))
                (not (prefix (cons x xs) nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (prefix (cons x xs) (cons y ys))
                   (and (= x y) (prefix xs ys)))))


(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (sorted Y0)
             (prefix X0 Y0)
             (not (sorted X0))))

(check-sat)
