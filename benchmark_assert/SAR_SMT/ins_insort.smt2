;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))


(declare-fun insort (Int Lst) Lst)
(assert (forall ((x Int))
                (= (insort x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (insort x (cons y ys))
                   (ite (<= x y)
                        (cons x (cons y ys))
                        (cons y (insort x ys))))))

(declare-fun ins (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (ins x (cons y ys))
                   (ite (< x y)
                        (cons x (cons y ys))
                        (cons y (ins x ys))))))

(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)

(assert (and (sorted X0)
             (= Y0 (ins x0 X0))
             (= Z0 (insort x0 X0))
             (not (= Y0 Z0))))

(check-sat)
