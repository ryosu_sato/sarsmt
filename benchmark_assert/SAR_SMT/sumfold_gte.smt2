;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun forall_ge (Int Lst) Bool)
(assert (forall ((x Int)) (forall_ge x nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_ge x (cons y xs)) (and (>= y x) (forall_ge x xs)))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun sumfold (Lst) Int)
(assert (forall ((x Int))
                (= (sumfold nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (sumfold (cons y ys))
                   (+ y (sumfold ys)))))

(declare-fun x0 () Int)
(declare-fun n0 () Int)
(declare-fun s0 () Int)
(declare-fun X0 () Lst)

(assert (and (forall_ge x0 X0)
             (= n0 (len X0))
             (= s0 (sumfold X0))
             (not (>= s0 (* x0 n0)))))

(check-sat)
; unsolvable
