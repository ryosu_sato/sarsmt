;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun prefix (Lst Lst) Bool)
(assert (forall ((xs Lst))
                (prefix nil xs)))
(assert (forall ((x Int) (xs Lst))
                (not (prefix (cons x xs) nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (prefix (cons x xs) (cons y ys))
                   (and (= x y) (prefix xs ys)))))

(declare-fun nth (Int Lst Int) Bool)
(assert (forall ((n Int) (x Int)) (not (nth n nil x))))
(assert (forall ((x Int)) (nth 0 (cons x nil) x)))
(assert (forall ((n Int) (x Int) (y Int) (xs Lst))
                (=> (nth (- n 1) xs y)
                   (nth n (cons x xs) y))))


(declare-fun i0 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (prefix X0 Y0)
             (nth i0 X0 x0)
             (nth i0 Y0 y0)
             (not (= x0 y0))))

(check-sat)
