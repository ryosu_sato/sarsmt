;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (= n0 (len (cons x0 X0)))
             (= n1 (len X0))
             (not (= n0 n1))))

(check-sat)
