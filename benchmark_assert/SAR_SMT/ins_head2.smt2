;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun ins (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (ins x (cons y ys))
                   (ite (< x y)
                        (cons x (cons y ys))
                        (cons y (ins x ys))))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)

(assert (and (>= x0 y0)
             (= Y0 (ins x0 X0))
             (not (= (ins x0 (cons y0 X0)) (cons y0 Y0)))))

(check-sat)
