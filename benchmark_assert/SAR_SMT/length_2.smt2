;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))


(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)

(assert (and (= n0 (len (cons x0 (cons x1 nil))))
             (not (>= n0 3))))

(check-sat)
