;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun prefix (Lst Lst) Bool)
(assert (forall ((xs Lst))
                (prefix nil xs)))
(assert (forall ((x Int) (xs Lst))
                (not (prefix (cons x xs) nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (prefix (cons x xs) (cons y ys))
                   (and (= x y) (prefix xs ys)))))


(declare-fun forall_eq (Int Lst) Bool)
(assert (forall ((x Int)) (forall_eq x nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (forall_eq x (cons y xs)) (and (= x y) (forall_eq x xs)))))


(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (prefix X0 (cons x0 X0)))

(check-sat)
