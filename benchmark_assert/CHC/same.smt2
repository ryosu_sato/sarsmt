;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun same (Int Lst) Bool)
(assert (forall ((x Int)) (same x nil)))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (same x (cons y xs)) (and (= x y) (same x xs)))))

(declare-fun has (Int Lst) Bool)
(assert (forall ((x Int))
                (not (has x nil))))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (has x (cons y xs)) (or (= x y) (has x xs)))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)
(declare-fun W0 () Lst)

(assert (or
         (not (|same| x0 nil))
         (and (|same| x0 X0)
              (not (|same| x0 (cons x0 X0))))
         (not (|has| x0 (cons x0 X0)))
         (and (|has| x0 X0)
              (not (|has| x0 (cons y0 X0))))
         (and
          (|same| x0 X0)
          (|has| (+ x0 1) X0))))


(check-sat)
