;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun prefilter (Lst) Bool)
(assert (forall ((xs Lst)) (prefilter xs)))

(declare-fun postfilter (Lst Lst) Bool)
(assert (forall ((xs Lst)) (postfilter xs nil)))
(assert (forall ((x Int) (xs Lst))
                (not (postfilter nil (cons x xs)))))
(assert (forall ((x Int) (xs Lst) (y Int) (ys Lst))
                (= (postfilter (cons x xs) (cons y ys))
                   (postfilter xs ys))))

(declare-fun prelength (Lst) Bool)
(assert (forall ((xs Lst)) (prelength xs)))


(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun postlength (Int Lst) Bool)
(assert (forall ((n Int) (xs Lst))
                (= n (len xs))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)

(assert
  (or
;; filter
    (and (prefilter X0)
         (= X0 nil)
         (not (postfilter X0 nil)))

    (and (prefilter X0)
         (= X0 nil)
         (not (postfilter X0 nil)))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (not (prefilter Y0)))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (postfilter Y0 Z0)
         (not (= b0 0))
         (not (postfilter X0 (cons y0 Z0))))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (postfilter Y0 Z0)
         (= b0 0)
         (not (postfilter X0 Z0)))

;; length
    (and (prelength X0)
         (= X0 nil)
         (not (postlength 0 X0)))

    (and (prelength X0)
         (= X0 (cons y0 Y0))
         (not (prelength Y0)))

    (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
         (not (postlength (+ 1 n0) X0)))

;; main
    (not (prefilter X0))

    (postfilter X0 Y0)
    (not (prelength X0))

    (and (postfilter X0 Y0)
         (postlength n0 X0)
         (not (prelength Y0)))

    (and (postfilter X0 Y0)
         (postlength n0 X0)
         (postlength m0 Y0)
         (not (>= n0 m0)))))

(check-sat)
