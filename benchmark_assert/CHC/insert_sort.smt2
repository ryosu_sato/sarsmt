;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun preinsert (Int Lst) Bool)
(assert (forall ((x Int) (xs Lst))
                (= (preinsert x xs) (sorted xs))))

(declare-fun postinsert (Int Lst Lst) Bool)
(assert (forall ((x Int) (xs Lst) (ys Lst))
                (= (postinsert x xs ys)
                   (and (sorted ys)
                        (or (<= x (head ys))
                            (<= (head xs) (head ys)))))))

(declare-fun preinsertsort (Lst) Bool)
(assert (forall ((xs Lst)) (preinsertsort xs)))

(declare-fun postinsertsort (Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst))
                (= (postinsertsort xs ys)
                   (sorted ys))))

(declare-fun presorted (Lst) Bool)
(assert (forall ((xs Lst)) (presorted xs)))

(declare-fun postsorted (Int Lst) Bool)
(assert (forall ((b Int) (xs Lst))
                (= (postsorted b xs)
                   (=> (sorted xs) (not (= b 0))))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)
(declare-fun W0 () Lst)

(assert
  (or
;; sorted
    (and (presorted X0)
         (= X0 nil)
         (not (= x0 0))
         (not (postsorted x0 X0)))

    (and (presorted X0)
         (= X0 (cons x1 nil))
         (not (= x0 0))
         (not (postsorted x0 X0)))

    (and (presorted X0)
         (= X0 (cons y1 (cons y2 Y0)))
         (<= y1 y2)
         (not (presorted (cons y2 Y0))))

    (and (presorted X0)
         (= X0 (cons x1 (cons x2 Y0)))
         (<= x1 x2)
         (postsorted b0 (cons x2 Y0))
         (not (postsorted b0 X0)))

    (and (presorted X0)
         (= X0 (cons x1 (cons x2 Y0)))
         (not (<= x1 x2))
         (not (postsorted 0 X0)))

;; insert
    (and (preinsert x1 X0)
         (= X0 nil)
         (not (postinsert x1 X0 (cons x1 nil))))

    (and (preinsert x1 X0)
         (= X0 (cons y1 Y0))
         (<= x1 y1)
         (not (postinsert x1 X0 (cons x1 X0))))

    (and (preinsert x1 X0)
         (= X0 (cons y1 Y0))
         (not (<= x1 y1))
         (postinsert x1 Y0 Z0)
         (not (postinsert x1 X0 (cons y1 Z0))))

; insert_sort
    (and (preinsertsort X0) (= X0 nil)
         (not (postinsertsort X0 nil)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0))
         (not (preinsertsort Y0)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0)) (postinsertsort Y0 Z0)
         (not (preinsert y1 Z0)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0)) (postinsertsort Y0 Z0) (postinsert y1 Z0 W0)
         (not (postinsertsort X0 Z0)))

;; main
    (not (preinsertsort X0))

    (and (postinsertsort X0 Y0)
         (not (presorted Y0)))

    (and (postinsertsort X0 Y0) (postsorted b0 Y0)
         (= b0 0))))

(check-sat)
