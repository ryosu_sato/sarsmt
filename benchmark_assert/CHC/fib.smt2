;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec fib ((xs list)) Bool
    (match xs (
        (nil false)
        ((cons x1 xs1)
            (match xs1 (
                (nil false)
                ((cons x2 xs2)
                    (match xs2 (
                        (nil (= x1 x2 1))
                        ((cons x3 xs3) (and (= x1 (+ x2 x3)) (fib xs1))))))))))))

(define-fun-rec len3x ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs1)
            (match xs1 (
                (nil false)
                ((cons x2 xs2)
                    (match xs2 (
                        (nil false)
                        ((cons x3 xs3) (len3x xs3)))))))))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun z0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (or (not (fib (cons 1 (cons 1 nil))))
            (and (= x0 (+ y0 z0))
                 (fib (cons y0 (cons z0 X0)))
                 (not (fib (cons x0 (cons y0 (cons z0 X0))))))
            (not (len3x nil))
            (and (len3x X0)
                 (not (len3x (cons x0 (cons y0 (cons z0 X0))))))
            (and (fib (cons x0 X0))
                 (len3x (cons x0 X0))
                 (= 1 (mod x0 2)))))

(check-sat)
