;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun premerge (Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst))
                (= (premerge xs ys)
                   (and (sorted xs)
                        (sorted ys)))))

(declare-fun postmerge (Lst Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst) (zs Lst))
                (= (postmerge xs ys zs)
                   (and (sorted zs)
                        (or (<= (head ys) (head zs))
                            (<= (head xs) (head zs)))))))

(declare-fun presorted (Lst) Bool)
(assert (forall ((xs Lst)) (presorted xs)))

(declare-fun postsorted (Int Lst) Bool)
(assert (forall ((b Int) (xs Lst))
                (= (postsorted b xs)
                   (=> (sorted xs) (not (= b 0))))))

(declare-fun presplit (Lst) Bool)
(assert (forall ((xs Lst)) (presplit xs)))

(declare-fun postsplit (Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst))
                (= (postsplit xs ys)
                   (sorted ys))))

(declare-fun prek (Lst Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst) (zs Lst)) (prek xs ys zs)))

(declare-fun postk (Lst Lst Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst) (zs Lst) (ws Lst))
                (= (postk xs ys zs ws)
                   (sorted ws))))

(declare-fun prescont (Int Int Lst Lst Lst Lst) Bool)
(assert (forall ((y1 Int) (y2 Int) (xs Lst) (ys Lst) (zs1 Lst) (zs2 Lst)) (prescont y1 y2 xs ys zs1 zs2)))

(declare-fun postscont (Int Int Lst Lst Lst Lst Lst) Bool)
(assert (forall ((y1 Int) (y2 Int) (xs Lst) (ys Lst) (zs1 Lst) (zs2 Lst) (zs3 Lst))
                (= (postscont y1 y2 xs ys zs1 zs2 zs3)
                   (sorted zs3))))

(declare-fun premergesort (Lst) Bool)
(assert (forall ((xs Lst)) (premergesort xs)))

(declare-fun postmergesort (Lst Lst) Bool)
(assert (forall ((xs Lst) (ys Lst))
                (= (postmergesort xs ys)
                   (sorted ys))))

(declare-fun premscont (Int Int Lst Lst Lst Lst) Bool)
(assert (forall ((y1 Int) (y2 Int) (xs Lst) (ys Lst) (zs1 Lst) (zs2 Lst)) (premscont y1 y2 xs ys zs1 zs2)))

(declare-fun postmscont (Int Int Lst Lst Lst Lst Lst) Bool)
(assert (forall ((y1 Int) (y2 Int) (xs Lst) (ys Lst) (zs1 Lst) (zs2 Lst) (zs3 Lst))
                (= (postmscont y1 y2 xs ys zs1 zs2 zs3)
                   (sorted zs3))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)
(declare-fun W0 () Lst)


(assert
  (or
;; merge
      (and (premerge X0 Y0)
           (= Y0 nil)
           (not (postmerge X0 Y0 X0)))

      (and (premerge X0 Y0) (= Y0 (cons y0 Y1)) (= X0 nil)
           (not (postmerge X0 Y0 Y0)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (<= x0 y0)
           (not (premerge X1 Y0)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (<= x0 y0) (postmerge X1 Y0 Z0)
           (not (postmerge X0 Y0 (cons x0 Z0))))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (not (<= x0 y0))
           (not (premerge X0 Y1)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (not (<= x0 y0)) (postmerge X0 Y1 Z0)
           (not (postmerge X0 Y0 (cons y0 Z0))))


;; split
      (and (presplit X0) (= X0 nil)
           (not (prek X0 nil nil)))

      (and (presplit X0) (= X0 nil) (postk X0 nil nil Y0)
           (not (postsplit X0 Y0)))

      (and (presplit X0) (= X0 (cons x0 nil))
           (not (prek X0 (cons x0 nil) nil)))

      (and (presplit X0) (= X0 (cons x0 nil)) (postk X0 (cons x0 nil) nil Y0)
           (not (postsplit X0 Y0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0)))
           (not (presplit Y0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prek X0 Z1 Z2)
           (not (prescont y1 y2 X0 Y0 Z1 Z2)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prek X0 Z1 Z2) (postscont y1 y2 X0 Y0 Z1 Z2 W0)
           (not (postk X0 Z1 Z2 W0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (postsplit Y0 Z0)
           (not (postsplit X0 Z0)))

;; scont
      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prescont y1 y2 X0 Y0 Z1 Z2)
           (not (prek X0 (cons y1 Z1) (cons y2 Z2))))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prescont y1 y2 X0 Y0 Z1 Z2) (postk X0 (cons y1 Z1) (cons y2 Z2) W0)
           (not (postscont y1 y2 X0 Y0 Z1 Z2 W0)))


;; merge_sort
      (and (premergesort X0) (= X0 nil)
           (not (postmergesort X0 nil)))

      (and (premergesort X0) (= X0 (cons x0 nil))
           (not (postmergesort X0 (cons x0 nil))))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1)))
           (not (presplit X0)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (prek X0 Y1 Y2)
           (not (premscont x0 x1 X0 Y0 Y1 Y2)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (prek X0 Y1 Y2) (postmscont y1 y2 X0 Y0 Y1 Y2 Z0)
           (not (postk X0 Y1 Y2 Z0)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (postsplit Y0 Z0)
           (not (postmergesort X0 Z0)))

;; mscont
      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2)
           (not (premergesort Y1)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2) (postmergesort Y1 Z1)
           (not (premergesort Y2)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2) (postmergesort Y1 Z1) (postmergesort Y2 Z2)
           (not (premerge Z1 Z2)))


;; sorted
      (and (presorted X0)
           (= X0 nil)
           (not (= x0 0))
           (not (postsorted x0 X0)))

      (and (presorted X0)
           (= X0 (cons x1 nil))
           (not (= x0 0))
           (not (postsorted x0 X0)))

      (and (presorted X0)
           (= X0 (cons y1 (cons y2 Y0)))
           (<= y1 y2)
           (not (presorted (cons y2 Y0))))

      (and (presorted X0)
           (= X0 (cons x1 (cons x2 Y0)))
           (<= x1 x2)
           (postsorted b0 (cons x2 Y0))
           (not (postsorted b0 X0)))

      (and (presorted X0)
           (= X0 (cons x1 (cons x2 Y0)))
           (not (<= x1 x2))
           (not (postsorted 0 X0)))


;; main
      (not (premergesort X0))

      (and (postmergesort X0 Y0)
           (not (presorted Y0)))

      (and (postmergesort X0 Y0) (postsorted b0 Y0))
           (= b0 0)))

(check-sat)
