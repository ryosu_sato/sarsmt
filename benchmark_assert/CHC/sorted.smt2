;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun sorted (Lst) Bool)
(assert (sorted nil))
(assert (forall ((x Int))
                (sorted (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst))
                (= (sorted (cons x (cons y xs)))
                   (and (<= x y) (sorted (cons y xs))))))

(declare-fun haszero (Lst) Bool)
(assert (forall ((x Int)) (not (haszero nil))))
(assert (forall ((x Int) (xs Lst) (ys Lst))
                (= (haszero (cons x xs)) (or (= x 0) (haszero xs)))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)
(declare-fun W0 () Lst)


(assert (or
         (not (|sorted| nil))
         (not (|sorted| (cons x0 nil)))
         (and
          (<= x0 y0)
          (|sorted| (cons y0 X0))
          (not (|sorted| (cons x0 (cons y0 X0)))))
         (not (|haszero| (cons 0 X0)))
         (and (|haszero| X0)
              (not (|haszero| (cons x0 X0))))
         (and (|sorted| (cons 1 X0))
              (|haszero| X0))))


(check-sat)
