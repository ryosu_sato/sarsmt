;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun premake (Int) Bool)
(assert (forall ((x Int)) (premake x)))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun postmake (Int Lst) Bool)
(assert (forall ((n Int) (xs Lst))
                (= (postmake n xs) (= n (len xs)))))

(declare-fun prelength (Lst) Bool)
(assert (forall ((xs Lst)) (prelength xs)))

(declare-fun postlength (Int Lst) Bool)
(assert (forall ((n Int) (xs Lst))
                (= (postlength n xs) (= n (len xs)))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Z0 () Lst)
(declare-fun W0 () Lst)


(assert
  (or
;; make
      (and (premake n0)
           (= n0 0)
           (not (postmake n0 nil)))

      (and (premake n0)
           (not (= n0 0))
           (not (premake (- n0 1))))

      (and (premake n0)
           (not (= n0 0))
           (postmake (- n0 1) X0)
           (not (postmake n0 (cons n0 X0))))

;; length
     (and (prelength X0)
          (= X0 nil)
          (not (postlength 0 X0)))

     (and (prelength X0)
          (= X0 (cons y0 Y0))
          (not (prelength Y0)))

     (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
          (not (postlength (+ 1 n0) X0)))

;; main
     (not (premake n0))

     (and (postmake n0 X0)
          (not (prelength X0)))

     (and (postmake n0 X0) (postlength m0 X0)
          (not (= n0 m0)))))

(check-sat)
