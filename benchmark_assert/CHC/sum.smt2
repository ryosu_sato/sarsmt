;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun sum (Lst Lst) Lst)
(assert (= (sum nil nil) nil))
(assert (forall ((x Int) (xs Lst) (y Int) (ys Lst))
                (= (sum (cons x xs) (cons y ys))
                   (cons (+ x y) (sum xs ys)))))


(declare-fun nonzero (Lst) Bool)
(assert (forall ((x Int)) (not (nonzero nil))))
(assert (forall ((x Int) (xs Lst) (ys Lst))
                (= (nonzero (cons x xs)) (or (not (= x 0)) (nonzero xs)))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun z0 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)

(assert (or
         (not (= (|sum| nil nil) nil))
         (and (= (+ x0 y0) z0)
              (= (|sum| X0 Y0) Z0)
              (not (= (|sum| (cons x0 X0) (cons y0 Y0)) (cons z0 Z0))))
         (and (not (= x0 0))
              (not (|nonzero| (cons x0 X0))))
         (and (|nonzero| X0)
              (not (|nonzero| (cons x0 X0))))
         (and (|nonzero| X0)
              (= (|sum| X0 X0) X0))))


(check-sat)
