;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0))
   (((cons (head Int) (tail Lst)) (nil))))

(declare-fun presnoc (Int Lst) Bool)
(assert (forall ((x Int) (xs Lst)) (presnoc x xs)))

(declare-fun postsnoc (Int Lst Lst) Bool)
(assert (forall ((x Int)) (not (postsnoc x nil nil))))
(assert (forall ((y Int) (x Int))
                (postsnoc y nil (cons x nil))))
(assert (forall ((z Int) (x Int) (xs Lst) (y Int) (ys Lst))
                (= (postsnoc z (cons x xs) (cons y ys))
                   (postsnoc z xs ys))))

(declare-fun prereverse (Lst) Bool)
(assert (forall ((xs Lst)) (prereverse xs)))

(declare-fun postreverse (Lst Lst) Bool)
(assert (postreverse nil nil))
(assert (forall ((x Int) (xs Lst) (y Int) (ys Lst))
                (= (postreverse (cons x xs) (cons y ys))
                   (postreverse xs ys))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))


(declare-fun prelength (Lst) Bool)
(assert (forall ((xs Lst)) (prelength xs)))

(declare-fun postlength (Int Lst) Bool)
(assert (forall ((n Int) (xs Lst))
                (= n (len xs))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)
(declare-fun W0 () Lst)


(assert
  (or
;; snoc
     (and (presnoc x0 X0)
          (= X0 nil)
          (not (postsnoc x0 X0 (cons x0 nil))))

     (and (presnoc x0 X0)
          (= X0 (cons y0 Y0))
          (not (presnoc x0 Y0)))

     (and (presnoc x0 X0) (= X0 (cons y0 Y0)) (postsnoc x0 Y0 Z0)
          (not (postsnoc x0 X0 (cons y0 Z0))))


;; reverse
     (and (prereverse X0)
          (= X0 nil)
          (not (postreverse X0 nil)))

     (and (prereverse X0) (= X0 (cons y0 Y0))
          (not (prereverse Y0)))

     (and (prereverse X0) (= X0 (cons y0 Y0)) (postreverse Y0 Z0)
          (not (presnoc y0 Z0)))

     (and (prereverse X0)
          (= X0 (cons y0 Y0))
          (postreverse Y0 Z0)
          (postsnoc y0 Z0 Z1)
          (not (postreverse X0 Z1)))

;; length
     (and (prelength X0)
          (= X0 nil)
          (not (postlength 0 X0)))

     (and (prelength X0)
          (= X0 (cons y0 Y0))
          (not (prelength Y0)))

     (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
          (not (postlength (+ 1 n0) X0)))

;; main
     (not (prereverse X0))

     (and (postreverse X0 Y0)
          (not (prelength X0)))

     (and (postreverse X0 Y0) (postlength n0 X0)
          (not (prelength Y0)))

     (and (postreverse X0 Y0) (postlength n0 X0) (postlength m0 Y0)
          (not (= n0 m0)))))

(check-sat)
