;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun leq (Lst Lst) Bool)
(assert (forall ((x Int))
                (leq nil nil)))
(assert (forall ((x Int) (xs Lst) (y Int) (ys Lst))
                (= (leq (cons x xs) (cons y ys))
                   (and (<= x y) (leq xs ys)))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () Lst)
(declare-fun X1 () Lst)
(declare-fun Y0 () Lst)
(declare-fun Y1 () Lst)
(declare-fun Y2 () Lst)
(declare-fun Z0 () Lst)
(declare-fun Z1 () Lst)
(declare-fun Z2 () Lst)
(declare-fun W0 () Lst)



(assert (or
         (not (|leq| nil nil))
         (and (<= x0 y0)
              (|leq| X0 Y0)
              (not (|leq| (cons x0 X0) (cons y0 Y0))))
         (and (|leq| X0 Y0)
              (|leq| Y0 X0)
              (not (= X0 Y0)))))

(check-sat)
