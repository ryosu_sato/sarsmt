;;; INFO: mem x (ins1 x xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun elem (Int Lst) Bool)
(assert (forall ((x Int))
                (not (elem x nil))))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (elem x (cons y xs)) (or (= x y) (elem x xs)))))

(declare-fun ins1 (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins1 x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (ins1 x (cons y xs))
                   (ite (= x y)
                        (cons y xs)
                        (cons y (ins1 x ys))))))

(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (ins1 x0 X0))
             (not (elem x0 Y0))))

(check-sat)
