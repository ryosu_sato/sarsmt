;;; INFO: ys = butlast xs /\ n = len xs => ys = take (n-1) xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun butlast (Lst) Lst)
(assert (= (butlast nil) nil))
(assert (forall ((x Int))
                (= (butlast (cons x nil)) nil)))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (butlast (cons y (cons z xs))) (butlast (cons z xs)))))

(declare-fun take (Int Lst) Lst)
(assert (forall ((x Int)) (= (take x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take (- n 1) xs))))))

(declare-fun n0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (butlast X0))
             (= n0 (len X0))
             (not (= Y0 (take (- n0 1) X0)))))
(assert (= nil (take (- 0 1) nil)))

(check-sat)
