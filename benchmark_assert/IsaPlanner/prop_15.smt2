;;; INFO: length (ins x xs) = 1 + length xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun ins (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (ins x (cons y ys))
                   (ite (< x y)
                        (cons x (cons y ys))
                        (cons y (ins x ys))))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun n1 () Int)
(declare-fun n2 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (ins x0 Y0))
             (= n1 (len X0))
             (= n2 (len Y0))
             (not (= (+ n1 1) n2))))

(check-sat)
