;;; INFO: count n xs + count n [m] = count n (m : xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun count (Int Lst) Int)
(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))


(declare-fun n0 () Int)
(declare-fun c1 () Int)
(declare-fun c2 () Int)
(declare-fun c3 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (= c1 (count n0 X0))
             (= c2 (count n0 (cons x0 nil)))
             (= c3 (count n0 (cons x0 X0)))
             (not (= c3 (+ c1 c2)))))

(check-sat)
