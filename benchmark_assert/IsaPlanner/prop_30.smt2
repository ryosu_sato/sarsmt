;;; INFO: mem x (ins x xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun elem (Int Lst) Bool)
(assert (forall ((x Int))
                (not (elem x nil))))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (elem x (cons y xs)) (or (= x y) (elem x xs)))))


(declare-fun ins (Int Lst) Lst)
(assert (forall ((x Int))
                (= (ins x nil) (cons x nil))))
(assert (forall ((x Int) (y Int) (xs Lst) (ys Lst))
                (= (ins x (cons y ys))
                   (ite (< x y)
                        (cons x (cons y ys))
                        (cons y (ins x ys))))))

(declare-fun x0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (ins x0 X0))
             (not (elem x0 Y0))))

(check-sat)
