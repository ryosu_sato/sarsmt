;;; INFO: take 0 xs = []
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun take (Int Lst) Lst)
(assert (forall ((x Int)) (= (take x nil) nil)))
(assert (forall ((n Int) (x Int) (xs Lst))
                (= (take n (cons x xs))
                   (ite (<= n 0)
                        nil
                        (cons x (take (- n 1) xs))))))

(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (take 0 X0))
             (not (= Y0 nil))))

(check-sat)
