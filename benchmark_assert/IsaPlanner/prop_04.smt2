;;; INFO: 1 + count n xs = count n (n::xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun count (Int Lst) Int)

(assert (forall ((x Int)) (= (count x nil) 0)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst))
                (= (count x (cons y xs2)) (ite (= x y) (+ 1 (count x xs2)) (count x xs2)))))

(assert
  (not
    (forall ((c1 Int) (c2 Int) (n0 Int) (L0 Lst))
      (=>
        (and (= c1 (count n0 L0))
             (= c2 (count n0 (cons n0 L0))))
        (= (+ c1 1) c2)))))

(check-sat)
