;;; INFO: xs <> [] => last (x:xs) = last xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun last (Lst) Int)
(assert (forall ((x Int))
                (= (last nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (last (cons y nil)) y)))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (last (cons y (cons z xs))) (last (cons z xs)))))


(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (cons x0 X0))
             (not (= X0 nil))
             (= y0 (last X0))
             (not (= y0 (last Y0)))))

(check-sat)
