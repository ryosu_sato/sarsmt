;;; INFO: len (butlast xs) = len xs - 1 (* 0-1 = 0 *)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))


(declare-fun butlast (Lst) Lst)
(assert (= (butlast nil) nil))
(assert (forall ((x Int))
                (= (butlast (cons x nil)) nil)))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (butlast (cons y (cons z xs))) (butlast (cons z xs)))))

(declare-fun len (Lst) Int)
(assert (forall ((x Int))
                (= (len nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (len (cons y ys))
                   (+ 1 (len ys)))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun X0 () Lst)
(declare-fun Y0 () Lst)

(assert (and (= Y0 (butlast X0))
             (= n0 (len Y0))
             (= n1 (len X0))
             (not (or (= n0 (- n1 1)) (and (= n0 0) (= n1 0))))))

(check-sat)
