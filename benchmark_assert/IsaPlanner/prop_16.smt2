;;; INFO: xs = [] => last (x:xs) = x
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun last (Lst) Int)
(assert (forall ((x Int))
                (= (last nil) 0)))
(assert (forall ((x Int) (y Int) (ys Lst))
                (= (last (cons y nil)) y)))
(assert (forall ((x Int) (y Int) (z Int) (xs Lst) (ys Lst))
                (= (last (cons y (cons z xs))) (last (cons z xs)))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () Lst)

(assert (and (= X0 nil)
             (= y0 (last (cons x0 X0)))
             (not (= x0 y0))))

(check-sat)
