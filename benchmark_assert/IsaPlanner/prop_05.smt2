;;; INFO: n = x => 1 + count n xs = count n (x::xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((Lst 0)) (((cons (head Int) (tail Lst)) (nil))))

(declare-fun count (Int Int Lst) Bool)
(assert (forall ((x Int)) (count 0 x nil)))
(assert (forall ((c Int) (x Int) (y Int) (xs Lst) (xs2 Lst)) (=> (>= c 0) (= (count c x (cons y xs2)) (ite (= x y) (count (- c 1) x xs2) (count c x xs2))))))

(declare-fun c1 () Int)
(declare-fun c2 () Int)
(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () Lst)

(assert (and (= n0 x0) (>= c1 0) (>= c2 0)
             (count c1 n0 X0)
             (count c2 n0 (cons x0 X0))
             (not (= (+ 1 c1) c2))))

(check-sat)
