;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))

(define-fun-rec sorted ((xs List)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))


(define-fun-rec haszero ((xs List)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (= x 0) (haszero xs2))))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun X1 () List)
(declare-fun Y0 () List)
(declare-fun Y1 () List)
(declare-fun Y2 () List)
(declare-fun Z0 () List)
(declare-fun Z1 () List)
(declare-fun Z2 () List)
(declare-fun W0 () List)


(assert (or
         (not (|sorted| nil))
         (not (|sorted| (cons x0 nil)))
         (and
          (<= x0 y0)
          (|sorted| (cons y0 X0))
          (not (|sorted| (cons x0 (cons y0 X0)))))
         (not (|haszero| (cons 0 X0)))
         (and (|haszero| X0)
              (not (|haszero| (cons x0 X0))))
         (and (|sorted| (cons 1 X0))
              (|haszero| X0))))

(check-sat)
