;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))

(define-fun-rec sorted ((xs List)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun premerge ((xs List) (ys List)) Bool
  (and (sorted xs)
       (sorted ys)))

(define-fun postmerge ((xs List) (ys List) (zs List)) Bool
  (and (sorted zs)
       (match zs ((nil (= xs ys nil))
                  ((cons z zs2) (or (and (not (= ys nil)) (<= (head ys) z))
                                    (and (not (= xs nil)) (<= (head xs) z))))))))

(define-fun presorted ((xs List)) Bool true)

(define-fun postsorted ((b Int) (xs List)) Bool
  (=> (sorted xs) (not (= b 0))))

(define-fun presplit ((xs List)) Bool true)

(define-fun postsplit ((xs List) (ys List)) Bool
  (sorted ys))

(define-fun prek ((xs List) (ys List) (zs List)) Bool true)

(define-fun postk ((xs List) (ys List) (zs List) (ws List)) Bool
  (sorted ws))

(define-fun prescont ((x Int) (y Int) (xs List) (ys List) (zs List) (ws List)) Bool true)

(define-fun postscont ((x Int) (y Int) (xs List) (ys List) (zs List) (ws List) (rs List)) Bool
  (sorted rs))

(define-fun premergesort ((xs List)) Bool true)

(define-fun postmergesort ((xs List) (ys List)) Bool
  (sorted ys))

(define-fun premscont ((x Int) (y Int) (xs List) (ys List) (zs List) (ws List)) Bool true)

(define-fun postmscont ((x Int) (y Int) (xs List) (ys List) (zs List) (ws List) (rs List)) Bool
  (sorted rs))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun X1 () List)
(declare-fun Y0 () List)
(declare-fun Y1 () List)
(declare-fun Y2 () List)
(declare-fun Z0 () List)
(declare-fun Z1 () List)
(declare-fun Z2 () List)
(declare-fun W0 () List)


(assert
  (or
;; merge
      (and (premerge X0 Y0)
           (= Y0 nil)
           (not (postmerge X0 Y0 X0)))

      (and (premerge X0 Y0) (= Y0 (cons y0 Y1)) (= X0 nil)
           (not (postmerge X0 Y0 Y0)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (<= x0 y0)
           (not (premerge X1 Y0)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (<= x0 y0) (postmerge X1 Y0 Z0)
           (not (postmerge X0 Y0 (cons x0 Z0))))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (not (<= x0 y0))
           (not (premerge X0 Y1)))

      (and (premerge X0 Y0) (= X0 (cons x0 X1)) (= Y0 (cons y0 Y1)) (not (<= x0 y0)) (postmerge X0 Y1 Z0)
           (not (postmerge X0 Y0 (cons y0 Z0))))


;; split
      (and (presplit X0) (= X0 nil)
           (not (prek X0 nil nil)))

      (and (presplit X0) (= X0 nil) (postk X0 nil nil Y0)
           (not (postsplit X0 Y0)))

      (and (presplit X0) (= X0 (cons x0 nil))
           (not (prek X0 (cons x0 nil) nil)))

      (and (presplit X0) (= X0 (cons x0 nil)) (postk X0 (cons x0 nil) nil Y0)
           (not (postsplit X0 Y0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0)))
           (not (presplit Y0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prek X0 Z1 Z2)
           (not (prescont y1 y2 X0 Y0 Z1 Z2)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prek X0 Z1 Z2) (postscont y1 y2 X0 Y0 Z1 Z2 W0)
           (not (postk X0 Z1 Z2 W0)))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (postsplit Y0 Z0)
           (not (postsplit X0 Z0)))

;; scont
      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prescont y1 y2 X0 Y0 Z1 Z2)
           (not (prek X0 (cons y1 Z1) (cons y2 Z2))))

      (and (presplit X0) (= X0 (cons y1 (cons y2 Y0))) (prescont y1 y2 X0 Y0 Z1 Z2) (postk X0 (cons y1 Z1) (cons y2 Z2) W0)
           (not (postscont y1 y2 X0 Y0 Z1 Z2 W0)))


;; merge_sort
      (and (premergesort X0) (= X0 nil)
           (not (postmergesort X0 nil)))

      (and (premergesort X0) (= X0 (cons x0 nil))
           (not (postmergesort X0 (cons x0 nil))))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1)))
           (not (presplit X0)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (prek X0 Y1 Y2)
           (not (premscont x0 x1 X0 Y0 Y1 Y2)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (prek X0 Y1 Y2) (postmscont y1 y2 X0 Y0 Y1 Y2 Z0)
           (not (postk X0 Y1 Y2 Z0)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (postsplit Y0 Z0)
           (not (postmergesort X0 Z0)))

;; mscont
      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2)
           (not (premergesort Y1)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2) (postmergesort Y1 Z1)
           (not (premergesort Y2)))

      (and (premergesort X0) (= X0 (cons x0 (cons x1 X1))) (premscont x0 x1 X0 X1 Y1 Y2) (postmergesort Y1 Z1) (postmergesort Y2 Z2)
           (not (premerge Z1 Z2)))


;; sorted
      (and (presorted X0)
           (= X0 nil)
           (not (= x0 0))
           (not (postsorted x0 X0)))

      (and (presorted X0)
           (= X0 (cons x1 nil))
           (not (= x0 0))
           (not (postsorted x0 X0)))

      (and (presorted X0)
           (= X0 (cons y1 (cons y2 Y0)))
           (<= y1 y2)
           (not (presorted (cons y2 Y0))))

      (and (presorted X0)
           (= X0 (cons x1 (cons x2 Y0)))
           (<= x1 x2)
           (postsorted b0 (cons x2 Y0))
           (not (postsorted b0 X0)))

      (and (presorted X0)
           (= X0 (cons x1 (cons x2 Y0)))
           (not (<= x1 x2))
           (not (postsorted 0 X0)))


;; main
      (not (premergesort X0))

      (and (postmergesort X0 Y0)
           (not (presorted Y0)))

      (and (postmergesort X0 Y0) (postsorted b0 Y0)
           (= b0 0))))

(check-sat)
