;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))


(define-fun-rec same ((x0 Int) (xs List)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (= x x0) (same x0 xs2))))))

(define-fun-rec has ((x Int) (xs List)) Bool
    (match xs (
        (nil false)
        ((cons y ys)
            (or (= x y) (has x ys))))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun X1 () List)
(declare-fun Y0 () List)
(declare-fun Y1 () List)
(declare-fun Y2 () List)
(declare-fun Z0 () List)
(declare-fun Z1 () List)
(declare-fun Z2 () List)
(declare-fun W0 () List)

(assert (or
         (not (|same| x0 nil))
         (and (|same| x0 X0)
              (not (|same| x0 (cons x0 X0))))
         (not (|has| x0 (cons x0 X0)))
         (and (|has| x0 X0)
              (not (|has| x0 (cons y0 X0))))
         (and
          (|same| x0 X0)
          (|has| (+ x0 1) X0))))

(check-sat)
