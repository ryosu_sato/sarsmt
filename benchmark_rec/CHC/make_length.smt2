;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))


(define-fun premake ((xs Int)) Bool true)

(define-fun-rec len ((xs List)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (len xs2))))))

(define-fun postmake ((n Int) (xs List)) Bool
             (= n (len xs)))

(define-fun prelength ((xs List)) Bool true)

(define-fun postlength ((n Int) (xs List)) Bool (= n (len xs)))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun Y0 () List)
(declare-fun Z0 () List)
(declare-fun W0 () List)


(assert
  (or
;; make
      (and (premake n0)
           (= n0 0)
           (not (postmake n0 nil)))

      (and (premake n0)
           (not (= n0 0))
           (not (premake (- n0 1))))

      (and (premake n0)
           (not (= n0 0))
           (postmake (- n0 1) X0)
           (not (postmake n0 (cons n0 X0))))

;; length
     (and (prelength X0)
          (= X0 nil)
          (not (postlength 0 X0)))

     (and (prelength X0)
          (= X0 (cons y0 Y0))
          (not (prelength Y0)))

     (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
          (not (postlength (+ 1 n0) X0)))

;; main
     (not (premake n0))

     (and (postmake n0 X0)
          (not (prelength X0)))

     (and (postmake n0 X0) (postlength m0 X0)
          (not (= n0 m0)))))

(check-sat)
