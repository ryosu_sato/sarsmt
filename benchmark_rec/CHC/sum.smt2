;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))


(define-fun-rec sum ((xs List) (ys List) (zs List)) Bool
    (match xs (
        (nil (= ys zs nil))
        ((cons x xs2)
            (match ys ((nil false)
                       ((cons y ys2)
                           (match zs ((nil false)
                                      ((cons z zs2) (and (= z (+ x y)) (sum xs2 ys2 zs2))))))))))))


(define-fun-rec nonzero ((xs List)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (not (= x 0)) (nonzero xs2))))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun z0 () Int)
(declare-fun X0 () List)
(declare-fun X1 () List)
(declare-fun Y0 () List)
(declare-fun Y1 () List)
(declare-fun Y2 () List)
(declare-fun Z0 () List)
(declare-fun Z1 () List)
(declare-fun Z2 () List)

(assert (or
         (not (|sum| nil nil nil))
         (and (= (+ x0 y0) z0)
              (|sum| X0 Y0 Z0)
              (not (|sum| (cons x0 X0) (cons y0 Y0) (cons z0 Z0))))
         (and (not (= x0 0))
              (not (|nonzero| (cons x0 X0))))
         (and (|nonzero| X0)
              (not (|nonzero| (cons x0 X0))))
         (and (|nonzero| X0)
              (|sum| X0 X0 X0))))

(check-sat)
