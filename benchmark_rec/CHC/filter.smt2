;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))

(define-fun prefilter ((xs List)) Bool true)

(define-fun-rec postfilter ((xs List) (ys List)) Bool
  (match ys ((nil true)
             ((cons y ys2) (match xs ((nil false)
                                      ((cons x xs2) (postfilter xs2 ys2))))))))


(define-fun-rec len ((xs List)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (len xs2))))))

(define-fun prelength ((xs List)) Bool true)

(define-fun postlength ((n Int) (xs List)) Bool (= n (len xs)))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () List)
(declare-fun Y0 () List)
(declare-fun Z0 () List)

(assert
  (or
;; filter
    (and (prefilter X0)
         (= X0 nil)
         (not (postfilter X0 nil)))

    (and (prefilter X0)
         (= X0 nil)
         (not (postfilter X0 nil)))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (not (prefilter Y0)))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (postfilter Y0 Z0)
         (not (= b0 0))
         (not (postfilter X0 (cons y0 Z0))))

    (and (prefilter X0)
         (= X0 (cons y0 Y0))
         (postfilter Y0 Z0)
         (= b0 0)
         (not (postfilter X0 Z0)))

;; length
    (and (prelength X0)
         (= X0 nil)
         (not (postlength 0 X0)))

    (and (prelength X0)
         (= X0 (cons y0 Y0))
         (not (prelength Y0)))

    (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
         (not (postlength (+ 1 n0) X0)))

;; main
    (not (prefilter X0))

    (and (postfilter X0 Y0)
         (not (prelength X0)))

    (and (postfilter X0 Y0)
         (postlength n0 X0)
         (not (prelength Y0)))

    (and (postfilter X0 Y0)
         (postlength n0 X0)
         (postlength m0 Y0)
         (not (>= n0 m0)))))

(check-sat)
