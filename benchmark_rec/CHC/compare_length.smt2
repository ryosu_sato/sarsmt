;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec len_eq ((xs list) (ys list)) Bool
    (match xs (
        (nil (= ys nil))
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (len_eq xs2 ys2))))))))

(define-fun-rec len_gt ((xs list) (ys list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (match ys (
                (nil true)
                ((cons y ys2) (len_gt xs2 ys2))))))))

(define-fun-rec len_lt ((xs list) (ys list)) Bool
    (match ys (
        (nil false)
        ((cons y ys2)
            (match xs (
                (nil true)
                ((cons x xs2) (len_lt xs2 ys2))))))))

(declare-fun d0 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (or (not (len_eq nil nil))
            (and (len_eq X0 Y0)
                 (not (len_eq (cons x0 X0) (cons y0 Y0))))
            (and (len_eq X0 Y0)
                 (not (len_gt (cons x0 X0) Y0)))
            (and (len_gt X0 Y0)
                 (not (len_gt (cons x0 X0) Y0)))
            (and (len_eq X0 Y0)
                 (not (len_lt X0 (cons y0 Y0))))
            (and (len_lt X0 Y0)
                 (not (len_lt X0 (cons y0 Y0))))
            (and (len_gt X0 Y0)
                 (len_lt X0 Y0))))

(check-sat)
