;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))

(define-fun-rec sorted ((xs List)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun preinsert ((x Int) (xs List)) Bool
  (sorted xs))

(define-fun postinsert ((x Int) (xs List) (ys List)) Bool
  (and (sorted ys)
       (match ys ((nil true)
                  ((cons y ys2) (or (<= x y)
                                    (and (not (= xs nil)) (<= (head xs) y))))))))

(define-fun preinsertsort ((xs List)) Bool true)

(define-fun postinsertsort ((xs List) (ys List)) Bool (sorted ys))

(define-fun presorted ((xs List)) Bool true)

(define-fun postsorted ((b Int) (xs List)) Bool
  (=> (sorted xs) (not (= b 0))))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun Y0 () List)
(declare-fun Z0 () List)
(declare-fun W0 () List)

(assert
  (or
;; sorted
    (and (presorted X0)
         (= X0 nil)
         (not (= x0 0))
         (not (postsorted x0 X0)))

    (and (presorted X0)
         (= X0 (cons x1 nil))
         (not (= x0 0))
         (not (postsorted x0 X0)))

    (and (presorted X0)
         (= X0 (cons y1 (cons y2 Y0)))
         (<= y1 y2)
         (not (presorted (cons y2 Y0))))

    (and (presorted X0)
         (= X0 (cons x1 (cons x2 Y0)))
         (<= x1 x2)
         (postsorted b0 (cons x2 Y0))
         (not (postsorted b0 X0)))

    (and (presorted X0)
         (= X0 (cons x1 (cons x2 Y0)))
         (not (<= x1 x2))
         (not (postsorted 0 X0)))

;; insert
    (and (preinsert x1 X0)
         (= X0 nil)
         (not (postinsert x1 X0 (cons x1 nil))))

    (and (preinsert x1 X0)
         (= X0 (cons y1 Y0))
         (<= x1 y1)
         (not (postinsert x1 X0 (cons x1 X0))))

    (and (preinsert x1 X0)
         (= X0 (cons y1 Y0))
         (not (<= x1 y1))
         (postinsert x1 Y0 Z0)
         (not (postinsert x1 X0 (cons y1 Z0))))

; insert_sort
    (and (preinsertsort X0) (= X0 nil)
         (not (postinsertsort X0 nil)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0))
         (not (preinsertsort Y0)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0)) (postinsertsort Y0 Z0)
         (not (preinsert y1 Z0)))

    (and (preinsertsort X0) (= X0 (cons y1 Y0)) (postinsertsort Y0 Z0) (postinsert y1 Z0 W0)
         (not (postinsertsort X0 Z0)))

;; main
    (not (preinsertsort X0))

    (and (postinsertsort X0 Y0)
         (not (presorted Y0)))

    (and (postinsertsort X0 Y0) (postsorted b0 Y0)
         (= b0 0))))

(check-sat)
