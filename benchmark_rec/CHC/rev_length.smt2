;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((List 0))
   (((cons (head Int) (tail List)) (nil))))

(define-fun presnoc ((x Int) (xs List)) Bool true)

(define-fun-rec postsnoc ((z Int) (xs List) (ys List)) Bool
  (match ys
         ((nil false)
          ((cons y ys2) (match xs ((nil (= ys2 nil))
                                   ((cons x xs2) (postsnoc z xs2 ys2))))))))

(define-fun prereverse ((xs List)) Bool true)

(define-fun-rec postreverse ((xs List) (ys List)) Bool
  (match xs
         ((nil (= ys nil))
          ((cons x xs2) (match ys ((nil false)
                                   ((cons y ys2) (postreverse xs2 ys2))))))))

(define-fun-rec len ((xs List)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (len xs2))))))

(define-fun prelength ((xs List)) Bool true)

(define-fun postlength ((n Int) (xs List)) Bool (= n (len xs)))


(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () List)
(declare-fun X1 () List)
(declare-fun Y0 () List)
(declare-fun Y1 () List)
(declare-fun Y2 () List)
(declare-fun Z0 () List)
(declare-fun Z1 () List)
(declare-fun Z2 () List)
(declare-fun W0 () List)


(assert
  (or
;; snoc
     (and (presnoc x0 X0)
          (= X0 nil)
          (not (postsnoc x0 X0 (cons x0 nil))))

     (and (presnoc x0 X0)
          (= X0 (cons y0 Y0))
          (not (presnoc x0 Y0)))

     (and (presnoc x0 X0) (= X0 (cons y0 Y0)) (postsnoc x0 Y0 Z0)
          (not (postsnoc x0 X0 (cons y0 Z0))))


;; reverse
     (and (prereverse X0)
          (= X0 nil)
          (not (postreverse X0 nil)))

     (and (prereverse X0) (= X0 (cons y0 Y0))
          (not (prereverse Y0)))

     (and (prereverse X0) (= X0 (cons y0 Y0)) (postreverse Y0 Z0)
          (not (presnoc y0 Z0)))

     (and (prereverse X0)
          (= X0 (cons y0 Y0))
          (postreverse Y0 Z0)
          (postsnoc y0 Z0 Z1)
          (not (postreverse X0 Z1)))

;; length
     (and (prelength X0)
          (= X0 nil)
          (not (postlength 0 X0)))

     (and (prelength X0)
          (= X0 (cons y0 Y0))
          (not (prelength Y0)))

     (and (prelength X0) (= X0 (cons y0 Y0)) (postlength n0 Y0)
          (not (postlength (+ 1 n0) X0)))

;; main
     (not (prereverse X0))

     (and (postreverse X0 Y0)
          (not (prelength X0)))

     (and (postreverse X0 Y0) (postlength n0 X0)
          (not (prelength Y0)))

     (and (postreverse X0 Y0) (postlength n0 X0) (postlength m0 Y0)
          (not (= n0 m0)))))

(check-sat)
