;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))


(define-fun-rec leq ((xs list) (ys list)) Bool
  (match xs ((nil (= ys nil))
             ((cons x xs2) (match ys ((nil false)
                                      ((cons y ys2) (and (<= x y) (leq xs2 ys2)))))))))

(declare-fun b0 () Int)
(declare-fun n0 () Int)
(declare-fun m0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)
(declare-fun x2 () Int)
(declare-fun y0 () Int)
(declare-fun y1 () Int)
(declare-fun y2 () Int)
(declare-fun X0 () list)
(declare-fun X1 () list)
(declare-fun Y0 () list)
(declare-fun Y1 () list)
(declare-fun Y2 () list)
(declare-fun Z0 () list)
(declare-fun Z1 () list)
(declare-fun Z2 () list)
(declare-fun W0 () list)


(assert (or
         (not (|leq| nil nil))
         (and (<= x0 y0)
              (|leq| X0 Y0)
              (not (|leq| (cons x0 X0) (cons y0 Y0))))
         (and (|leq| X0 Y0)
              (|leq| Y0 X0)
              (not (= X0 Y0)))))

(check-sat)
