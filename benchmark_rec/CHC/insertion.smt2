;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec insertion ((x Int) (xs list)) list
    (match xs (
        (nil (cons x nil))
        ((cons y ys)
            (ite (<= x y)
                 (cons x xs)
                 (cons y (insertion x ys)))))))

(define-fun-rec sorted ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun-rec unsorted ((xs list)) Bool
    (match xs (
        (nil false)
        ((cons x1 xs2)
            (match xs2 (
                (nil false)
                ((cons x2 xs3) (or (> x1 x2) (unsorted xs2)))))))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (or (not (= (insertion x0 nil) (cons x0 nil)))
            (and (<= x0 y0)
                 (not (= (insertion x0 (cons y0 X0)) (cons x0 (cons y0 X0)))))
            (and (> x0 y0)
                 (= (insertion x0 X0) Y0)
                 (not (= (insertion x0 (cons y0 X0)) (cons y0 Y0))))
            (not (sorted nil))
            (not (sorted (cons x0 nil)))
            (and (<= x0 y0)
                 (sorted (cons y0 X0))
                 (not (sorted (cons x0 (cons y0 X0)))))
            (and (> x0 y0)
                 (not (unsorted (cons x0 (cons y0 X0)))))
            (and (unsorted X0)
                 (not (unsorted (cons x0 X0))))
            (and (sorted X0)
                 (= (insertion x0 X0) Y0)
                 (unsorted Y0))))

(check-sat)
