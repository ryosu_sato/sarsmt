;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec prefix ((xs list) (ys list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (and (= x y) (prefix xs2 ys2)))))))))

(define-fun-rec haszero ((xs list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (= x 0) (haszero xs2))))))

(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (prefix X0 Y0)
             (haszero Y0)
             (not (haszero X0))))

(check-sat)
