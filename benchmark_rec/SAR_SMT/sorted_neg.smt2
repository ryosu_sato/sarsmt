;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec sorted ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun-rec exists_neg ((xs list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
         (or (< x 0) (exists_neg xs2))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)

(assert (and (sorted (cons x0 X0))
             (exists_neg X0)
             (>= x0 0)))

(check-sat)
