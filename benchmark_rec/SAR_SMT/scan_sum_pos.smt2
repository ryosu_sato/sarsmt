;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec forall_pos ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (>= x 0) (forall_pos xs2))))))

; ys,_ = fold_right (fun x (acc,s) -> (x+s)::acc, x+s) xs ([],0)
(define-fun-rec scan_sum ((xs list) (ys list)) Bool
    (match xs (
        (nil (= ys nil))
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (or (and (= x y) (= xs2 nil) (= ys2 nil))
                                  (and (not (= ys2 nil))
                                       (= y (+ x (head ys2)))
                                       (scan_sum xs2 ys2))))))))))

(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (scan_sum X0 Y0)
             (forall_pos X0)
             (not (forall_pos Y0))))

(check-sat)
