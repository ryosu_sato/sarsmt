;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun n2 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (>= n0 0)
             (take n0 X0 Y0)
             (= n1 (length Y0))
             (not (>= n0 n1))))

(check-sat)
