;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec prefix ((xs list) (ys list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (and (= x y) (prefix xs2 ys2)))))))))

(define-fun-rec forall_pos ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (>= x 0) (forall_pos xs2))))))

(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (prefix X0 Y0)
             (forall_pos X0)
             (not (forall_pos Y0))))

(check-sat)
