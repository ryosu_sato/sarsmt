;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec haszero ((xs list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (= x 0) (haszero xs2))))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun n0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (take n0 Y0 X0)
             (haszero X0)
             (not (haszero Y0))))

(check-sat)
