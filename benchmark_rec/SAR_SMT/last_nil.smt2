;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec last ((xs list)) Int
  (match xs (
     (nil 0)
     ((cons x ys)
         (match ys (
             (nil x)
             ((cons y zs) (last ys))))))))

(declare-fun n0 () Int)
(declare-fun X0 () list)

(assert (and (= X0 nil)
             (not (= 0 (last X0)))))

(check-sat)
