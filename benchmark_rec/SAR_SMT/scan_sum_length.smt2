;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec forall_pos ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (>= x 0) (forall_pos xs2))))))

; ys,_ = fold_right (fun x (acc,s) -> (x+s)::acc, x+s) xs ([],0)
(define-fun-rec scan_sum ((xs list) (ys list)) Bool
    (match xs (
        (nil (= ys nil))
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (or (and (= x y) (= xs2 nil) (= ys2 nil))
                                  (and (not (= ys2 nil))
                                       (= y (+ x (head ys2)))
                                       (scan_sum xs2 ys2))))))))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (scan_sum X0 Y0)
             (= n0 (length X0))
             (= n1 (length Y0))
             (not (= n0 n1))))

(check-sat)
