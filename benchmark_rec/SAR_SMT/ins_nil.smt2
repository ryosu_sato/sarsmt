;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec ins ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (< x z) (= ys (cons x xs)))
                (and (>= x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (ins x zs ws)))))))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)

(assert (and (= X0 nil)
             (not (ins x0 X0 (cons x0 nil)))))

(check-sat)
