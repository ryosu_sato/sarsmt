;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec sorted ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun-rec forall_ge ((x0 Int) (xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (>= x x0) (forall_ge x0 xs2))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)

(assert (and (sorted (cons x0 X0))
             (not (forall_ge x0 X0))))

(check-sat)
