;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

; take_total n xs = ys
(define-fun-rec take_total ((n Int) (xs list)) list
    (ite (<= n 0)
         nil
         (match xs (
             (nil nil)
             ((cons x xs2) (cons x (take_total (- n 1) xs2)))))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun n2 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (= Y0 (take_total n0 X0))
             (= n1 (length Y0))
             (= n2 (length X0))
             (<= n0 n2)
             (not (= n0 n1))))

(check-sat)
