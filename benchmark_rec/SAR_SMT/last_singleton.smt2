;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec last ((xs list)) Int
  (match xs (
     (nil 0)
     ((cons x ys)
         (match ys (
             (nil x)
             ((cons y zs) (last ys))))))))

(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (= Y0 (cons x0 X0))
             (= X0 nil)
             (not (= x0 (last Y0)))))

(check-sat)
