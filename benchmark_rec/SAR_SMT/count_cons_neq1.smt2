;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec count ((c Int) (x0 Int) (xs list)) Bool
    (match xs (
        (nil (= c 0))
        ((cons x xs2)
            (or (and (= x x0) (count (- c 1) x0 xs2))
                (and (not (= x x0)) (count c x0 xs2)))))))

(declare-fun c0 () Int)
(declare-fun c1 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)

(assert (and (not (= x0 y0))
             (count c0 x0 X0)
             (count c1 x0 (cons y0 X0))
             (not (= c0 c1))))

(check-sat)
