;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec prefix ((xs list) (ys list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (and (= x y) (prefix xs2 ys2)))))))))

(define-fun-rec forall_eq ((x0 Int) (xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (= x x0) (forall_eq x0 xs2))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (prefix X0 (cons x0 X0))
             (not (forall_eq x0 X0))))

(check-sat)
