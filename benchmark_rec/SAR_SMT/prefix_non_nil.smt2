;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec prefix ((xs list) (ys list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (and (= x y) (prefix xs2 ys2)))))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (prefix (cons x0 X0) Y0)
             (= nil Y0)))

(check-sat)
