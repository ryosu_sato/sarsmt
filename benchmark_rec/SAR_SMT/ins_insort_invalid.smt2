;;; EXPECT: sat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec ins ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (< x z) (= ys (cons x xs)))
                (and (>= x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (ins x zs ws)))))))))))

(define-fun-rec insort ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (<= x z) (= ys (cons x xs)))
                (and (> x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (insort x zs ws)))))))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)
(declare-fun Z0 () list)

(declare-fun y0 () Int)
(declare-fun z0 () Int)

(assert (and (ins x0 X0 Y0)
             (insort x0 X0 Z0)
             (not (= Y0 Z0))))

(check-sat)
