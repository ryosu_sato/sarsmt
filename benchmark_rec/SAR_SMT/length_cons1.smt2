;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () list)

(assert (and (= n0 (length (cons x0 X0)))
             (= n1 (length X0))
             (not (= (+ n1 1) n0))))

(check-sat)
