;;; EXPECT: sat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun x1 () Int)

(assert (and (= n0 (length (cons x0 (cons x1 nil))))
             (not (>= n0 3))))

(check-sat)
