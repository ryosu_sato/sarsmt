;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec sorted ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun-rec nth ((i Int) (x0 Int) (xs list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (and (= i 0) (= x x0))
                (nth (- i 1) x0 xs2))))))

(declare-fun i0 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)

(assert (and (sorted (cons x0 X0))
             (nth i0 y0 X0)
             (not (<= x0 y0))))

(check-sat)
