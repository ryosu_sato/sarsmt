;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec prefix ((xs list) (ys list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (match ys (
                (nil false)
                ((cons y ys2) (and (= x y) (prefix xs2 ys2)))))))))

(define-fun-rec nth ((i Int) (x0 Int) (xs list)) Bool
    (match xs (
        (nil false)
        ((cons x xs2)
            (or (and (= i 0) (= x x0))
                (nth (- i 1) x0 xs2))))))

(declare-fun i0 () Int)
(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (prefix X0 Y0)
             (nth i0 x0 X0)
             (nth i0 y0 Y0)
             (not (= x0 y0))))

(check-sat)
