;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun n0 () Int)
(declare-fun X0 () list)

(assert (and (= X0 nil)
             (not (take n0 X0 nil))))

(check-sat)
