;;; EXPECT: unsat
(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec gte ((x0 Int) (xs list)) Bool
    (match xs (
        (nil true)
        ((cons x xs2)
            (and (>= x x0) (gte x0 xs2))))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(define-fun-rec sumfold ((xs list)) Int
    (match xs (
        (nil 0)
        ((cons x xs2)
            (+ x (sumfold xs2))))))

(declare-fun x0 () Int)
(declare-fun n0 () Int)
(declare-fun s0 () Int)
(declare-fun X0 () list)

(assert (and (gte x0 X0)
             (= n0 (length X0))
             (= s0 (sumfold X0))
             (not (>= s0 (* x0 n0)))))

(check-sat)
; unsolvable
