;;; INFO: sorted xs /\ ys = insort x xs => sorted ys
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec sorted ((xs list)) Bool
    (match xs (
        (nil true)
        ((cons x1 xs2)
            (match xs2 (
                (nil true)
                ((cons x2 xs3) (and (<= x1 x2) (sorted xs2)))))))))

(define-fun-rec insort ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (<= x z) (= ys (cons x xs)))
                (and (> x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (insort x zs ws)))))))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (sorted X0)
             (insort x0 X0 Y0)
             (not (sorted Y0))))

(check-sat)
