;;; INFO: take 0 xs = []
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (take 0 X0 Y0)
             (not (= Y0 nil))))

(check-sat)
