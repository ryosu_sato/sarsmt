;;; INFO: len (butlast xs) = len xs - 1 (* 0-1 = 0 *)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec butlast ((xs list) (ys list)) Bool
    (match xs (
        (nil (= ys nil))
        ((cons x1 xs1)
            (match xs1 (
                (nil (= ys nil))
                ((cons x2 xs2)
                    (match ys (
                        (nil false)
                        ((cons y1 ys1) (butlast xs1 ys1)))))))))))

(define-fun-rec length ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (length xs2))))))

(declare-fun n0 () Int)
(declare-fun n1 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (butlast X0 Y0)
             (= n0 (length Y0))
             (= n1 (length X0))
             (not (or (= n0 (- n1 1)) (and (= n0 0) (= n1 0))))))

(check-sat)
