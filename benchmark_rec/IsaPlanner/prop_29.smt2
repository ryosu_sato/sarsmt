;;; INFO: mem x (ins1 x xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec elem ((x Int) (xs list)) Bool
    (match xs (
        (nil false)
        ((cons y ys)
            (or (= x y) (elem x ys))))))

(define-fun-rec ins1 ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (= x z) (= ys xs)
                (and (not (= x z))
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (ins1 x zs ws))))))))))))

(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (ins1 x0 X0 Y0)
             (not (elem x0 Y0))))

(check-sat)
