;;; INFO: ys = butlast xs /\ n = len xs => ys = take (n-1) xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec len ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (len xs2))))))

(define-fun-rec butlast ((xs list) (ys list)) Bool
    (match xs (
        (nil (= ys nil))
        ((cons x1 xs1)
            (match xs1 (
                (nil (= ys nil))
                ((cons x2 xs2)
                    (match ys (
                        (nil false)
                        ((cons y1 ys1) (and (= x1 y1) (butlast xs1 ys1))))))))))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun n0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (butlast X0 Y0)
             (= n0 (len X0))
             (not (take (- n0 1) X0 Y0))))
(assert (take (- 0 1) nil nil))

(check-sat)
