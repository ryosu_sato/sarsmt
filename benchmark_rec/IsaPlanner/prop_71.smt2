;;; INFO: x <> y => mem x (ins y xs) = mem x xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec elem ((x Int) (xs list)) Bool
    (match xs (
        (nil false)
        ((cons y ys)
            (or (= x y) (elem x ys))))))

(define-fun-rec ins ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (< x z) (= ys (cons x xs)))
                (and (>= x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (ins x zs ws)))))))))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (or (and (not (= x0 y0))
                 (ins y0 X0 Y0)
                 (elem x0 X0)
                 (not (elem x0 Y0)))
            (and (not (= x0 y0))
                 (ins y0 X0 Y0)
                 (elem x0 Y0)
                 (not (elem x0 X0)))))

(check-sat)
