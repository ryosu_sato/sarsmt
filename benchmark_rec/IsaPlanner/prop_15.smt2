;;; INFO: length (ins x xs) = 1 + length xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec ins ((x Int) (xs list) (ys list)) Bool
    (match xs (
        (nil (= ys (cons x nil)))
        ((cons z zs)
            (or (and (< x z) (= ys (cons x xs)))
                (and (>= x z)
                     (match ys (
                         (nil false)
                         ((cons w ws) (and (= z w) (ins x zs ws)))))))))))

(define-fun-rec len ((xs list)) Int
   (match xs (
       (nil 0)
       ((cons x xs2) (+ 1 (len xs2))))))

(declare-fun n1 () Int)
(declare-fun n2 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (ins x0 X0 Y0)
             (= n1 (len X0))
             (= n2 (len Y0))
             (not (= (+ n1 1) n2))))

(check-sat)
