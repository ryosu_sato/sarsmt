;;; INFO: xs <> [] => last (x:xs) = last xs
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec last ((xs list)) Int
  (match xs (
     (nil 0)
     ((cons x ys)
         (match ys (
             (nil x)
             ((cons y zs) (last ys))))))))

(declare-fun x0 () Int)
(declare-fun y0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)

(assert (and (= Y0 (cons x0 X0))
             (not (= X0 nil))
             (= y0 (last X0))
             (not (= y0 (last Y0)))))

(check-sat)
