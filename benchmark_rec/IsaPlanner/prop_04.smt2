;;; INFO: 1 + count n xs = count n (n::xs)
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

(define-fun-rec count ((c Int) (x0 Int) (xs list)) Bool
    (match xs (
        (nil (= c 0))
        ((cons x xs2)
            (or (and (= x x0) (count (- c 1) x0 xs2))
                (and (not (= x x0)) (count c x0 xs2)))))))

(declare-fun c1 () Int)
(declare-fun c2 () Int)
(declare-fun n0 () Int)
(declare-fun L0 () list)

(assert (and (count c1 n0 L0)
             (count c2 n0 (cons n0 L0))
             (not (= (+ c1 1) c2))))

(check-sat)
