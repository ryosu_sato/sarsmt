;;; INFO: n >= 0 /\ ys = take n xs /\ zs = take (n+1) (x::xs) => zs = x::ys
;;; EXPECT: unsat

(set-logic ALL)

(declare-datatypes ((list 0))
   (((cons (head Int) (tail list)) (nil))))

; take n xs = ys
(define-fun-rec take ((n Int) (xs list) (ys list)) Bool
    (match ys (
        (nil
            (or (<= n 0)
                (and (> n 0) (= xs nil))))
        ((cons y ys2)
            (match xs (
                (nil false)
                ((cons x xs2) (and (> n 0) (= x y) (take (- n 1) xs2 ys2)))))))))

(declare-fun n0 () Int)
(declare-fun x0 () Int)
(declare-fun X0 () list)
(declare-fun Y0 () list)
(declare-fun Z0 () list)

(assert (and (>= n0 0)
             (take n0 X0 Y0)
             (take (+ n0 1) (cons x0 X0) Z0)
             (not (= Z0 (cons x0 Y0)))))

(check-sat)
