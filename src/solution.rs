use crate::argument::*;
use crate::rules;
use crate::sar::*;

use std::collections::{BTreeMap, BTreeSet};

type Tmp = (
    SymbolicAutomaticRelation<EArgId, LArgId>,
    BTreeSet<ExistEArgId>,
    BTreeSet<ExistLArgId>,
);
type Pair = (Tmp, Tmp);

pub fn translate(
    isv_vec: &Vec<rules::IntSortedVar>,
    lsv_vec: &Vec<rules::ListSortedVar>,
    sar_def: rules::Sar,
) -> Pair {
    let mut ctx = Context::new();

    let rules::Sar::Sar0(
        _,
        _,
        _,
        et_vec,
        _,
        _,
        lt_vec,
        _,
        _,
        _,
        _,
        is_vec,
        _,
        _,
        fs_vec,
        _,
        _,
        _,
        trans_vec,
        _,
        _,
    ) = sar_def;

    let mut is_map = BTreeMap::new();
    for rules::IntSortedVar::IntSortedVar0(_, ev, _, _) in isv_vec {
        let rules::ElementVariable::EVar0(s) = ev as &rules::ElementVariable;
        is_map.insert(s.to_string(), EArgId::new(&mut ctx));
    }

    let mut ls_map = BTreeMap::new();
    for rules::ListSortedVar::ListSortedVar0(_, lv, _, _) in lsv_vec {
        let rules::ListVariable::LVar0(s) = lv as &rules::ListVariable;
        ls_map.insert(s.to_string(), LArgId::new(&mut ctx));
    }

    let ees_map = BTreeMap::new();
    let els_map = BTreeMap::new();

    let mut rid_vec = vec![];
    let mut reps = Repetition::new();
    for et in et_vec {
        let rid = RepeatId::new(&mut ctx);
        reps.insert(
            rid,
            ETerm::<EArgId, LArgId>::from(&et, &is_map, &ls_map, &ees_map, &els_map),
        );
        rid_vec.push(rid);
    }

    let mut pid_vec = vec![];
    let mut args = Arguments::new();
    for lt in lt_vec {
        let pid = ParamId::new(&mut ctx);
        args.insert(
            pid,
            LTerm::<EArgId, LArgId>::from(&lt, &is_map, &ls_map, &ees_map, &els_map),
        );
        pid_vec.push(pid);
    }

    let mut q_set = BTreeSet::new();
    for rules::State::State0(s) in &is_vec {
        q_set.insert(s.to_string());
    }
    for rules::State::State0(s) in &fs_vec {
        q_set.insert(s.to_string());
    }
    for rules::Transition::Transition0(_, s1, s2, _, _) in &trans_vec {
        let rules::State::State0(s) = s1 as &rules::State;
        q_set.insert(s.to_string());
        let rules::State::State0(s) = s2 as &rules::State;
        q_set.insert(s.to_string());
    }
    let mut state_map = BTreeMap::new();
    for q_str in q_set {
        state_map.insert(q_str, State::new(&mut ctx));
    }

    let is_set = is_vec
        .iter()
        .map(|rules::State::State0(s)| *state_map.get(s).unwrap())
        .collect();

    let fs_set = fs_vec
        .iter()
        .map(|rules::State::State0(s)| *state_map.get(s).unwrap())
        .collect();

    let mut trans = Transitions::new();
    for rules::Transition::Transition0(_, st1, st2, label, _) in trans_vec {
        let rules::State::State0(s1) = *st1;
        let rules::State::State0(s2) = *st2;
        trans.insert(
            *state_map.get(&s1).unwrap(),
            *state_map.get(&s2).unwrap(),
            Label::from(&label, &pid_vec, &rid_vec),
        );
    }

    let tmp0 = SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx);
    let tmp1 = SymbolicAutomaticRelation::complement(&tmp0);
    (
        (tmp0, BTreeSet::new(), BTreeSet::new()),
        (tmp1, BTreeSet::new(), BTreeSet::new()),
    )
}

pub fn translate_with_eq(
    isv_vec: &Vec<rules::IntSortedVar>,
    lsv_vec: &Vec<rules::ListSortedVar>,
    eqsar_def: rules::EQSar,
) -> Tmp {
    let mut ctx = Context::new();

    let rules::EQSar::EQSar0(_, _, _, ex_sv_vec, _, sar_def, _) = eqsar_def;

    let mut is_map = BTreeMap::new();
    for rules::IntSortedVar::IntSortedVar0(_, ev, _, _) in isv_vec {
        let rules::ElementVariable::EVar0(s) = ev as &rules::ElementVariable;
        is_map.insert(s.to_string(), EArgId::new(&mut ctx));
    }

    let mut ls_map = BTreeMap::new();
    for rules::ListSortedVar::ListSortedVar0(_, lv, _, _) in lsv_vec {
        let rules::ListVariable::LVar0(s) = lv as &rules::ListVariable;
        ls_map.insert(s.to_string(), LArgId::new(&mut ctx));
    }

    let mut ees_map = BTreeMap::new();
    let mut els_map = BTreeMap::new();

    let mut set1 = BTreeSet::new();
    let mut set2 = BTreeSet::new();
    for ex_sv in ex_sv_vec {
        match ex_sv {
            rules::EQSortedVar::EQSortedVar0(tmp) => {
                let rules::EQIntSortedVar::EQIntSortedVar0(_, eev, _, _) = *tmp;
                let rules::EQElementVariable::EQEVar0(s) = *eev;
                let eeaid = ExistEArgId::new();
                ees_map.insert(s, eeaid);
                set1.insert(eeaid);
            }

            rules::EQSortedVar::EQSortedVar1(tmp) => {
                let rules::EQListSortedVar::EQListSortedVar0(_, elv, _, _) = *tmp;
                let rules::EQListVariable::EQLVar0(s) = *elv;
                let elaid = ExistLArgId::new();
                els_map.insert(s, elaid);
                set2.insert(elaid);
            }
        }
    }

    let rules::Sar::Sar0(
        _,
        _,
        _,
        et_vec,
        _,
        _,
        lt_vec,
        _,
        _,
        _,
        _,
        is_vec,
        _,
        _,
        fs_vec,
        _,
        _,
        _,
        trans_vec,
        _,
        _,
    ) = *sar_def;

    let mut rid_vec = vec![];
    let mut reps = Repetition::new();
    for et in et_vec {
        let rid = RepeatId::new(&mut ctx);
        reps.insert(
            rid,
            ETerm::<EArgId, LArgId>::from(&et, &is_map, &ls_map, &ees_map, &els_map),
        );
        rid_vec.push(rid);
    }

    let mut pid_vec = vec![];
    let mut args = Arguments::new();
    for lt in lt_vec {
        let pid = ParamId::new(&mut ctx);
        args.insert(
            pid,
            LTerm::<EArgId, LArgId>::from(&lt, &is_map, &ls_map, &ees_map, &els_map),
        );
        pid_vec.push(pid);
    }

    let mut q_set = BTreeSet::new();
    for rules::State::State0(s) in &is_vec {
        q_set.insert(s.to_string());
    }
    for rules::State::State0(s) in &fs_vec {
        q_set.insert(s.to_string());
    }
    for rules::Transition::Transition0(_, s1, s2, _, _) in &trans_vec {
        let rules::State::State0(s) = s1 as &rules::State;
        q_set.insert(s.to_string());
        let rules::State::State0(s) = s2 as &rules::State;
        q_set.insert(s.to_string());
    }
    let mut state_map = BTreeMap::new();
    for q_str in q_set {
        state_map.insert(q_str, State::new(&mut ctx));
    }

    let is_set = is_vec
        .iter()
        .map(|rules::State::State0(s)| *state_map.get(s).unwrap())
        .collect();

    let fs_set = fs_vec
        .iter()
        .map(|rules::State::State0(s)| *state_map.get(s).unwrap())
        .collect();

    let mut trans = Transitions::new();
    for rules::Transition::Transition0(_, st1, st2, label, _) in trans_vec {
        let rules::State::State0(s1) = *st1;
        let rules::State::State0(s2) = *st2;
        trans.insert(
            *state_map.get(&s1).unwrap(),
            *state_map.get(&s2).unwrap(),
            Label::from(&label, &pid_vec, &rid_vec),
        );
    }

    (
        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx),
        set1,
        set2,
    )
}
