pub use ruly::*;

reserved_words!();

add_rule!(
    ElementVariable => EVar0 ({(r"[a-z][0-9]+")})
);

add_rule!(
    FunctionSymbol => FuncSym0 ({(r"[A-Za-z-+/\*=%?!.$_˜&ˆ<>@][0-9A-Za-z-+/\*=%?!.$_˜&ˆ<>@]*")})
);

add_rule!(
    PredicateSymbol => PredSym0 ({(r"[A-Za-z-+/\*=%?!.$_˜&ˆ<>@][0-9A-Za-z-+/\*=%?!.$_˜&ˆ<>@]*")})
);

add_rule!(
    ElementTerm => ETerm0 ({(r"(0|[1-9][0-9]*)"), i32, |s: String|s.parse::<i32>().unwrap()})
        | ETerm1 (ElementVariable)
        | ETerm2 ({(r"\(")}, {(r"head")}, ListTerm, {(r"\)")})
        | ETerm3 ({(r"\(")}, FunctionSymbol, (ElementTerm,1), {(r"\)")})
);

add_rule!(
    ListVariable => LVar0 ({(r"[A-Z][0-9]+")})
);

add_rule!(
    ListTerm => LTerm0 ({(r"nil")})
        | LTerm1 (ListVariable)
        | LTerm2 ({(r"\(")}, {(r"tail")}, ListTerm, {(r"\)")})
        | LTerm3 ({(r"\(")}, {(r"insert")}, ElementTerm, ListTerm, {(r"\)")})
);

add_rule!(
    UninterpretedPredicate => UPred0 ({(r"\|[a-z][a-z0-9_]*\|")})
);

add_rule!(
    UninterpretedApplication => UApp0 ({(r"\(")}, UninterpretedPredicate, (ElementTerm,0), (ListTerm,0), {(r"\)")})
);

add_rule!(
    ListEquality => LEq ({(r"\(")}, {(r"=")}, ListTerm, ListTerm, {(r"\)")})
);

add_rule!(
    IFormula => IFormula0 (ListEquality)
        | IFormula1 ({(r"\(")}, PredicateSymbol, (ElementTerm,1), {(r"\)")})
);

add_rule!(
    Formula => Formula0 ({(r"\(")}, {(r"and")}, (Formula,1), {(r"\)")})
        | Formula1 ({(r"\(")}, {(r"or")}, (Formula,1), {(r"\)")})
        | Formula2 ({(r"\(")}, {(r"=>")}, Formula, Formula, {(r"\)")})
        | Formula3 ({(r"\(")}, {(r"not")}, Formula, {(r"\)")})
        | Formula4 (UninterpretedApplication)
        | Formula5 (IFormula)
        | Formula6 ({(r"true")})
        | Formula7 ({(r"false")})
);

add_rule!(
    IntSortedVar => IntSortedVar0 ({(r"\(")}, ElementVariable, IntSort, {(r"\)")})
);

add_rule!(
    ListSortedVar => ListSortedVar0 ({(r"\(")}, ListVariable, ListSort, {(r"\)")})
);

add_rule!(
    SortedVar => SortedVar0 (IntSortedVar)
        | SortedVar1 (ListSortedVar)
);

add_rule!(
    Assertion => Assertion0 ({(r"\(")}, {(r"assert")}, {(r"\(")}, {(r"forall")}, {(r"\(")}, (SortedVar,1), {(r"\)")}, Formula, {(r"\)")}, {(r"\)")})
);

add_rule!(
    SetLogic => SetLogic0 ({(r"\(")}, {(r"set\-logic")}, {(r"HORN")}, {(r"\)")})
);

add_rule!(
    CheckSat => CheckSat0 ({(r"\(")}, {(r"check\-sat")}, {(r"\)")})
);

add_rule!(
    IntSort => IntSort0 ({(r"Int")})
);

add_rule!(
    ListSort => ListSort0 ({(r"\(")}, {(r"List")}, {(r"Int")}, {(r"\)")})
);

add_rule!(
    EQElementVariable => EQEVar0 ({(r"e[a-z][0-9]+")})
);

add_rule!(
    EQListVariable => EQLVar0 ({(r"E[A-Z][0-9]+")})
);

add_rule!(
    EQIntSortedVar => EQIntSortedVar0 ({(r"\(")}, EQElementVariable, IntSort, {(r"\)")})
);

add_rule!(
    EQListSortedVar => EQListSortedVar0 ({(r"\(")}, EQListVariable, ListSort, {(r"\)")})
);

add_rule!(
    EQSortedVar => EQSortedVar0 (EQIntSortedVar)
        | EQSortedVar1 (EQListSortedVar)
);

add_rule!(
    EQSar => EQSar0 ({(r"\(")}, {(r"exists")}, {(r"\(")}, (EQSortedVar,1), {(r"\)")}, Sar, {(r"\)")})
);

add_rule!(
    FunctionDefinition => FuncDef0 ({(r"\(")}, {(r"define\-fun")}, UninterpretedPredicate, {(r"\(")}, (IntSortedVar,0), (ListSortedVar,0), {(r"\)")}, {(r"Bool")}, FunctionBody, {(r"\)")})
);

add_rule!(
    FunctionBody => FunctionBody0 (Sar)
        | FunctionBody1 ({(r"\(")}, EQSar, EQSar, {(r"\)")})
);

add_rule!(
    State => State0 ({(r"q_[0-9]+")})
);

add_rule!(
    Transition => Transition0 ({(r"\(")}, State, State, Label, {(r"\)")})
);

add_rule!(
    EQElementTerm => EQETerm0 ({(r"(0|[1-9][0-9]*)"), i32, |s: String|s.parse::<i32>().unwrap()})
        | EQETerm1 (ElementVariable)
        | EQETerm2 (EQElementVariable)
        | EQETerm3 ({(r"\(")}, {(r"head")}, EQListTerm, {(r"\)")})
        | EQETerm4 ({(r"\(")}, FunctionSymbol, (EQElementTerm,1), {(r"\)")})
);

add_rule!(
    EQListTerm => EQLTerm0 ({(r"nil")})
        | EQLTerm1 (ListVariable)
        | EQLTerm2 (EQListVariable)
        | EQLTerm3 ({(r"\(")}, {(r"tail")}, EQListTerm, {(r"\)")})
        | EQLTerm4 ({(r"\(")}, {(r"insert")}, EQElementTerm, EQListTerm, {(r"\)")})
);

add_rule!(
    Sar => Sar0 ({(r"\(")},
        {(r"\(")}, {(r"\(")}, (EQElementTerm,0), {(r"\)")}, {(r"\(")}, (EQListTerm,0), {(r"\)")}, {(r"\)")},
        {(r"\(")}, {(r"\(")}, (State,0), {(r"\)")},
        {(r"\(")}, (State,0), {(r"\)")}, {(r"\)")},
        {(r"\(")}, (Transition,0), {(r"\)")},
        {(r"\)")})
);

add_rule!(
    Term => Term0 ({(r"(0|[1-9][0-9]*)"), i32, |s: String|s.parse::<i32>().unwrap()})
        | Term1 ({(r"l_[0-9]+")})
        | Term2 ({(r"x_[0-9]+")})
        | Term3 ({(r"\(")}, FunctionSymbol, (Term,1), {(r"\)")})
);

add_rule!(
    Label => Label0 ({(r"true")})
        | Label1 ({(r"false")})
        | Label2 ({(r"\(")}, {(r"and")}, (Label,1), {(r"\)")})
        | Label3 ({(r"\(")}, {(r"or")}, (Label,1), {(r"\)")})
        | Label4 ({(r"\(")}, {(r"not")}, Label, {(r"\)")})
        | Label5 ({(r"\(")}, {(r"pad")}, {(r"l_[0-9]+")},{(r"\)")})
        | Label6 ({(r"\(")}, PredicateSymbol, (Term,1), {(r"\)")})
);

add_rule!(
    BenchMark => BenchMark0 (SetLogic, (FunctionDefinition,0), (Assertion,0), CheckSat)
);
