use crate::rules::*;
use crate::*;

use chrono::Local;

use std::cmp::Ordering;
use std::collections::BTreeMap;
use std::fs::File;
use std::io::prelude::*;
use std::io::Write;
use std::process::Command;

#[derive(Debug, PartialEq, Eq)]
enum SmtResult {
    Sat,
    Unsat,
    Unknown,
    Timeout,
}
impl Ord for SmtResult {
    fn cmp(&self, other: &Self) -> Ordering {
        match self {
            SmtResult::Sat => match other {
                SmtResult::Sat => Ordering::Equal,
                _ => Ordering::Greater,
            },
            SmtResult::Timeout => match other {
                SmtResult::Sat => Ordering::Less,
                SmtResult::Timeout => Ordering::Equal,
                _ => Ordering::Greater,
            },
            SmtResult::Unknown => match other {
                SmtResult::Unsat => Ordering::Greater,
                SmtResult::Unknown => Ordering::Equal,
                _ => Ordering::Less,
            },
            SmtResult::Unsat => match other {
                SmtResult::Unsat => Ordering::Equal,
                _ => Ordering::Less,
            },
        }
    }
}
impl PartialOrd for SmtResult {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl std::fmt::Display for SmtResult {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SmtResult::Sat => write!(f, "sat"),
            SmtResult::Unsat => write!(f, "unsat"),
            SmtResult::Unknown => write!(f, "unknown"),
            SmtResult::Timeout => write!(f, "timeout"),
        }
    }
}

pub fn run(com: Option<&str>, filepath: &str) -> Result<(), Box<std::error::Error>> {
    let datetime = Local::now().format("%Y%m%d-%H%M%S-%f").to_string();
    let filename = filepath.split("/").last().expect("invalid path.");
    let basename = filename.split(".").next().expect("invalid path.");

    let mut f = File::open(filepath).expect("file not found");
    let mut s = String::new();
    f.read_to_string(&mut s)
        .expect("something went wrong reading the file");

    let mut parser = Ruly::new();
    parser.set_skip_reg(r"([ \n\r\t]*;[\x20-\x7e\x80-\xff]*(\n|\r))*[ \n\r\t]*");
    parser.set_input(&s);

    match parser.run() {
        Ok(BenchMark::BenchMark0(_, fundef_vec, assert_vec, _)) => {
            let mut pred_map = BTreeMap::new();

            for fun_def in fundef_vec {
                let FunctionDefinition::FuncDef0(
                    _,
                    _,
                    fun_name,
                    _,
                    isv_vec,
                    lsv_vec,
                    _,
                    _,
                    fun_body,
                    _,
                ) = fun_def;

                match *fun_body {
                    FunctionBody::FunctionBody0(sar_def) => {
                        let UninterpretedPredicate::UPred0(s) = *fun_name;
                        pred_map.insert(s, solution::translate(&isv_vec, &lsv_vec, *sar_def));
                    }

                    FunctionBody::FunctionBody1(_, eqsar_def_pos, eqsar_def_neg, _) => {
                        let UninterpretedPredicate::UPred0(s) = *fun_name;
                        pred_map.insert(
                            s,
                            (
                                solution::translate_with_eq(&isv_vec, &lsv_vec, *eqsar_def_pos),
                                solution::translate_with_eq(&isv_vec, &lsv_vec, *eqsar_def_neg),
                            ),
                        );
                    }
                }
            }

            let dir_path = match com {
                Some(solver) => format!("output/{}_{}_{}", basename, solver, datetime),
                None => format!("output/{}_graph_{}", basename, datetime),
            };

            std::fs::create_dir_all(&dir_path)?;

            let mut result = SmtResult::Sat;

            for (id, assert) in assert_vec.into_iter().enumerate() {
                let Assertion::Assertion0(_, _, _, _, _, _, _, formula, _, _) = assert;

                let sar_def =
                    sar::SymbolicAutomaticRelation::from_formula(&formula, &pred_map, false);

                match com {
                    Some(solver) => {
                        let ir_path = format!("{}/clause_{}.smt2", dir_path, id);
                        let mut ir_file = File::create(&ir_path)?;
                        let s = sar_def.generate_chc();
                        ir_file.write_all(s.as_bytes())?;
                        ir_file.flush()?;

                        let tmp;

                        let output = Command::new("timeout")
                            .args(&["-sKILL"])
                            .args(&["60", solver, &ir_path])
                            .output()
                            .expect("failed to start `timeout`");
                        let s = String::from_utf8_lossy(&output.stdout);
                        let res_str;
                        match output.status.code() {
                            Some(_) => {
                                res_str = s.split('\n').next().unwrap();
                                match res_str {
                                    "sat" => tmp = SmtResult::Sat,
                                    "unsat" => tmp = SmtResult::Unsat,
                                    "unknown" => tmp = SmtResult::Unknown,
                                    _ => panic!("unknown result: {}", s),
                                }
                            }
                            None => {
                                res_str = "timeout";
                                tmp = SmtResult::Timeout;
                            }
                        }

                        println!("clause No.{} : {}", id, res_str);
                        if tmp < result {
                            result = tmp;
                        }
                    }

                    None => {
                        let path = format!("{}/clause_{}.dot", dir_path, id);
                        let mut dot_file = File::create(&path)?;
                        let s = sar_def.visualize();
                        dot_file.write_all(s.as_bytes())?;
                        dot_file.flush()?;
                    }
                }
            }

            if let Some(_) = com {
                println!("{}", result);
            }
        }

        err => {
            println!("{:?}", err);
        }
    }

    Ok(())
}
