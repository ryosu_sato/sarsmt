use crate::{rules::*, sar};

use std::collections::btree_map::{IntoIter, Iter, IterMut, Keys};
use std::collections::{BTreeMap, BTreeSet};
use std::fmt;

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct EArgId(usize);
impl fmt::Display for EArgId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "v{}", self.0)
    }
}
impl EArgId {
    pub fn new(ctx: &mut sar::Context) -> Self {
        let n = ctx.earg_cnt;

        ctx.earg_cnt += 1;

        EArgId(n)
    }

    pub fn from(n: usize) -> Self {
        EArgId(n)
    }
}

static mut EECNT: usize = 0;
#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct ExistEArgId(usize);
impl fmt::Display for ExistEArgId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "ev{}", self.0)
    }
}
impl ExistEArgId {
    pub fn new() -> Self {
        let a: usize;
        unsafe {
            EECNT += 1;
            a = EECNT;
        }

        ExistEArgId(a)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ETerm<Et, Lt> {
    Num(i32),
    Var(Et),
    EVar(ExistEArgId),
    Hd(Box<LTerm<Et, Lt>>),
    Fapp(String, Vec<ETerm<Et, Lt>>),
}
impl<Et: fmt::Display, Lt: fmt::Display> fmt::Display for ETerm<Et, Lt> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ETerm::Num(n) => write!(f, "{}", n),

            ETerm::Var(eaid) => write!(f, "{}", eaid),

            ETerm::EVar(eeaid) => write!(f, "{}", eeaid),

            ETerm::Hd(lt) => write!(f, "hd({})", lt),

            ETerm::Fapp(s, term_vec) => {
                write!(f, "({}", s);
                for t in term_vec {
                    write!(f, " {}", t);
                }
                write!(f, ")")
            }
        }
    }
}
impl<Et, Lt> ETerm<Et, Lt> {
    pub fn num(n: i32) -> Self {
        ETerm::Num(n)
    }

    pub fn var(eaid: Et) -> Self {
        ETerm::Var(eaid)
    }

    pub fn evar(eeaid: ExistEArgId) -> Self {
        ETerm::EVar(eeaid)
    }

    pub fn hd(lt: LTerm<Et, Lt>) -> Self {
        ETerm::Hd(Box::new(lt))
    }

    pub fn fapp(s: &str, t_vec: Vec<ETerm<Et, Lt>>) -> Self {
        ETerm::Fapp(s.to_string(), t_vec)
    }

    fn reduce(&mut self) {
        match self {
            ETerm::Hd(lt) => {
                lt.reduce();
                match lt as &mut LTerm<Et, Lt> {
                    LTerm::Nil => {
                        panic!("head of nil is invalid.");
                    }

                    LTerm::Cons(et, _) => {
                        et.reduce();
                        *self = std::mem::replace(et, ETerm::num(0));
                    }

                    _ => {}
                }
            }

            ETerm::Fapp(_, et_vec) => {
                for et in et_vec {
                    et.reduce();
                }
            }

            _ => {}
        }
    }
}

impl ETerm<EArgId, LArgId> {
    fn substitute(
        &self,
        et_map: &Vec<ETerm<String, String>>,
        lt_map: &Vec<LTerm<String, String>>,
        eeaid_map: &BTreeMap<ExistEArgId, std::string::String>,
        elaid_map: &BTreeMap<ExistLArgId, std::string::String>,
    ) -> ETerm<String, String> {
        match self {
            ETerm::Hd(lt) => ETerm::hd(lt.substitute(et_map, lt_map, eeaid_map, elaid_map)),

            ETerm::Var(EArgId(id)) => {
                if *id < et_map.len() {
                    et_map[*id].clone()
                } else {
                    panic!("")
                }
            }

            ETerm::EVar(eeaid) => ETerm::var(eeaid_map.get(eeaid).unwrap().clone()),

            ETerm::Fapp(s, et_vec) => ETerm::fapp(
                s,
                et_vec
                    .iter()
                    .map(|et| et.substitute(et_map, lt_map, eeaid_map, elaid_map))
                    .collect(),
            ),

            ETerm::Num(n) => ETerm::num(*n),
        }
    }
}
impl ETerm<String, String> {
    pub fn from(et: &ElementTerm) -> Self {
        match et {
            ElementTerm::ETerm0(n) => ETerm::num(n.1),

            ElementTerm::ETerm1(ev) => {
                let ElementVariable::EVar0(s) = ev as &ElementVariable;
                ETerm::var(s.to_string())
            }

            ElementTerm::ETerm2(_, _ /* head */, lt, _) => {
                ETerm::hd(LTerm::<String, String>::from(lt))
            }

            ElementTerm::ETerm3(_, f, et_vec, _) => {
                let FunctionSymbol::FuncSym0(s) = f as &FunctionSymbol;

                let mut v = vec![];

                for et2 in et_vec {
                    v.push(ETerm::<String, String>::from(et2));
                }

                ETerm::fapp(s, v)
            }
        }
    }
}

impl ETerm<EArgId, LArgId> {
    pub fn from(
        et: &EQElementTerm,
        is_map: &BTreeMap<String, EArgId>,
        ls_map: &BTreeMap<String, LArgId>,
        ees_map: &BTreeMap<String, ExistEArgId>,
        els_map: &BTreeMap<String, ExistLArgId>,
    ) -> Self {
        match et {
            EQElementTerm::EQETerm0(n) => ETerm::num(n.1),

            EQElementTerm::EQETerm1(ev) => {
                let ElementVariable::EVar0(s) = ev as &ElementVariable;
                ETerm::var(*is_map.get(s).unwrap())
            }

            EQElementTerm::EQETerm2(eev) => {
                let EQElementVariable::EQEVar0(s) = eev as &EQElementVariable;
                ETerm::evar(*ees_map.get(s).unwrap())
            }

            EQElementTerm::EQETerm3(_, _ /* head */, lt, _) => ETerm::hd(
                LTerm::<EArgId, LArgId>::from(lt, is_map, ls_map, ees_map, els_map),
            ),

            EQElementTerm::EQETerm4(_, f, et_vec, _) => {
                let FunctionSymbol::FuncSym0(s) = f as &FunctionSymbol;

                let mut v: Vec<ETerm<EArgId, LArgId>> = vec![];

                for et2 in et_vec {
                    v.push(ETerm::<EArgId, LArgId>::from(
                        et2, is_map, ls_map, ees_map, els_map,
                    ));
                }

                ETerm::fapp(s, v)
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct LArgId(usize);
impl fmt::Display for LArgId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "V{}", self.0)
    }
}
impl LArgId {
    pub fn new(ctx: &mut sar::Context) -> Self {
        let n = ctx.larg_cnt;

        ctx.larg_cnt += 1;

        LArgId(n)
    }

    pub fn from(n: usize) -> Self {
        LArgId(n)
    }
}

static mut ELCNT: usize = 0;
#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct ExistLArgId(usize);
impl fmt::Display for ExistLArgId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "EV{}", self.0)
    }
}
impl ExistLArgId {
    pub fn new() -> Self {
        let a: usize;
        unsafe {
            ELCNT += 1;
            a = ELCNT;
        }

        ExistLArgId(a)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum LTerm<Et, Lt> {
    Nil,
    LVar(Lt, isize), // Lvar(a,b)でリスト変数V_aのb番目の要素から始まるリスト
    ELVar(ExistLArgId, isize), // Lvar(a,b)でリスト変数V_aのb番目の要素から始まるリスト
    Cons(Box<ETerm<Et, Lt>>, Box<LTerm<Et, Lt>>),
}
impl<Et: fmt::Display, Lt: fmt::Display> fmt::Display for LTerm<Et, Lt> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LTerm::Nil => write!(f, "[]"),

            LTerm::LVar(laid, shift) => write!(f, "tl^{}{}{}({})", r"{", shift, r"}", laid),

            LTerm::ELVar(elaid, shift) => write!(f, "tl^{}{}{}({})", r"{", shift, r"}", elaid),

            LTerm::Cons(t, lt) => write!(f, "{} :: {}", t, lt),
        }
    }
}
impl<Et, Lt> LTerm<Et, Lt> {
    pub fn nil() -> Self {
        LTerm::Nil
    }

    pub fn lvar(laid: Lt, shift: isize) -> Self {
        LTerm::LVar(laid, shift)
    }

    pub fn elvar(elaid: ExistLArgId, shift: isize) -> Self {
        LTerm::ELVar(elaid, shift)
    }

    pub fn cons(t: ETerm<Et, Lt>, lt: LTerm<Et, Lt>) -> Self {
        LTerm::Cons(Box::new(t), Box::new(lt))
    }

    pub fn len_of_cons(&self) -> usize {
        match &self {
            LTerm::Cons(_, lt) => 1 + lt.len_of_cons(),
            _ => 0,
        }
    }

    fn reduce(&mut self) {
        match self {
            LTerm::Cons(et, lt) => {
                et.reduce();
                lt.reduce();
            }

            _ => {}
        }
    }
}

impl LTerm<EArgId, LArgId> {
    fn substitute(
        &self,
        et_map: &Vec<ETerm<String, String>>,
        lt_map: &Vec<LTerm<String, String>>,
        eeaid_map: &BTreeMap<ExistEArgId, std::string::String>,
        elaid_map: &BTreeMap<ExistLArgId, std::string::String>,
    ) -> LTerm<String, String> {
        match self {
            LTerm::LVar(laid1, tmp) => {
                let mut shift1 = *tmp;
                if let Some(mut lt) = lt_map.get(laid1.0) {
                    if shift1 < 0 {
                        panic!("");
                    }

                    while shift1 > 0 {
                        if let LTerm::Cons(_, lt2) = lt {
                            lt = lt2;
                            shift1 -= 1;
                        } else {
                            break;
                        }
                    }

                    match lt {
                        LTerm::Nil => LTerm::nil(),

                        LTerm::LVar(laid2, shift2) => {
                            LTerm::lvar(String::from(laid2), shift1 + *shift2)
                        }

                        _ => lt.clone(),
                    }
                } else {
                    panic!("")
                }
            }

            LTerm::Cons(et, lt) => LTerm::cons(
                et.substitute(et_map, lt_map, eeaid_map, elaid_map),
                lt.substitute(et_map, lt_map, eeaid_map, elaid_map),
            ),

            LTerm::ELVar(elaid, shift) => {
                LTerm::lvar(elaid_map.get(&elaid).unwrap().clone(), *shift)
            }

            LTerm::Nil => LTerm::nil(),
        }
    }
}
impl LTerm<String, String> {
    pub fn from(lt: &ListTerm) -> Self {
        match lt as &ListTerm {
            ListTerm::LTerm0(_ /* nil */) => LTerm::nil(),

            ListTerm::LTerm1(lv) => {
                let ListVariable::LVar0(s) = lv as &ListVariable;
                LTerm::lvar(s.to_string(), 0)
            }

            ListTerm::LTerm2(_, _ /* tail */, lt2, _) => match LTerm::<String, String>::from(lt2) {
                LTerm::Nil => {
                    panic!("tail of nil.");
                }

                LTerm::LVar(s, shift) => LTerm::lvar(s, shift + 1),

                LTerm::ELVar(_, _) => {
                    panic!("");
                }

                LTerm::Cons(_, b) => *b,
            },

            ListTerm::LTerm3(_, _ /* insert */, et2, lt2, _) => LTerm::cons(
                ETerm::<String, String>::from(et2),
                LTerm::<String, String>::from(lt2),
            ),
        }
    }
}

impl LTerm<EArgId, LArgId> {
    pub fn from(
        lt: &EQListTerm,
        is_map: &BTreeMap<String, EArgId>,
        ls_map: &BTreeMap<String, LArgId>,
        ees_map: &BTreeMap<String, ExistEArgId>,
        els_map: &BTreeMap<String, ExistLArgId>,
    ) -> Self {
        match lt {
            EQListTerm::EQLTerm0(_ /* nil */) => LTerm::nil(),

            EQListTerm::EQLTerm1(lv) => {
                let ListVariable::LVar0(s) = lv as &ListVariable;
                LTerm::lvar(*ls_map.get(s).unwrap(), 0)
            }

            EQListTerm::EQLTerm2(elv) => {
                let EQListVariable::EQLVar0(s) = elv as &EQListVariable;
                LTerm::elvar(*els_map.get(s).unwrap(), 0)
            }

            EQListTerm::EQLTerm3(_, _ /* tail */, lt2, _) => {
                match LTerm::<EArgId, LArgId>::from(lt2, is_map, ls_map, ees_map, els_map) {
                    LTerm::Nil => {
                        panic!("tail of nil.");
                    }

                    LTerm::LVar(s, shift) => LTerm::lvar(s, shift + 1),

                    LTerm::ELVar(_, _) => {
                        panic!("");
                    }

                    LTerm::Cons(_, b) => *b,
                }
            }

            EQListTerm::EQLTerm4(_, _ /* insert */, et2, lt2, _) => LTerm::cons(
                ETerm::<EArgId, LArgId>::from(et2, is_map, ls_map, ees_map, els_map),
                LTerm::<EArgId, LArgId>::from(lt2, is_map, ls_map, ees_map, els_map),
            ),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Arguments<Et, Lt>(BTreeMap<sar::ParamId, LTerm<Et, Lt>>);
impl<Et: fmt::Display, Lt: fmt::Display> fmt::Display for Arguments<Et, Lt> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "args : {}", r"{");
        for (pid, lt) in self {
            writeln!(f, "  x{} -> {}", pid, lt);
        }
        writeln!(f, "{}", r"}")
    }
}
impl<Et, Lt> std::iter::IntoIterator for Arguments<Et, Lt> {
    type Item = (sar::ParamId, LTerm<Et, Lt>);
    type IntoIter = IntoIter<sar::ParamId, LTerm<Et, Lt>>;
    fn into_iter(self) -> IntoIter<sar::ParamId, LTerm<Et, Lt>> {
        self.0.into_iter()
    }
}
impl<'a, Et, Lt> std::iter::IntoIterator for &'a Arguments<Et, Lt> {
    type Item = (&'a sar::ParamId, &'a LTerm<Et, Lt>);
    type IntoIter = Iter<'a, sar::ParamId, LTerm<Et, Lt>>;
    fn into_iter(self) -> Iter<'a, sar::ParamId, LTerm<Et, Lt>> {
        self.0.iter()
    }
}
impl<'a, Et, Lt> std::iter::IntoIterator for &'a mut Arguments<Et, Lt> {
    type Item = (&'a sar::ParamId, &'a mut LTerm<Et, Lt>);
    type IntoIter = IterMut<'a, sar::ParamId, LTerm<Et, Lt>>;
    fn into_iter(self) -> IterMut<'a, sar::ParamId, LTerm<Et, Lt>> {
        self.0.iter_mut()
    }
}
impl<Et, Lt> std::iter::FromIterator<(sar::ParamId, LTerm<Et, Lt>)> for Arguments<Et, Lt> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: std::iter::IntoIterator<Item = (sar::ParamId, LTerm<Et, Lt>)>,
    {
        Arguments(iter.into_iter().collect())
    }
}
impl<'a, Et: Eq + Ord, Lt: Eq + Ord> Arguments<Et, Lt> {
    pub fn new() -> Self {
        Arguments(BTreeMap::new())
    }

    pub fn insert(&mut self, pid: sar::ParamId, lt: LTerm<Et, Lt>) {
        self.0.insert(pid, lt);
    }

    pub fn keys(&self) -> Keys<sar::ParamId, LTerm<Et, Lt>> {
        self.0.keys()
    }

    pub fn remove(&mut self, pid: &sar::ParamId) -> Option<LTerm<Et, Lt>> {
        self.0.remove(pid)
    }

    pub fn iter(&'a self) -> Iter<'a, sar::ParamId, LTerm<Et, Lt>> {
        self.0.iter()
    }

    pub fn params(&self) -> BTreeSet<sar::ParamId> {
        self.keys().into_iter().map(|pid| *pid).collect()
    }

    pub fn max_len_of_cons(&self) -> usize {
        let mut m = 0;
        for (_, lt) in self {
            let tmp = lt.len_of_cons();
            if tmp > m {
                m = tmp;
            }
        }
        m
    }

    pub fn shift(&mut self, i: isize) {
        for (_, lt) in self {
            match lt {
                LTerm::LVar(_, shift) => {
                    *shift += i;
                }

                _ => panic!("cons or nil is in new args!"),
            }
        }
    }

    pub fn merge(&mut self, other: Self) {
        for (pid, lt) in other.0 {
            self.insert(pid, lt);
        }
    }

    // 重複している項を消去し，引数の置き換えの対応を返す
    pub fn remove_duplicates(&mut self) -> BTreeMap<sar::ParamId, sar::ParamId> {
        let mut params_to_remove = BTreeMap::new();
        let param_arg_vec: Vec<(sar::ParamId, &LTerm<Et, Lt>)> =
            self.iter().map(|(pid, lt)| (*pid, lt)).collect();
        for i in 0..param_arg_vec.len() {
            let (pid1, lt1) = param_arg_vec[i];
            for j in i + 1..param_arg_vec.len() {
                let (pid2, lt2) = param_arg_vec[j];
                if lt1 == lt2 {
                    if !params_to_remove.contains_key(&pid2) {
                        params_to_remove.insert(pid2, pid1);
                    }
                }
            }
        }

        for (pid_to_replace, _) in &params_to_remove {
            self.remove(pid_to_replace);
        }

        params_to_remove
    }

    pub fn remove_nil(&mut self) {
        *self = std::mem::replace(self, Arguments::new())
            .into_iter()
            .filter(|(_, b)| b != &LTerm::nil())
            .collect();
    }

    pub fn reduce(&mut self) {
        for (_, lt) in &mut self.0 {
            lt.reduce();
        }
    }

    pub fn contains_key(&self, pid: &sar::ParamId) -> bool {
        self.0.contains_key(pid)
    }
}
impl Arguments<String, String> {
    pub fn fillup_args(&mut self, ctx: &mut sar::Context) -> Vec<Vec<sar::ParamId>> {
        let mut range_of_lvar: BTreeMap<String, BTreeMap<isize, sar::ParamId>> = BTreeMap::new();
        for (pid, lt) in self.iter() {
            match lt {
                LTerm::LVar(laid, shift) => {
                    if let Some(btmap) = range_of_lvar.get_mut(laid) {
                        btmap.insert(*shift, *pid);
                    } else {
                        range_of_lvar.insert(
                            String::from(laid),
                            [(*shift, *pid)].iter().cloned().collect(),
                        );
                    }
                }

                _ => panic!("not normalized."),
            }
        }

        for (laid, btmap) in &mut range_of_lvar {
            let (&min, _) = btmap.iter().next().expect("");
            let (&max, _) = btmap.iter().next_back().expect("");
            for i in min..max + 1 {
                if !btmap.contains_key(&i) {
                    let lt = LTerm::lvar(String::from(laid), i);
                    let pid = sar::ParamId::new(ctx);
                    self.insert(pid, lt);
                    btmap.insert(i, pid);
                }
            }
        }

        let mut params_of_lvar = vec![];
        for (_, btmap) in &range_of_lvar {
            params_of_lvar.push(btmap.iter().map(|(_, &pid)| pid).collect());
        }

        params_of_lvar
    }
}

impl Arguments<EArgId, LArgId> {
    pub fn substitute(
        &self,
        et_map: &Vec<ETerm<String, String>>,
        lt_map: &Vec<LTerm<String, String>>,
        eeaid_map: &BTreeMap<ExistEArgId, std::string::String>,
        elaid_map: &BTreeMap<ExistLArgId, std::string::String>,
    ) -> Arguments<String, String> {
        let mut args = Arguments::new();
        for (pid, lt) in self {
            args.insert(*pid, lt.substitute(et_map, lt_map, eeaid_map, elaid_map));
        }
        args
    }
}

#[derive(Debug, Clone)]
pub struct Repetition<Et, Lt>(BTreeMap<sar::RepeatId, ETerm<Et, Lt>>);
impl<Et: fmt::Display, Lt: fmt::Display> fmt::Display for Repetition<Et, Lt> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "reps : {}", r"{");
        for (pid, lt) in self {
            writeln!(f, "  x{} -> {}", pid, lt);
        }
        writeln!(f, "{}", r"}")
    }
}
impl<Et, Lt> std::iter::IntoIterator for Repetition<Et, Lt> {
    type Item = (sar::RepeatId, ETerm<Et, Lt>);
    type IntoIter = IntoIter<sar::RepeatId, ETerm<Et, Lt>>;
    fn into_iter(self) -> IntoIter<sar::RepeatId, ETerm<Et, Lt>> {
        self.0.into_iter()
    }
}
impl<'a, Et, Lt> std::iter::IntoIterator for &'a Repetition<Et, Lt> {
    type Item = (&'a sar::RepeatId, &'a ETerm<Et, Lt>);
    type IntoIter = Iter<'a, sar::RepeatId, ETerm<Et, Lt>>;
    fn into_iter(self) -> Iter<'a, sar::RepeatId, ETerm<Et, Lt>> {
        self.0.iter()
    }
}
impl<'a, Et, Lt> std::iter::IntoIterator for &'a mut Repetition<Et, Lt> {
    type Item = (&'a sar::RepeatId, &'a mut ETerm<Et, Lt>);
    type IntoIter = IterMut<'a, sar::RepeatId, ETerm<Et, Lt>>;
    fn into_iter(self) -> IterMut<'a, sar::RepeatId, ETerm<Et, Lt>> {
        self.0.iter_mut()
    }
}
impl<Et, Lt> std::iter::FromIterator<(sar::RepeatId, ETerm<Et, Lt>)> for Repetition<Et, Lt> {
    fn from_iter<I>(iter: I) -> Self
    where
        I: std::iter::IntoIterator<Item = (sar::RepeatId, ETerm<Et, Lt>)>,
    {
        Repetition(iter.into_iter().collect())
    }
}
impl<'a, Et: Eq, Lt: Eq> Repetition<Et, Lt> {
    pub fn new() -> Self {
        Repetition(BTreeMap::new())
    }

    pub fn insert(&mut self, rid: sar::RepeatId, et: ETerm<Et, Lt>) {
        self.0.insert(rid, et);
    }

    pub fn keys(&self) -> Keys<sar::RepeatId, ETerm<Et, Lt>> {
        self.0.keys()
    }

    pub fn remove(&mut self, rid: &sar::RepeatId) -> Option<ETerm<Et, Lt>> {
        self.0.remove(rid)
    }

    pub fn iter(&'a self) -> Iter<'a, sar::RepeatId, ETerm<Et, Lt>> {
        self.0.iter()
    }

    pub fn params(&self) -> BTreeSet<sar::RepeatId> {
        self.keys().into_iter().map(|rid| *rid).collect()
    }

    pub fn merge(&mut self, other: Self) {
        for (rid, et) in other.0 {
            self.insert(rid, et);
        }
    }

    // 重複している項を消去し，引数の置き換えの対応を返す
    pub fn remove_duplicates(&mut self) -> BTreeMap<sar::RepeatId, sar::RepeatId> {
        let mut params_to_remove = BTreeMap::new();
        let param_arg_vec: Vec<(sar::RepeatId, &ETerm<Et, Lt>)> =
            self.iter().map(|(rid, et)| (*rid, et)).collect();
        for i in 0..param_arg_vec.len() {
            let (rid1, et1) = param_arg_vec[i];
            for j in i + 1..param_arg_vec.len() {
                let (rid2, et2) = param_arg_vec[j];
                if et1 == et2 {
                    if !params_to_remove.contains_key(&rid2) {
                        params_to_remove.insert(rid2, rid1);
                    }
                }
            }
        }

        for (rid_to_replace, _) in &params_to_remove {
            self.remove(rid_to_replace);
        }

        params_to_remove
    }

    pub fn contains_key(&self, rid: &sar::RepeatId) -> bool {
        self.0.contains_key(rid)
    }
}

impl Repetition<EArgId, LArgId> {
    pub fn substitute(
        &self,
        et_map: &Vec<ETerm<String, String>>,
        lt_map: &Vec<LTerm<String, String>>,
        eeaid_map: &BTreeMap<ExistEArgId, std::string::String>,
        elaid_map: &BTreeMap<ExistLArgId, std::string::String>,
    ) -> Repetition<String, String> {
        let mut reps = Repetition::new();
        for (rid, lt) in self {
            reps.insert(*rid, lt.substitute(et_map, lt_map, eeaid_map, elaid_map));
        }
        reps
    }
}

// #[test]
pub fn test() {
    let mut et: ETerm<usize, usize> = ETerm::hd(LTerm::cons(
        ETerm::fapp(
            "f",
            vec![
                ETerm::num(3),
                ETerm::hd(LTerm::cons(
                    ETerm::num(5),
                    LTerm::cons(ETerm::num(4), LTerm::nil()),
                )),
            ],
        ),
        LTerm::nil(),
    ));

    println!("{}", et);

    et.reduce();
    println!("{}", et);
}
