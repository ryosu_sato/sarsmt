mod argument;
mod rules;
mod sar;
mod solution;
mod verifier;

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if let (Some(com), Some(filepath), None) = (args.get(1), args.get(2), args.get(3)) {
        if com == "z3" || com == "hoice" || com == "eld" {
            verifier::run(Some(com), filepath);
        } else if com == "graph" {
            verifier::run(None, filepath);
        } else {
            println!("invalid arguments.");
        }
    } else {
        println!("invalid arguments.");
    }
}
