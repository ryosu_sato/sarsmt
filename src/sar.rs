use crate::{argument, rules};

use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::fmt;
use z3::ast::Ast;

pub fn counter() -> u32 {
    static mut CNT: u32 = 0;

    let ret;
    unsafe {
        CNT += 1;
        ret = CNT;
    }

    ret
}

pub fn generate_evariable() -> String {
    format!("v_{}", counter())
}

pub fn generate_lvariable() -> String {
    format!("V_{}", counter())
}

pub fn generate_eqevar() -> String {
    format!("ex{}", counter())
}

pub fn generate_eqlvar() -> String {
    format!("EX{}", counter())
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct ParamId(u32);
impl fmt::Display for ParamId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl ParamId {
    pub fn new(ctx: &mut Context) -> Self {
        let n = ctx.param_cnt;

        ctx.param_cnt += 1;

        ParamId(n)
    }
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct RepeatId(u32);
impl fmt::Display for RepeatId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl RepeatId {
    pub fn new(ctx: &mut Context) -> Self {
        let n = ctx.repeat_cnt;

        ctx.repeat_cnt += 1;

        RepeatId(n)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum TermInner {
    Num(i32),                // 整数
    Var(ParamId),            // SARの制約中に現れる変数およびSARの引数のid
    RVar(RepeatId),          // SARの制約中に現れる変数およびSARの引数のid
    Fapp(String, Vec<Term>), // 関数適用
}
impl TermInner {
    fn substitute(&mut self, pid: &ParamId, t: &Term) {
        match self {
            TermInner::Var(pid2) => {
                if pid == pid2 {
                    *self = t.0.clone()
                }
            }

            TermInner::Fapp(_, t_vec) => {
                for t2 in t_vec {
                    t2.0.substitute(pid, t);
                }
            }

            _ => {}
        }
    }

    fn get_all_vars(&self) -> BTreeSet<ParamId> {
        let mut ret = BTreeSet::new();

        match self {
            TermInner::Var(pid) => {
                ret.insert(*pid);
            }

            TermInner::Fapp(_, t_vec) => {
                for t in t_vec {
                    ret = ret.union(&t.get_all_vars()).map(|pid| *pid).collect();
                }
            }

            _ => {}
        }

        ret
    }

    fn get_all_rvars(&self) -> BTreeSet<RepeatId> {
        let mut ret = BTreeSet::new();

        match self {
            TermInner::RVar(rid) => {
                ret.insert(*rid);
            }

            TermInner::Fapp(_, t_vec) => {
                for t in t_vec {
                    ret = ret.union(&t.get_all_rvars()).map(|rid| *rid).collect();
                }
            }

            _ => {}
        }

        ret
    }

    fn to_chc(&self, s: &mut String) {
        match self {
            TermInner::Num(n) => *s += &format!("{}", n),

            TermInner::Var(pid) => *s += &format!("x{}_value", pid),

            TermInner::Fapp(f, t_vec) => {
                *s += &format!("({}", f);
                for t in t_vec {
                    *s += " ";
                    t.to_chc(s);
                }
                *s += ")";
            }

            TermInner::RVar(rid) => *s += &format!("r{}_value", rid),
        }
    }

    fn replace_prid(
        &mut self,
        param_map: &BTreeMap<ParamId, ParamId>,
        rep_map: &BTreeMap<RepeatId, RepeatId>,
    ) {
        match self {
            TermInner::Var(src) => {
                if let Some(tgt) = param_map.get(src) {
                    *src = *tgt;
                }
            }

            TermInner::RVar(src) => {
                if let Some(tgt) = rep_map.get(src) {
                    *src = *tgt;
                }
            }

            TermInner::Fapp(_, t_vec) => {
                for t2 in t_vec {
                    t2.0.replace_prid(param_map, rep_map);
                }
            }

            _ => {}
        }
    }

    fn to_ast<'ctx>(&self, ctx: &'ctx z3::Context) -> z3::ast::Int<'ctx> {
        match self {
            TermInner::Num(n) => z3::ast::Int::from_u64(ctx, *n as u64),

            TermInner::Var(pid) => {
                z3::ast::Int::new_const(ctx, z3::Symbol::String(format!("x{}_v", pid)))
            }

            TermInner::RVar(rid) => {
                z3::ast::Int::new_const(ctx, z3::Symbol::String(format!("r{}_v", rid)))
            }

            TermInner::Fapp(s, t_vec) => {
                if s == "+" && t_vec.len() >= 2 {
                    let mut v: VecDeque<z3::ast::Int> =
                        t_vec.into_iter().map(|t| t.to_ast(ctx)).collect();
                    let top = v.pop_front().expect("hoge");
                    v.iter().fold(top, |sum, a| sum + a)
                } else if s == "-" && t_vec.len() >= 2 {
                    let mut v: VecDeque<z3::ast::Int> =
                        t_vec.into_iter().map(|t| t.to_ast(ctx)).collect();
                    let top = v.pop_front().expect("hoge");
                    v.iter().fold(top, |sum, a| sum - a)
                } else if s == "*" && t_vec.len() >= 2 {
                    let mut v: VecDeque<z3::ast::Int> =
                        t_vec.into_iter().map(|t| t.to_ast(ctx)).collect();
                    let top = v.pop_front().expect("hoge");
                    v.iter().fold(top, |sum, a| sum * a)
                } else if s == "-" && t_vec.len() == 1 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    -ast1
                } else if s == "div" && t_vec.len() == 2 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    let ast2 = t_vec[1].to_ast(ctx);
                    ast1.div(&ast2)
                } else if s == "rem" && t_vec.len() == 2 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    let ast2 = t_vec[1].to_ast(ctx);
                    ast1.rem(&ast2)
                } else if s == "mod" && t_vec.len() == 2 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    let ast2 = t_vec[1].to_ast(ctx);
                    ast1.modulo(&ast2)
                } else if s == "^" && t_vec.len() == 2 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    let ast2 = t_vec[1].to_ast(ctx);
                    ast1.power(&ast2)
                } else {
                    panic!("invalid operator");
                }
            }
        }
    }
}

impl fmt::Display for TermInner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            TermInner::Num(n) => write!(f, "{}", n),

            TermInner::Var(pid) => write!(f, "x{}", pid),

            TermInner::RVar(rid) => write!(f, "r{}", rid),

            TermInner::Fapp(s, t_vec) => {
                if s == "+" && t_vec.len() >= 2 {
                    write!(f, "({}", t_vec[0]);
                    for i in 1..t_vec.len() {
                        write!(f, " + {}", t_vec[i]);
                    }
                    write!(f, ")")
                } else if s == "-" && t_vec.len() >= 2 {
                    write!(f, "({}", t_vec[0]);
                    for i in 1..t_vec.len() {
                        write!(f, " - {}", t_vec[i]);
                    }
                    write!(f, ")")
                } else if s == "*" && t_vec.len() >= 2 {
                    write!(f, "({}", t_vec[0]);
                    for i in 1..t_vec.len() {
                        write!(f, " * {}", t_vec[i]);
                    }
                    write!(f, ")")
                } else if s == "-" && t_vec.len() == 1 {
                    write!(f, "-{}", t_vec[0])
                } else if s == "div" && t_vec.len() == 2 {
                    write!(f, "({} / {})", t_vec[0], t_vec[1])
                } else if s == "rem" && t_vec.len() == 2 {
                    write!(f, "({} rem {})", t_vec[0], t_vec[1])
                } else if s == "mod" && t_vec.len() == 2 {
                    write!(f, "({} % {})", t_vec[0], t_vec[1])
                } else if s == "^" && t_vec.len() == 2 {
                    write!(f, "({} ^ {})", t_vec[0], t_vec[1])
                } else {
                    panic!("invalid operator");
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Term(TermInner);
impl Term {
    pub fn num(n: i32) -> Self {
        Term(TermInner::Num(n))
    }

    pub fn var(pid: ParamId) -> Self {
        Term(TermInner::Var(pid))
    }

    pub fn rvar(rid: RepeatId) -> Self {
        Term(TermInner::RVar(rid))
    }

    pub fn fapp(s: &str, t_vec: Vec<Term>) -> Self {
        Term(TermInner::Fapp(s.to_string(), t_vec))
    }

    fn substitute(&mut self, pid: &ParamId, t: &Term) {
        self.0.substitute(pid, &t);
    }

    fn get_all_vars(&self) -> BTreeSet<ParamId> {
        self.0.get_all_vars()
    }

    fn get_all_rvars(&self) -> BTreeSet<RepeatId> {
        self.0.get_all_rvars()
    }

    fn from_arg_term(
        at: &argument::ETerm<String, String>,
        ctx: &mut Context,
        new_args: &mut argument::Arguments<String, String>,
        new_reps: &mut argument::Repetition<String, String>,
    ) -> Self {
        match at {
            argument::ETerm::Num(n) => Term::num(*n),

            argument::ETerm::Var(aid) => {
                let rid = RepeatId::new(ctx);
                new_reps.insert(rid, argument::ETerm::var(String::from(aid)));
                Term::rvar(rid)
            }

            argument::ETerm::EVar(_) => {
                panic!("");
            }

            argument::ETerm::Hd(lt) => match lt as &argument::LTerm<String, String> {
                argument::LTerm::Nil => panic!("head of nil is invalid"),

                argument::LTerm::LVar(laid, shift) => {
                    let pid = ParamId::new(ctx);
                    new_args.insert(pid, argument::LTerm::lvar(String::from(laid), *shift));
                    Term::var(pid)
                }

                argument::LTerm::ELVar(_, _) => {
                    panic!("");
                }

                argument::LTerm::Cons(at2, _) => Term::from_arg_term(at2, ctx, new_args, new_reps),
            },

            argument::ETerm::Fapp(s, at_vec) => {
                let mut term_vec = vec![];

                for at2 in at_vec {
                    term_vec.push(Term::from_arg_term(at2, ctx, new_args, new_reps));
                }

                Term::fapp(s, term_vec)
            }
        }
    }

    fn to_chc(&self, s: &mut String) {
        self.0.to_chc(s);
    }

    fn replace_prid(
        &mut self,
        param_map: &BTreeMap<ParamId, ParamId>,
        rep_map: &BTreeMap<RepeatId, RepeatId>,
    ) {
        self.0.replace_prid(param_map, rep_map);
    }

    fn to_ast<'ctx>(&self, ctx: &'ctx z3::Context) -> z3::ast::Int<'ctx> {
        self.0.to_ast(ctx)
    }

    pub fn from(term: &rules::Term, pid_vec: &Vec<ParamId>, rid_vec: &Vec<RepeatId>) -> Self {
        match term {
            rules::Term::Term0(n) => Term::num(n.1),
            rules::Term::Term1(s) => {
                Term::var(pid_vec[s.to_string().split_off(2).parse::<usize>().unwrap()])
            }
            rules::Term::Term2(s) => {
                Term::rvar(rid_vec[s.to_string().split_off(2).parse::<usize>().unwrap()])
            }
            rules::Term::Term3(_, f, t_vec, _) => {
                let rules::FunctionSymbol::FuncSym0(s) = f as &_;
                Term::fapp(
                    s,
                    t_vec
                        .iter()
                        .map(|t| Term::from(t, pid_vec, rid_vec))
                        .collect(),
                )
            }
        }
    }
}
impl fmt::Display for Term {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
enum LabelInner {
    Top,
    Bot,
    And(Vec<Label>),
    Or(Vec<Label>),
    Not(Box<Label>),
    Pad(ParamId), // padding character
    Papp(String, Vec<Term>),
}
impl LabelInner {
    fn substitute(&mut self, pid: &ParamId, t: &Term) {
        match self {
            LabelInner::And(l_vec) => {
                let mut v = vec![];

                for l in l_vec as &mut Vec<Label> {
                    l.0.substitute(pid, t);

                    match &mut l.0 {
                        LabelInner::Top => {}

                        LabelInner::Bot => {
                            *self = LabelInner::Bot;
                            return;
                        }

                        l_sub => {
                            let b = std::mem::replace(l_sub, LabelInner::Top);
                            v.push(Label(b));
                        }
                    }
                }

                if v.is_empty() {
                    *self = LabelInner::Top;
                } else if v.len() == 1 {
                    *self = v.pop().unwrap().0;
                } else {
                    *self = LabelInner::And(v);
                }
            }

            LabelInner::Or(l_vec) => {
                let mut v = vec![];

                for l in l_vec as &mut Vec<Label> {
                    l.0.substitute(pid, t);

                    match &mut l.0 {
                        LabelInner::Top => {
                            *self = LabelInner::Top;
                            return;
                        }

                        LabelInner::Bot => {}

                        l_sub => {
                            let b = std::mem::replace(l_sub, LabelInner::Top);
                            v.push(Label(b));
                        }
                    }
                }

                if v.is_empty() {
                    *self = LabelInner::Bot;
                } else if v.len() == 1 {
                    *self = v.pop().unwrap().0;
                } else {
                    *self = LabelInner::Or(v);
                }
            }

            LabelInner::Not(l1) => {
                l1.0.substitute(pid, t);

                match &mut l1.0 {
                    LabelInner::Not(l2) => {
                        let b = std::mem::replace(&mut l2.0, LabelInner::Top);
                        *self = b;
                    }

                    _ => {}
                }
            }

            LabelInner::Pad(pid2) => {
                if pid == pid2 {
                    *self = LabelInner::Bot;
                }
            }

            LabelInner::Papp(_, t_vec) => {
                for t2 in t_vec {
                    t2.substitute(pid, t)
                }
            }

            _ => {}
        }
    }

    fn replace_var_with_padding(&mut self, pid: &ParamId) {
        match self {
            LabelInner::And(l_vec) | LabelInner::Or(l_vec) => {
                for l in l_vec {
                    l.0.replace_var_with_padding(pid);
                }
            }

            LabelInner::Not(l1) => {
                l1.0.replace_var_with_padding(pid);
            }

            LabelInner::Pad(pid2) => {
                if pid == pid2 {
                    *self = LabelInner::Top;
                }
            }

            LabelInner::Papp(_, t_vec) => {
                let mut isin = false;

                for t2 in t_vec {
                    if t2.get_all_vars().contains(pid) {
                        isin = true;
                        break;
                    }
                }

                if isin {
                    *self = LabelInner::Bot;
                }
            }

            _ => {}
        }
    }

    fn check_var_not_padding(&mut self, pid: &ParamId) {
        match self {
            LabelInner::And(l_vec) | LabelInner::Or(l_vec) => {
                for l in l_vec {
                    l.0.check_var_not_padding(pid);
                }
            }

            LabelInner::Not(l1) => {
                l1.0.check_var_not_padding(pid);
            }

            LabelInner::Pad(pid2) => {
                if pid == pid2 {
                    *self = LabelInner::Bot;
                }
            }

            _ => {}
        }
    }

    fn get_all_vars(&self) -> BTreeSet<ParamId> {
        let mut ret = BTreeSet::new();

        match self {
            LabelInner::And(l_vec) | LabelInner::Or(l_vec) => {
                for l in l_vec {
                    ret = ret.union(&l.get_all_vars()).map(|pid| *pid).collect();
                }
            }

            LabelInner::Not(l) => {
                ret = l.get_all_vars();
            }

            LabelInner::Pad(pid) => {
                ret.insert(*pid);
            }

            LabelInner::Papp(_, t_vec) => {
                for t in t_vec {
                    ret = ret.union(&t.get_all_vars()).map(|pid| *pid).collect();
                }
            }

            _ => {}
        }

        ret
    }

    fn get_all_rvars(&self) -> BTreeSet<RepeatId> {
        let mut ret = BTreeSet::new();

        match self {
            LabelInner::And(l_vec) | LabelInner::Or(l_vec) => {
                for l in l_vec {
                    ret = ret.union(&l.get_all_rvars()).map(|rid| *rid).collect();
                }
            }

            LabelInner::Not(l) => {
                ret = l.get_all_rvars();
            }

            LabelInner::Papp(_, t_vec) => {
                for t in t_vec {
                    ret = ret.union(&t.get_all_rvars()).map(|rid| *rid).collect();
                }
            }

            _ => {}
        }

        ret
    }

    fn to_chc(&self, s: &mut String) {
        match self {
            LabelInner::Top => *s += "true",

            LabelInner::Bot => *s += "false",

            LabelInner::And(l_vec) => {
                *s += "(and";
                for l in l_vec {
                    *s += " ";
                    l.to_chc(s);
                }
                *s += ")";
            }

            LabelInner::Or(l_vec) => {
                *s += "(or";
                for l in l_vec {
                    *s += " ";
                    l.to_chc(s);
                }
                *s += ")";
            }

            LabelInner::Not(l1) => {
                *s += "(not ";
                l1.to_chc(s);
                *s += ")";
            }

            LabelInner::Pad(pid) => {
                *s += &format!("(not x{}_bool)", pid);
            }

            LabelInner::Papp(p, t_vec) => {
                let mut variables_in_terms = BTreeSet::new();

                *s += &format!("(and ");

                {
                    if p == "!=" {
                        *s += &format!("(not (=");
                    } else {
                        *s += &format!("({}", p);
                    }

                    for t in t_vec {
                        *s += " ";
                        t.to_chc(s);
                        variables_in_terms = variables_in_terms
                            .union(&t.get_all_vars())
                            .map(|pid| *pid)
                            .collect();
                    }

                    if p == "!=" {
                        *s += "))";
                    } else {
                        *s += ")";
                    }
                }

                for pid in variables_in_terms {
                    *s += &format!(" x{}_bool", pid);
                }

                *s += ")";
            }
        }
    }

    fn replace_prid(
        &mut self,
        param_map: &BTreeMap<ParamId, ParamId>,
        rep_map: &BTreeMap<RepeatId, RepeatId>,
    ) {
        match self {
            LabelInner::And(l_vec) => {
                for l in l_vec as &mut Vec<Label> {
                    l.0.replace_prid(param_map, rep_map);
                }
            }

            LabelInner::Or(l_vec) => {
                for l in l_vec as &mut Vec<Label> {
                    l.0.replace_prid(param_map, rep_map);
                }
            }

            LabelInner::Not(l1) => {
                l1.0.replace_prid(param_map, rep_map);
            }

            LabelInner::Pad(src) => {
                if let Some(tgt) = param_map.get(src) {
                    *src = *tgt;
                }
            }

            LabelInner::Papp(_, t_vec) => {
                for t2 in t_vec {
                    t2.replace_prid(param_map, rep_map);
                }
            }

            _ => {}
        }
    }

    fn to_ast<'ctx>(&self, ctx: &'ctx z3::Context) -> z3::ast::Bool<'ctx> {
        match self {
            LabelInner::Top => z3::ast::Bool::from_bool(ctx, true),

            LabelInner::Bot => z3::ast::Bool::from_bool(ctx, false),

            LabelInner::And(l_vec) => {
                let v: Vec<z3::ast::Bool> = l_vec.into_iter().map(|l| l.to_ast(ctx)).collect();
                z3::ast::Bool::and(ctx, &(v.iter().collect::<Vec<&z3::ast::Bool>>()))
            }

            LabelInner::Or(l_vec) => {
                let v: Vec<z3::ast::Bool> = l_vec.into_iter().map(|l| l.to_ast(ctx)).collect();
                z3::ast::Bool::or(ctx, &(v.iter().collect::<Vec<&z3::ast::Bool>>()))
            }

            LabelInner::Not(l1) => l1.to_ast(ctx).not(),

            LabelInner::Pad(pid) => {
                z3::ast::Bool::new_const(ctx, z3::Symbol::String(format!("x{}_b", pid))).not()
            }

            LabelInner::Papp(s, t_vec) => {
                let mut variables_in_terms = BTreeSet::new();
                for t in t_vec {
                    variables_in_terms = variables_in_terms
                        .union(&t.get_all_vars())
                        .map(|pid| *pid)
                        .collect();
                }
                let mut v: VecDeque<z3::ast::Bool> = variables_in_terms
                    .iter()
                    .map(|pid| {
                        z3::ast::Bool::new_const(ctx, z3::Symbol::String(format!("x{}_b", pid)))
                    })
                    .collect();

                if t_vec.len() == 2 {
                    let ast1 = t_vec[0].to_ast(ctx);
                    let ast2 = t_vec[1].to_ast(ctx);
                    if s == "=" {
                        let tmp = ast1._eq(&ast2);
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else if s == "!=" {
                        let tmp = ast1._eq(&ast2).not();
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else if s == "<" {
                        let tmp = ast1.lt(&ast2);
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else if s == "<=" {
                        let tmp = ast1.le(&ast2);
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else if s == ">" {
                        let tmp = ast1.gt(&ast2);
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else if s == ">=" {
                        let tmp = ast1.ge(&ast2);
                        v.push_front(tmp);
                        let v: Vec<&z3::ast::Bool> = v.iter().collect();
                        z3::ast::Bool::and(ctx, &v)
                    } else {
                        panic!("invalid predicate");
                    }
                } else {
                    panic!("invalid predicate");
                }
            }
        }
    }
}

impl fmt::Display for LabelInner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LabelInner::Top => write!(f, "⊤"),

            LabelInner::Bot => write!(f, "⊥"),

            LabelInner::And(l_vec) => {
                if l_vec.len() == 1 {
                    write!(f, "{}", l_vec[0])
                } else if l_vec.len() > 1 {
                    write!(f, "({}", l_vec[0]);
                    for i in 1..l_vec.len() {
                        write!(f, " ∧ {}", l_vec[i]);
                    }
                    write!(f, ")")
                } else {
                    panic!("")
                }
            }

            LabelInner::Or(l_vec) => {
                if l_vec.len() == 1 {
                    write!(f, "{}", l_vec[0])
                } else if l_vec.len() > 1 {
                    write!(f, "({}", l_vec[0]);
                    for i in 1..l_vec.len() {
                        write!(f, " ∨ {}", l_vec[i]);
                    }
                    write!(f, ")")
                } else {
                    panic!("")
                }
            }

            LabelInner::Not(l1) => write!(f, "¬{}", l1),

            LabelInner::Pad(pid) => write!(f, "(x{} = ◇)", pid),

            LabelInner::Papp(s, t_vec) => {
                if t_vec.len() == 2 {
                    if s == "=" {
                        write!(f, "({} = {})", t_vec[0], t_vec[1])
                    } else if s == "!=" {
                        write!(f, "({} ≠ {})", t_vec[0], t_vec[1])
                    } else if s == "<" {
                        write!(f, "({} < {})", t_vec[0], t_vec[1])
                    } else if s == "<=" {
                        write!(f, "({} ≤ {})", t_vec[0], t_vec[1])
                    } else if s == ">" {
                        write!(f, "({} > {})", t_vec[0], t_vec[1])
                    } else if s == ">=" {
                        write!(f, "({} ≥ {})", t_vec[0], t_vec[1])
                    } else {
                        panic!("invalid predicate");
                    }
                } else {
                    panic!("invalid predicate");
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Label(LabelInner);
impl Label {
    pub fn top() -> Self {
        Label(LabelInner::Top)
    }

    pub fn bot() -> Self {
        Label(LabelInner::Bot)
    }

    pub fn and(mut l_vec: Vec<Label>) -> Self {
        l_vec = l_vec
            .into_iter()
            .filter(|l| l != &Label(LabelInner::Top))
            .collect();

        if l_vec.contains(&Label(LabelInner::Bot)) {
            Label::bot()
        } else if l_vec.is_empty() {
            Label(LabelInner::Top)
        } else if l_vec.len() == 1 {
            l_vec.pop().unwrap()
        } else {
            Label(LabelInner::And(l_vec))
        }
    }

    pub fn or(mut l_vec: Vec<Label>) -> Self {
        l_vec = l_vec
            .into_iter()
            .filter(|l| l != &Label(LabelInner::Bot))
            .collect();

        if l_vec.contains(&Label(LabelInner::Top)) {
            Label::top()
        } else if l_vec.is_empty() {
            Label(LabelInner::Bot)
        } else if l_vec.len() == 1 {
            l_vec.pop().unwrap()
        } else {
            Label(LabelInner::Or(l_vec))
        }
    }

    pub fn not(l: Label) -> Self {
        match l {
            Label(LabelInner::Top) => Label::bot(),
            Label(LabelInner::Bot) => Label::top(),
            Label(LabelInner::Not(l2)) => *l2,
            _ => Label(LabelInner::Not(Box::new(l))),
        }
    }

    pub fn pad(pid: ParamId) -> Self {
        Label(LabelInner::Pad(pid))
    }

    pub fn papp(s: &str, t_vec: Vec<Term>) -> Self {
        Label(LabelInner::Papp(s.to_string(), t_vec))
    }

    pub fn substitute(&mut self, pid: &ParamId, t: &Term) {
        self.0.substitute(pid, t);
    }

    fn replace_var_with_padding(&mut self, pid: &ParamId) {
        self.0.replace_var_with_padding(pid);
    }

    fn check_var_not_padding(&mut self, pid: &ParamId) {
        self.0.check_var_not_padding(pid);
    }

    fn get_all_vars(&self) -> BTreeSet<ParamId> {
        self.0.get_all_vars()
    }

    fn get_all_rvars(&self) -> BTreeSet<RepeatId> {
        self.0.get_all_rvars()
    }

    fn to_chc(&self, s: &mut String) {
        self.0.to_chc(s);
    }

    fn replace_prid(
        &mut self,
        param_map: &BTreeMap<ParamId, ParamId>,
        rep_map: &BTreeMap<RepeatId, RepeatId>,
    ) {
        self.0.replace_prid(param_map, rep_map);
    }

    fn to_ast<'ctx>(&self, ctx: &'ctx z3::Context) -> z3::ast::Bool<'ctx> {
        self.0.to_ast(ctx)
    }

    pub fn from(label: &rules::Label, pid_vec: &Vec<ParamId>, rid_vec: &Vec<RepeatId>) -> Self {
        match label {
            rules::Label::Label0(_ /* true */) => Label::top(),

            rules::Label::Label1(_ /* false */) => Label::bot(),

            rules::Label::Label2(_, _ /* and */, label_vec, _) => Label::and(
                label_vec
                    .iter()
                    .map(|lbl| Label::from(lbl, pid_vec, rid_vec))
                    .collect(),
            ),

            rules::Label::Label3(_, _ /* or */, label_vec, _) => Label::or(
                label_vec
                    .iter()
                    .map(|lbl| Label::from(lbl, pid_vec, rid_vec))
                    .collect(),
            ),

            rules::Label::Label4(_, _ /* not */, label, _) => {
                Label::not(Label::from(label, pid_vec, rid_vec))
            }

            rules::Label::Label5(_, _ /* pad */, s, _) => {
                Label::pad(pid_vec[s.to_string().split_off(2).parse::<usize>().unwrap()])
            }

            rules::Label::Label6(_, pred_sym, t_vec, _) => {
                let rules::PredicateSymbol::PredSym0(s) = pred_sym as &_;

                Label::papp(
                    s,
                    t_vec
                        .iter()
                        .map(|t| Term::from(t, pid_vec, rid_vec))
                        .collect(),
                )
            }
        }
    }
}

impl fmt::Display for Label {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

//
//
//
//
//
//
//
//

#[derive(Debug, PartialEq, Eq, Clone, Copy, PartialOrd, Ord)]
pub struct State(u32);
impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "q{}", self.0)
    }
}
impl State {
    pub fn new(ctx: &mut Context) -> Self {
        let n = ctx.state_cnt;

        ctx.state_cnt += 1;

        State(n)
    }
}

#[derive(Debug, Clone)]
pub struct Transitions {
    transitions: BTreeMap<State, BTreeMap<State, Label>>,
    transitions_rev: BTreeMap<State, BTreeSet<State>>,
}
impl fmt::Display for Transitions {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "transitions: {}", r"{");

        for (s1, map) in &self.transitions {
            for (s2, l) in map {
                writeln!(f, "  {} -> {}: {}", s1, s2, l);
            }
        }

        write!(f, "{}", r"}")
    }
}
impl Transitions {
    pub fn new() -> Self {
        Transitions {
            transitions: BTreeMap::new(),
            transitions_rev: BTreeMap::new(),
        }
    }

    pub fn insert(&mut self, from: State, to: State, l: Label) {
        self.transitions
            .entry(from)
            .or_insert(BTreeMap::new())
            .insert(to, l);

        self.transitions_rev
            .entry(to)
            .or_insert(BTreeSet::new())
            .insert(from);
    }

    fn remove(&mut self, from: State, to: State) {
        if let Some(map) = self.transitions.get_mut(&from) {
            map.remove(&to);
        }
        if let Some(map) = self.transitions_rev.get_mut(&to) {
            map.remove(&from);
        }
    }

    fn remove_around_state(&mut self, s: &State) {
        for (&s2, _) in self.transitions.get(&s).unwrap_or(&BTreeMap::new()) {
            self.transitions_rev.get_mut(&s2).expect("hoge").remove(&s);
        }

        for &s1 in self.transitions_rev.get(&s).unwrap_or(&BTreeSet::new()) {
            self.transitions.get_mut(&s1).expect("hoge").remove(&s);
        }

        self.transitions.remove(&s);
        self.transitions_rev.remove(&s);
    }

    fn dfs(&self, s1: State, set: &mut BTreeSet<State>) {
        for (&s2, _) in self.transitions.get(&s1).unwrap_or(&BTreeMap::new()) {
            if !set.contains(&s2) {
                set.insert(s2);
                self.dfs(s2, set);
            }
        }
    }

    fn dfs_reverse(&self, s2: State, set: &mut BTreeSet<State>) {
        for &s1 in self.transitions_rev.get(&s2).unwrap_or(&BTreeSet::new()) {
            if !set.contains(&s1) {
                set.insert(s1);
                self.dfs_reverse(s1, set);
            }
        }
    }

    // falseと同値な遷移を消去
    fn remove_unnecessary_transitions(&mut self) {
        let mut pairs_to_remove = vec![];

        for (s1, map) in &self.transitions {
            for (s2, l) in map {
                let cfg = z3::Config::new();
                let ctx = z3::Context::new(&cfg);
                let formula = l.to_ast(&ctx);
                let solver = z3::Solver::new(&ctx);
                solver.assert(&formula);
                match solver.check() {
                    z3::SatResult::Unsat => {
                        pairs_to_remove.push((*s1, *s2));
                    }

                    z3::SatResult::Unknown => panic!(format!("unknown: {}", formula)),

                    _ => {}
                }
            }
        }

        for (s1, s2) in pairs_to_remove {
            self.remove(s1, s2);
        }
    }

    fn clone_all(&self) -> BTreeMap<(State, State), Label> {
        let mut ret = BTreeMap::new();

        for (s1, map) in &self.transitions {
            for (s2, l) in map {
                ret.insert((*s1, *s2), l.clone());
            }
        }

        ret
    }

    fn get_map(&self, from: State) -> Option<&BTreeMap<State, Label>> {
        self.transitions.get(&from)
    }

    fn get_all(&self) -> BTreeMap<(State, State), &Label> {
        let mut ret = BTreeMap::new();

        for (s1, map) in &self.transitions {
            for (s2, l) in map {
                ret.insert((*s1, *s2), l);
            }
        }

        ret
    }

    fn get_all_mut(&mut self) -> BTreeMap<(State, State), &mut Label> {
        let mut ret = BTreeMap::new();

        for (s1, map) in &mut self.transitions {
            for (s2, l) in map {
                ret.insert((*s1, *s2), l);
            }
        }

        ret
    }

    fn get(&self, from: &State, to: &State) -> Option<&Label> {
        let a = self.transitions.get(from)?;
        a.get(to)
    }
}

#[derive(Debug, Clone)]
pub struct Context {
    sarid: u32,
    sar_name: String,
    state_cnt: u32,
    pub earg_cnt: usize,
    pub larg_cnt: usize,
    param_cnt: u32,
    repeat_cnt: u32,
}
impl Context {
    pub fn new() -> Self {
        static mut CNT: u32 = 0;

        let sarid;
        unsafe {
            sarid = CNT;
            CNT += 1;
        }

        Context {
            sarid,
            sar_name: format!("SAR-{}", sarid),
            state_cnt: 0,
            earg_cnt: 0,
            larg_cnt: 0,
            param_cnt: 0,
            repeat_cnt: 0,
        }
    }
}

// "states" must have all states contained in all transitions
// "parameters" must have all variables contained in all transitions
#[derive(Debug, Clone)]
pub struct SymbolicAutomaticRelation<Et, Lt> {
    states: BTreeSet<State>,         // uniquely determined from transitions
    initial_states: BTreeSet<State>, // initialy determined
    final_states: BTreeSet<State>,
    arguments: argument::Arguments<Et, Lt>,
    repetitions: argument::Repetition<Et, Lt>,
    transitions: Transitions,
    context: Context,
}
impl<Et: fmt::Display, Lt: fmt::Display> fmt::Display for SymbolicAutomaticRelation<Et, Lt> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "{}", r"sar : {");

        write!(f, "  initial states : [");
        for is in &self.initial_states {
            write!(f, " {},", is);
        }
        writeln!(f, "]");

        write!(f, "  final states : [");
        for fs in &self.final_states {
            write!(f, " {},", fs);
        }
        writeln!(f, "]");

        write!(f, "  states : [");
        for s in &self.states {
            write!(f, " {},", s);
        }
        writeln!(f, "]");

        write!(f, "  arguments : {}", r"{");
        for (&pid, lt) in &self.arguments {
            write!(f, "    x{} -> {}", pid, lt);
        }
        writeln!(f, "  {}", r"}");

        write!(f, "  repetitions : {}", r"{");
        for (&rid, et) in &self.repetitions {
            write!(f, "    r{} -> {}", rid, et);
        }
        writeln!(f, "  {}", r"}");

        writeln!(f, "  transitions: {}", self.transitions);

        writeln!(f, "  context: {:?}", self.context);

        write!(f, "{}", r"}")
    }
}
impl<Et, Lt> SymbolicAutomaticRelation<Et, Lt>
where
    Et: Eq + Ord + Clone + fmt::Display + fmt::Debug,
    Lt: Eq + Ord + Clone + fmt::Display + fmt::Debug,
{
    pub fn top() -> Self {
        let mut ctx = Context::new();
        ctx.sar_name = format!("top");

        let s0 = State::new(&mut ctx);

        let is_set = [s0].iter().map(|s| *s).collect::<BTreeSet<_>>();
        let fs_set = is_set.clone();

        let args = argument::Arguments::new();

        let reps = argument::Repetition::new();

        let trans = Transitions::new();

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    pub fn bot() -> Self {
        let mut ctx = Context::new();
        ctx.sar_name = format!("bot");

        let is_set = BTreeSet::new();
        let fs_set = BTreeSet::new();

        let args = argument::Arguments::new();

        let reps = argument::Repetition::new();

        let trans = Transitions::new();

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    pub fn from(
        is_set: BTreeSet<State>,
        fs_set: BTreeSet<State>,
        mut args: argument::Arguments<Et, Lt>,
        reps: argument::Repetition<Et, Lt>,
        mut trans: Transitions,
        ctx: Context,
    ) -> Self {
        let mut states = BTreeSet::new();
        let mut vars = BTreeSet::new();
        let mut rvars = BTreeSet::new();

        for &s in &is_set {
            states.insert(s);
        }

        for &s in &fs_set {
            states.insert(s);
        }

        for ((s1, s2), l) in trans.get_all() {
            states.insert(s1);
            states.insert(s2);

            vars = vars.union(&l.get_all_vars()).map(|pid| *pid).collect();

            rvars = rvars.union(&l.get_all_rvars()).map(|rid| *rid).collect();
        }

        args.reduce();

        if !vars.is_subset(&args.params()) || !rvars.is_subset(&reps.params()) {
            panic!("invalid params.");
        }

        let padding_labels = Label::and(args.params().iter().map(|pid| Label::pad(*pid)).collect());

        for s in &states {
            if let None = trans.get(s, s) {
                trans.insert(*s, *s, Label::bot());
            }
        }

        for ((s1, s2), l) in trans.get_all_mut() {
            if s1 == s2 {
                let tmp = std::mem::replace(l, Label::top());
                *l = Label::or(vec![tmp, padding_labels.clone()]);
            } else {
                let tmp = std::mem::replace(l, Label::top());
                *l = Label::and(vec![tmp, Label::not(padding_labels.clone())]);
            }
        }

        let mut ret = SymbolicAutomaticRelation {
            states: states,
            initial_states: is_set,
            arguments: args,
            repetitions: reps,
            transitions: trans,
            final_states: fs_set,
            context: ctx,
        };

        ret.remove_duplicates();
        ret.trim();

        ret
    }

    fn determinize(sar: &Self) -> Self {
        let mut ctx = Context::new();
        ctx.sar_name = format!("{}", sar.context.sar_name);
        ctx.earg_cnt = sar.context.earg_cnt;
        ctx.larg_cnt = sar.context.larg_cnt;
        ctx.param_cnt = sar.context.param_cnt;
        ctx.repeat_cnt = sar.context.repeat_cnt;

        let mut v = vec![BTreeSet::new()];
        for &s in &sar.states {
            let tmp2 = v.clone();

            for mut hoge in tmp2 {
                hoge.insert(s);
                v.push(hoge);
            }
        }
        let state_map = v
            .into_iter()
            .map(|set| (set, State::new(&mut ctx)))
            .collect::<BTreeMap<_, _>>();

        let states = state_map.values().cloned().collect::<BTreeSet<_>>();
        let is_set = [*state_map.get(&sar.initial_states).unwrap()]
            .iter()
            .map(|s| *s)
            .collect();
        let fs_set = state_map
            .iter()
            .filter(|&(set, _)| !set.is_disjoint(&sar.final_states))
            .map(|(_, s)| *s)
            .collect::<BTreeSet<_>>();
        let args = sar.arguments.clone();
        let reps = sar.repetitions.clone();

        let mut trans = Transitions::new();
        for (set_src, state_src) in &state_map {
            let mut v = vec![];
            for s in set_src {
                if let Some(m) = sar.transitions.get_map(*s) {
                    v.append(
                        &mut m
                            .iter()
                            .map(|(&s, l)| (s, l))
                            .collect::<Vec<(State, &Label)>>(),
                    );
                }
            }

            let mut v2 = vec![(BTreeSet::new(), vec![])];
            for (s, l) in v {
                let tmp = v2.clone();
                for (c, d) in &mut v2 {
                    c.insert(s);
                    d.push(l.clone());
                }
                for (c, mut d) in tmp {
                    d.push(Label::not(l.clone()));
                    v2.push((c, d));
                }
            }

            let mut map = BTreeMap::new();

            for (set_tgt, l_vec) in v2 {
                let state_tgt = *state_map.get(&set_tgt).unwrap();
                map.entry((*state_src, state_tgt))
                    .or_insert(vec![])
                    .push(Label::and(l_vec));
            }

            for ((src, tgt), l_vec) in map {
                trans.insert(src, tgt, Label::or(l_vec));
            }
        }

        SymbolicAutomaticRelation {
            states,
            initial_states: is_set,
            final_states: fs_set,
            arguments: args,
            repetitions: reps,
            transitions: trans,
            context: ctx,
        }
    }

    fn insert_initial_state(&mut self, is: State) {
        self.states.insert(is);
        self.initial_states.insert(is);
    }

    // with inserting states and parameters
    fn insert_transition(&mut self, from: State, to: State, l: Label) {
        self.states.insert(from);
        self.states.insert(to);

        for pid in l.get_all_vars() {
            if !self.arguments.contains_key(&pid) {
                self.arguments.insert(pid, argument::LTerm::nil());
            }
        }

        for rid in l.get_all_rvars() {
            if !self.repetitions.contains_key(&rid) {
                self.repetitions.insert(rid, argument::ETerm::num(0));
            }
        }

        self.transitions.insert(from, to, l);
    }

    fn insert_final_state(&mut self, final_state: State) {
        self.states.insert(final_state);
        self.final_states.insert(final_state);
    }

    // equality of list
    fn eq(lt1: argument::LTerm<Et, Lt>, lt2: argument::LTerm<Et, Lt>) -> Self {
        let mut ctx = Context::new();

        let pid1 = ParamId::new(&mut ctx);
        let pid2 = ParamId::new(&mut ctx);

        let s0 = State::new(&mut ctx);

        let is_set: BTreeSet<State> = [s0].iter().map(|s| *s).collect();
        let fs_set = is_set.clone();

        let mut args = argument::Arguments::new();
        args.insert(pid1, lt1);
        args.insert(pid2, lt2);

        let reps = argument::Repetition::new();

        let mut trans = Transitions::new();
        trans.insert(
            s0,
            s0,
            Label::papp("=", vec![Term::var(pid1), Term::var(pid2)]),
        );

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    pub fn intersection(sar1: &Self, sar2: &Self) -> Self {
        let mut ctx = Context::new();

        let mut state_map = BTreeMap::new();
        let mut is_set = BTreeSet::new();
        let mut fs_set = BTreeSet::new();
        for s1 in &sar1.states {
            for s2 in &sar2.states {
                let new_state = State::new(&mut ctx);
                state_map.insert((*s1, *s2), new_state);

                if sar1.initial_states.contains(&s1) && sar2.initial_states.contains(&s2) {
                    is_set.insert(new_state);
                }

                if sar1.final_states.contains(&s1) && sar2.final_states.contains(&s2) {
                    fs_set.insert(new_state);
                }
            }
        }

        let mut args = argument::Arguments::new();
        let mut param_map1 = BTreeMap::new();
        let mut param_map2 = BTreeMap::new();
        for (pid, lt) in &sar1.arguments {
            let new_pid = ParamId::new(&mut ctx);
            args.insert(new_pid, lt.clone());
            param_map1.insert(*pid, new_pid);
        }
        for (pid, lt) in &sar2.arguments {
            let new_pid = ParamId::new(&mut ctx);
            args.insert(new_pid, lt.clone());
            param_map2.insert(*pid, new_pid);
        }

        let mut reps = argument::Repetition::new();
        let mut rep_map1 = BTreeMap::new();
        let mut rep_map2 = BTreeMap::new();
        for (rid, et) in &sar1.repetitions {
            let new_rid = RepeatId::new(&mut ctx);
            reps.insert(new_rid, et.clone());
            rep_map1.insert(*rid, new_rid);
        }
        for (rid, et) in &sar2.repetitions {
            let new_rid = RepeatId::new(&mut ctx);
            reps.insert(new_rid, et.clone());
            rep_map2.insert(*rid, new_rid);
        }

        let mut transitions1 = sar1.transitions.clone_all();
        for (_, l) in &mut transitions1 {
            l.replace_prid(&param_map1, &rep_map1);
        }

        let mut transitions2 = sar2.transitions.clone_all();
        for (_, l) in &mut transitions2 {
            l.replace_prid(&param_map2, &rep_map2);
        }

        let mut trans = Transitions::new();
        for ((s11, s12), l1) in &transitions1 {
            for ((s21, s22), l2) in &transitions2 {
                let from = state_map.get(&(*s11, *s21)).expect("foo");
                let to = state_map.get(&(*s12, *s22)).expect("foo");
                let l = Label::and(vec![l1.clone(), l2.clone()]);
                trans.insert(*from, *to, l);
            }
        }

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    pub fn union(sar1: &Self, sar2: &Self) -> Self {
        let mut ctx = Context::new();

        let mut state_map = BTreeMap::new();
        let mut is_set = BTreeSet::new();
        let mut fs_set = BTreeSet::new();
        let mut args = argument::Arguments::new();
        let mut reps = argument::Repetition::new();
        let mut trans = Transitions::new();

        for s in &sar1.states {
            let new_state = State::new(&mut ctx);
            state_map.insert(*s, new_state);

            if sar1.initial_states.contains(&s) {
                is_set.insert(new_state);
            }

            if sar1.final_states.contains(&s) {
                fs_set.insert(new_state);
            }
        }
        for s in &sar2.states {
            let new_state = State::new(&mut ctx);
            state_map.insert(*s, new_state);

            if sar2.initial_states.contains(&s) {
                is_set.insert(new_state);
            }

            if sar2.final_states.contains(&s) {
                fs_set.insert(new_state);
            }
        }

        {
            let mut param_map1 = BTreeMap::new();
            for (pid, lt) in &sar1.arguments {
                let new_pid = ParamId::new(&mut ctx);
                args.insert(new_pid, lt.clone());
                param_map1.insert(*pid, new_pid);
            }

            let mut rep_map1 = BTreeMap::new();
            for (rid, et) in &sar1.repetitions {
                let new_rid = RepeatId::new(&mut ctx);
                reps.insert(new_rid, et.clone());
                rep_map1.insert(*rid, new_rid);
            }

            for ((s1, s2), l) in sar1.transitions.get_all() {
                let from = state_map.get(&s1).expect("foo");
                let to = state_map.get(&s2).expect("foo");
                let mut lbl = l.clone();
                lbl.replace_prid(&param_map1, &rep_map1);
                trans.insert(*from, *to, lbl);
            }
        }

        {
            let mut param_map2 = BTreeMap::new();
            for (pid, lt) in &sar2.arguments {
                let new_pid = ParamId::new(&mut ctx);
                args.insert(new_pid, lt.clone());
                param_map2.insert(*pid, new_pid);
            }

            let mut rep_map2 = BTreeMap::new();
            for (rid, et) in &sar2.repetitions {
                let new_rid = RepeatId::new(&mut ctx);
                reps.insert(new_rid, et.clone());
                rep_map2.insert(*rid, new_rid);
            }

            for ((s1, s2), l) in sar2.transitions.get_all() {
                let from = state_map.get(&s1).expect("foo");
                let to = state_map.get(&s2).expect("foo");
                let mut lbl = l.clone();
                lbl.replace_prid(&param_map2, &rep_map2);
                trans.insert(*from, *to, lbl);
            }
        }

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    pub fn complement(sar: &Self) -> Self {
        let mut sar = SymbolicAutomaticRelation::determinize(sar);
        sar.final_states = sar
            .states
            .difference(&sar.final_states)
            .map(|s| *s)
            .collect();
        sar.remove_duplicates();
        sar.trim();
        sar
    }

    // 元のSAR Rをm遷移分展開したSARを作るために，Rのそれぞれの状態に対し新たにm+1個の状態を作る．
    fn make_new_states(&self, m: usize, ctx: &mut Context) -> Vec<BTreeMap<State, State>> {
        let mut new_states = vec![];

        for _ in 0..m + 1 {
            let mut btmap = BTreeMap::new();

            for &s in &self.states {
                btmap.insert(s, State::new(ctx));
            }

            new_states.push(btmap);
        }

        new_states
    }

    // 初期状態から到達不可能な状態を取得
    fn get_unreachable_states(&self) -> BTreeSet<State> {
        let mut tmp = BTreeSet::new();
        for &is in &self.initial_states {
            if !tmp.contains(&is) {
                tmp.insert(is);
                self.transitions.dfs(is, &mut tmp);
            }
        }
        self.states.difference(&tmp).map(|s| *s).collect()
    }

    // 受理状態へ到達不可能な状態を取得
    fn get_empty_states(&self) -> BTreeSet<State> {
        let mut tmp = BTreeSet::new();
        for &fs in &self.final_states {
            if !tmp.contains(&fs) {
                tmp.insert(fs);
                self.transitions.dfs_reverse(fs, &mut tmp);
            }
        }
        self.states.difference(&tmp).map(|s| *s).collect()
    }

    // ある状態とそれに出入りする遷移を消去
    fn remove_state(&mut self, s: State) {
        self.states.remove(&s);
        self.initial_states.remove(&s);
        self.final_states.remove(&s);
        self.transitions.remove_around_state(&s);
    }

    // 初期状態から到達不可能，または受理状態へ到達不可能な状態を消去
    fn trim(&mut self) {
        self.transitions.remove_unnecessary_transitions();

        let set1 = self.get_unreachable_states();
        let set2 = self.get_empty_states();
        let states_to_remove: BTreeSet<_> = set1.union(&set2).map(|a| *a).collect();

        for s in states_to_remove {
            self.remove_state(s);
        }
    }

    fn remove_duplicates(&mut self) {
        // 重複している引数の削除
        let param_map = self.arguments.remove_duplicates();

        let rep_map = self.repetitions.remove_duplicates();

        for (_, l) in self.transitions.get_all_mut() {
            l.replace_prid(&param_map, &rep_map);
        }
    }
}

impl SymbolicAutomaticRelation<String, String> {
    pub fn from_uapp(
        uapp: &rules::UninterpretedApplication,
        pred_map: &BTreeMap<
            String,
            (
                (
                    SymbolicAutomaticRelation<argument::EArgId, argument::LArgId>,
                    BTreeSet<argument::ExistEArgId>,
                    BTreeSet<argument::ExistLArgId>,
                ),
                (
                    SymbolicAutomaticRelation<argument::EArgId, argument::LArgId>,
                    BTreeSet<argument::ExistEArgId>,
                    BTreeSet<argument::ExistLArgId>,
                ),
            ),
        >,
        is_pos: bool,
    ) -> Self {
        let rules::UninterpretedApplication::UApp0(_, up, eterm_vec, lterm_vec, _) = uapp;

        let rules::UninterpretedPredicate::UPred0(s) = up as &rules::UninterpretedPredicate;

        let (tmp, eeaid_set, elaid_set) = if is_pos {
            pred_map.get(s).unwrap().0.clone()
        } else {
            pred_map.get(s).unwrap().1.clone()
        };

        let mut et_map = vec![];
        for et in eterm_vec {
            et_map.push(argument::ETerm::<String, String>::from(&et));
        }

        let mut lt_map = vec![];
        for lt in lterm_vec {
            lt_map.push(argument::LTerm::<String, String>::from(&lt));
        }

        let mut eeaid_map = BTreeMap::new();
        for eeaid in eeaid_set {
            eeaid_map.insert(eeaid, generate_eqevar());
        }

        let mut elaid_map = BTreeMap::new();
        for elaid in elaid_set {
            elaid_map.insert(elaid, generate_eqlvar());
        }

        tmp.substitute(&et_map, &lt_map, &eeaid_map, &elaid_map)
    }

    pub fn from_iformula(iformula: &rules::IFormula) -> Self {
        match iformula {
            rules::IFormula::IFormula0(list_eq) => {
                let rules::ListEquality::LEq(_, _ /* = */, lt1, lt2, _) =
                    list_eq as &rules::ListEquality;

                SymbolicAutomaticRelation::eq(
                    argument::LTerm::<String, String>::from(lt1),
                    argument::LTerm::<String, String>::from(lt2),
                )
            }

            rules::IFormula::IFormula1(_, ps, elemterm_vec, _) => {
                let rules::PredicateSymbol::PredSym0(s) = ps as &rules::PredicateSymbol;

                let mut ctx = Context::new();

                let s0 = State::new(&mut ctx);
                let s1 = State::new(&mut ctx);

                let is_set = [s0].iter().map(|s| *s).collect();
                let fs_set = [s1].iter().map(|s| *s).collect();

                let mut args = argument::Arguments::new();

                let mut t_vec = vec![];
                for elemterm in elemterm_vec {
                    let x = ParamId::new(&mut ctx);

                    let et = argument::ETerm::<String, String>::from(elemterm);

                    args.insert(x, argument::LTerm::cons(et, argument::LTerm::nil()));

                    t_vec.push(Term::var(x));
                }

                let reps = argument::Repetition::new();

                let mut trans = Transitions::new();
                trans.insert(s0, s1, Label::papp(s, t_vec));

                SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
            }
        }
    }

    pub fn from_formula(
        formula: &rules::Formula,
        pred_map: &BTreeMap<
            String,
            (
                (
                    SymbolicAutomaticRelation<argument::EArgId, argument::LArgId>,
                    BTreeSet<argument::ExistEArgId>,
                    BTreeSet<argument::ExistLArgId>,
                ),
                (
                    SymbolicAutomaticRelation<argument::EArgId, argument::LArgId>,
                    BTreeSet<argument::ExistEArgId>,
                    BTreeSet<argument::ExistLArgId>,
                ),
            ),
        >,
        is_pos: bool,
    ) -> Self {
        match formula {
            rules::Formula::Formula0(_, _ /* and */, fml_vec, _) => {
                if is_pos {
                    fml_vec
                        .iter()
                        .map(|fml| SymbolicAutomaticRelation::from_formula(fml, pred_map, true))
                        .fold(SymbolicAutomaticRelation::top(), |sar1, sar2| {
                            SymbolicAutomaticRelation::intersection(&sar1, &sar2)
                        })
                } else {
                    fml_vec
                        .iter()
                        .map(|fml| SymbolicAutomaticRelation::from_formula(fml, pred_map, false))
                        .fold(SymbolicAutomaticRelation::bot(), |sar1, sar2| {
                            SymbolicAutomaticRelation::union(&sar1, &sar2)
                        })
                }
            }

            rules::Formula::Formula1(_, _ /* or */, fml_vec, _) => {
                if is_pos {
                    fml_vec
                        .iter()
                        .map(|fml| SymbolicAutomaticRelation::from_formula(fml, pred_map, true))
                        .fold(SymbolicAutomaticRelation::bot(), |sar1, sar2| {
                            SymbolicAutomaticRelation::union(&sar1, &sar2)
                        })
                } else {
                    fml_vec
                        .iter()
                        .map(|fml| SymbolicAutomaticRelation::from_formula(fml, pred_map, false))
                        .fold(SymbolicAutomaticRelation::top(), |sar1, sar2| {
                            SymbolicAutomaticRelation::intersection(&sar1, &sar2)
                        })
                }
            }

            rules::Formula::Formula2(_, _ /* => */, fml1, fml2, _) => {
                if is_pos {
                    SymbolicAutomaticRelation::union(
                        &SymbolicAutomaticRelation::from_formula(fml1, pred_map, false),
                        &SymbolicAutomaticRelation::from_formula(fml2, pred_map, true),
                    )
                } else {
                    SymbolicAutomaticRelation::intersection(
                        &SymbolicAutomaticRelation::from_formula(fml1, pred_map, true),
                        &SymbolicAutomaticRelation::from_formula(fml2, pred_map, false),
                    )
                }
            }

            rules::Formula::Formula3(_, _ /* not */, fml, _) => {
                SymbolicAutomaticRelation::from_formula(&fml, pred_map, !is_pos)
            }

            rules::Formula::Formula4(u_app) => {
                SymbolicAutomaticRelation::from_uapp(u_app, pred_map, is_pos)
            }

            rules::Formula::Formula5(i_formula) => {
                let tmp = SymbolicAutomaticRelation::from_iformula(i_formula);
                if is_pos {
                    tmp
                } else {
                    SymbolicAutomaticRelation::complement(&tmp)
                }
            }

            rules::Formula::Formula6(_) => {
                if is_pos {
                    SymbolicAutomaticRelation::top()
                } else {
                    SymbolicAutomaticRelation::bot()
                }
            }

            rules::Formula::Formula7(_) => {
                if is_pos {
                    SymbolicAutomaticRelation::bot()
                } else {
                    SymbolicAutomaticRelation::top()
                }
            }
        }
    }

    pub fn visualize(&self) -> String {
        let mut ret = String::new();

        ret += &format!("digraph G {}\n", r"{");

        ret += &format!("    rankdir = LR;\n\n");

        ret += &format!("    node [shape = point]; init;\n");

        for s in &self.final_states {
            ret += &format!("    node [shape = doublecircle]; {};\n", s);
        }

        ret += &format!("    node [shape = circle];\n");

        for s in &self.initial_states {
            ret += &format!("    init -> {};\n", s);
        }

        let mut v = vec![];

        let mut i = 0;
        for ((s1, s2), l) in self.transitions.get_all() {
            ret += &format!(r#"    {} -> {} [ label = "φ{}" ];{}"#, s1, s2, i, "\n");
            v.push((i, l));
            i += 1;
        }

        for (rid, et) in &self.repetitions {
            ret += &format!("    // r{} -> {}\n", rid, et);
        }
        for (pid, lt) in &self.arguments {
            ret += &format!("    // x{} -> {}\n", pid, lt);
        }

        for (i, l) in v {
            ret += &format!("    // φ{} = {}\n", i, l);
        }

        ret += &format!("{}", r"}");

        ret
    }

    // 引数を整理する
    fn arrange(&self) -> Self {
        let mut ctx = Context::new();
        ctx.sar_name = format!("{}_arranged", self.context.sar_name);
        ctx.param_cnt = self.context.param_cnt;
        ctx.repeat_cnt = self.context.repeat_cnt;

        // 引数の全リストのconsの長さの最大値をmとする
        let m = self.arguments.max_len_of_cons();

        // 元の状態ごとに新しい状態の列を管理
        let new_states = self.make_new_states(m, &mut ctx);
        // 初期状態と受理状態を設定
        let mut is_set: BTreeSet<_> = self
            .initial_states
            .iter()
            .map(|s| *new_states[0].get(s).expect("error1"))
            .collect();
        let fs_set: BTreeSet<_> = self
            .final_states
            .iter()
            .map(|s| *new_states[m].get(s).expect("error2"))
            .collect();

        let mut args = self.arguments.clone();
        let mut reps = self.repetitions.clone();

        let mut trans = Transitions::new();

        let mut new_args = argument::Arguments::new();
        let mut new_reps = argument::Repetition::new();
        {
            let mut new_labels = vec![];
            {
                for i in 0..m {
                    let mut ith_transitions = vec![];
                    for ((s1, s2), l) in self.transitions.get_all() {
                        let from = new_states[i].get(&s1).expect("error4");
                        let to = new_states[i + 1].get(&s2).expect("error4");
                        ith_transitions.push(((from, to), vec![l.clone()]));
                    }
                    new_labels.push(ith_transitions);
                }
                let mut mth_transitions = vec![];
                for ((s1, s2), l) in self.transitions.get_all() {
                    let from = new_states[m].get(&s1).expect("error4");
                    let to = new_states[m].get(&s2).expect("error4");
                    mth_transitions.push(((from, to), vec![l.clone()]));
                }
                new_labels.push(mth_transitions);
            }

            for (pid, lt) in &mut args {
                if let argument::LTerm::LVar(_, _) = lt {
                    continue;
                }

                let mut i = 0;
                while let argument::LTerm::Cons(et2, lt2) = lt {
                    let mut tmp_args = argument::Arguments::new();

                    let term_from_at =
                        Term::from_arg_term(et2, &mut ctx, &mut tmp_args, &mut new_reps);

                    for (_, l_vec) in &mut new_labels[i] {
                        l_vec.push(Label::papp(
                            "=",
                            vec![Term::var(*pid), term_from_at.clone()],
                        ));
                    }

                    // ltをlt2で置き換え
                    *lt = std::mem::replace(
                        lt2 as &mut argument::LTerm<_, _>,
                        argument::LTerm::nil(),
                    );

                    tmp_args.shift(-(i as isize));
                    new_args.merge(tmp_args);

                    i += 1;
                }

                match lt {
                    argument::LTerm::Nil => {
                        while i <= m {
                            for (_, l_vec) in &mut new_labels[i] {
                                l_vec.push(Label::pad(*pid));
                            }

                            i += 1;
                        }

                        *lt = argument::LTerm::lvar(generate_lvariable(), 0);
                    }

                    argument::LTerm::LVar(laid, shift) => {
                        let new_shift = *shift - (i as isize);
                        let new_pid = ParamId::new(&mut ctx);

                        while i <= m {
                            for (_, l_vec) in &mut new_labels[i] {
                                l_vec.push(Label::or(vec![
                                    Label::papp("=", vec![Term::var(*pid), Term::var(new_pid)]),
                                    Label::and(vec![Label::pad(*pid), Label::pad(new_pid)]),
                                ]));
                            }

                            i += 1;
                        }

                        new_args.insert(
                            new_pid,
                            argument::LTerm::lvar(String::from(laid.as_str()), new_shift),
                        );

                        *lt = argument::LTerm::lvar(generate_lvariable(), 0);
                    }

                    argument::LTerm::ELVar(_, _) => panic!(""),

                    argument::LTerm::Cons(_, _) => panic!("impossible."),
                }
            }

            for ith_transitions in new_labels {
                for ((s1, s2), mut l_vec) in ith_transitions {
                    if l_vec.len() > 1 {
                        trans.insert(*s1, *s2, Label::and(l_vec));
                    } else {
                        let l = l_vec.pop().unwrap();
                        trans.insert(*s1, *s2, l);
                    }
                }
            }
        }

        {
            // repeatについて

            let mut new_labels = vec![];

            for (rid, et) in &mut reps {
                match et {
                    argument::ETerm::Hd(_)
                    | argument::ETerm::Fapp(_, _)
                    | argument::ETerm::Num(_) => {
                        // l中のxをtで置き換え，t中の変数vに対しall(v)を新たな引数として加える．ltをlt2とする
                        let term_from_at =
                            Term::from_arg_term(et, &mut ctx, &mut new_args, &mut new_reps);

                        // 初期状態から出る遷移に追加するための (pid=term_from_at) を用意
                        new_labels.push(Label::papp("=", vec![Term::rvar(*rid), term_from_at]));

                        // ltをrepeat(v)で置き換え（vは新しい変数）
                        *et = argument::ETerm::var(generate_evariable());
                    }

                    _ => {}
                }
            }

            // // そもそも開始状態に受理状態があれば空ではないのでCHCの出力は不必要
            // if !is_set
            //     .intersection(&fs_set)
            //     .map(|s| *s)
            //     .collect::<BTreeSet<_>>()
            //     .is_empty()
            // {
            //     panic!("");
            // }

            if !new_labels.is_empty() {
                let s0 = State::new(&mut ctx);
                let mut tr_vec = vec![];
                for &is in &is_set {
                    for (&s, l) in trans.get_map(is).unwrap() {
                        let mut tmp = new_labels.clone();
                        tmp.push(l.clone());
                        tr_vec.push((s0, s, Label::and(tmp)));
                    }
                }
                for (from, to, l) in tr_vec {
                    trans.insert(from, to, l);
                }
                is_set = [s0].iter().map(|s| *s).collect();
            }
        }

        // 新しい引数の追加
        args.merge(new_args);
        reps.merge(new_reps);

        SymbolicAutomaticRelation::from(is_set, fs_set, args, reps, trans, ctx)
    }

    fn to_chc(&mut self, s: &mut String) {
        let pid_of_repeat_vec = self.repetitions.params();
        let pid_of_lvar_vec_vec = self.arguments.fillup_args(&mut self.context);
        let no_args = pid_of_repeat_vec.is_empty()
            && pid_of_lvar_vec_vec
                .iter()
                .map(|v| v.is_empty())
                .fold(true, |b1, b2| b1 && b2);

        *s += "(set-logic HORN)\n\n";

        for state in &self.states {
            *s += &format!("(declare-fun {}", state);
            *s += " ( ";
            for _ in &pid_of_repeat_vec {
                *s += "Int ";
            }
            for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                for _ in 0..pid_of_lvar_vec.len() {
                    *s += "Int Bool ";
                }
            }
            *s += ") Bool)\n";
        }
        *s += "\n";

        // 初期状態に入るホーン節
        for &is in &self.initial_states {
            *s += "(assert (forall ( ";
            if no_args {
                *s += &format!("(dummy Int) ");
            } else {
                for &rid in &pid_of_repeat_vec {
                    *s += &format!("(r{}_value Int) ", rid);
                }
                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    for pid in pid_of_lvar_vec {
                        *s += &format!(
                            "(x{}_value Int) (x{}_bool Bool) (y{}_value Int) (y{}_bool Bool) ",
                            pid, pid, pid, pid
                        );
                    }
                }
            }
            *s += ") ";
            {
                *s += "(=> ";

                // body
                {
                    *s += "(and true";

                    for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                        for i in 1..pid_of_lvar_vec.len() {
                            let pid1 = pid_of_lvar_vec[i - 1];
                            let pid2 = pid_of_lvar_vec[i];
                            *s += &format!(" (=> (not y{}_bool) (not y{}_bool))", pid1, pid2);
                        }
                    }

                    *s += ")";
                }

                *s += " ";

                // head
                {
                    if no_args {
                        *s += &format!("{}", is);
                    } else {
                        *s += &format!("({}", is);
                        for pid in &pid_of_repeat_vec {
                            *s += &format!(" r{}_value", pid);
                        }
                        for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                            for i in 0..pid_of_lvar_vec.len() {
                                let pid = pid_of_lvar_vec[i];
                                *s += &format!(" y{}_value y{}_bool", pid, pid);
                            }
                        }
                        *s += ")";
                    }
                }

                *s += ")";
            }
            *s += "))\n";
        }

        // 各遷移に対応するホーン節
        for ((s1, s2), l) in self.transitions.get_all() {
            {
                // 遷移があり得るか確認

                let cfg = z3::Config::new();
                let ctx = z3::Context::new(&cfg);

                let mut v = vec![l.to_ast(&ctx)];

                let mut tmp1 = vec![z3::ast::Bool::from_bool(&ctx, false)];

                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    tmp1.push(z3::ast::Bool::new_const(
                        &ctx,
                        z3::Symbol::String(format!("x{}_b", pid_of_lvar_vec[0])),
                    ));
                }

                let tmp2: Vec<&z3::ast::Bool> = tmp1.iter().collect();
                v.push(z3::ast::Bool::or(&ctx, &tmp2));

                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    if pid_of_lvar_vec.len() > 1 {
                        let pid1 = pid_of_lvar_vec[pid_of_lvar_vec.len() - 2];
                        let pid2 = pid_of_lvar_vec[pid_of_lvar_vec.len() - 1];

                        let b1 = z3::ast::Bool::new_const(
                            &ctx,
                            z3::Symbol::String(format!("x{}_b", pid1)),
                        );
                        let b2 = z3::ast::Bool::new_const(
                            &ctx,
                            z3::Symbol::String(format!("x{}_b", pid2)),
                        );

                        v.push(b1.not().implies(&b2.not()));
                    }
                }

                let a: Vec<&z3::ast::Bool> = v.iter().collect();
                let b = z3::ast::Bool::and(&ctx, &a);

                let solver = z3::Solver::new(&ctx);
                solver.assert(&b);

                if let z3::SatResult::Sat = solver.check() {
                    // println!("{} -> {} : {}", s1, s2, b);
                } else {
                    continue;
                }
            }

            *s += "(assert (forall ( ";
            if no_args {
                *s += &format!("(dummy Int) ");
            } else {
                for &rid in &pid_of_repeat_vec {
                    *s += &format!("(r{}_value Int) ", rid);
                }
                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    for pid in pid_of_lvar_vec {
                        *s += &format!(
                            "(x{}_value Int) (x{}_bool Bool) (y{}_value Int) (y{}_bool Bool) ",
                            pid, pid, pid, pid
                        );
                    }
                }
            }
            *s += ") ";
            {
                *s += "(=> ";

                // body
                {
                    *s += &format!("(and ");
                    if no_args {
                        *s += &format!("{} ", s1);
                    } else {
                        *s += &format!("({}", s1);
                        for pid in &pid_of_repeat_vec {
                            *s += &format!(" r{}_value", pid);
                        }
                        for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                            for pid in pid_of_lvar_vec {
                                *s += &format!(" x{}_value x{}_bool", pid, pid);
                            }
                        }
                        *s += ") ";
                    }
                    l.to_chc(s);
                    *s += " (or false";
                    for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                        for pid in pid_of_lvar_vec {
                            *s += &format!(" x{}_bool", pid);
                        }
                    }
                    *s += ")";
                    for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                        for pid in pid_of_lvar_vec {
                            *s += &format!(" (=> (not x{}_bool) (not y{}_bool))", pid, pid);
                        }
                    }
                    for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                        for i in 1..pid_of_lvar_vec.len() {
                            let pid1 = pid_of_lvar_vec[i - 1];
                            let pid2 = pid_of_lvar_vec[i];
                            *s += &format!(" (= y{}_value x{}_value)", pid1, pid2);
                            *s += &format!(" (= y{}_bool x{}_bool)", pid1, pid2);
                        }
                    }
                    *s += ")";
                }

                *s += " ";

                // head
                {
                    if no_args {
                        *s += &format!("{}", s2);
                    } else {
                        *s += &format!("({}", s2);
                        for pid in &pid_of_repeat_vec {
                            *s += &format!(" r{}_value", pid);
                        }
                        for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                            for i in 0..pid_of_lvar_vec.len() {
                                let pid = pid_of_lvar_vec[i];
                                *s += &format!(" y{}_value y{}_bool", pid, pid);
                            }
                        }
                        *s += ")";
                    }
                }

                *s += ")";
            }
            *s += "))\n";
        }

        // 受理状態に対応するホーン節
        for &s1 in &self.final_states {
            *s += "(assert (forall ( ";
            if no_args {
                *s += &format!("(dummy Int) ");
            } else {
                for &rid in &pid_of_repeat_vec {
                    *s += &format!("(r{}_value Int) ", rid);
                }
                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    for pid in pid_of_lvar_vec {
                        *s += &format!(
                            "(x{}_value Int) (x{}_bool Bool) (y{}_value Int) (y{}_bool Bool) ",
                            pid, pid, pid, pid
                        );
                    }
                }
            }
            *s += ") ";
            {
                *s += "(=> ";

                // body
                *s += &format!("(and ");
                if no_args {
                    *s += &format!("{}", s1);
                } else {
                    *s += &format!("({}", s1);
                    for pid in &pid_of_repeat_vec {
                        *s += &format!(" r{}_value", pid);
                    }
                    for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                        for pid in pid_of_lvar_vec {
                            *s += &format!(" x{}_value x{}_bool", pid, pid);
                        }
                    }
                    *s += ")";
                }
                for pid_of_lvar_vec in &pid_of_lvar_vec_vec {
                    for pid in pid_of_lvar_vec {
                        *s += &format!(" (not x{}_bool)", pid);
                    }
                }
                *s += ")";

                *s += " ";

                //head
                *s += "false";

                *s += ")";
            }
            *s += "))\n";
        }

        *s += "\n";
        *s += "(check-sat)\n";
    }

    pub fn generate_chc(&self) -> String {
        let mut sar = self.arrange();
        sar.trim();
        let mut s = String::new();
        sar.to_chc(&mut s);
        s
    }
}

impl SymbolicAutomaticRelation<argument::EArgId, argument::LArgId> {
    pub fn substitute(
        &self,
        et_map: &Vec<argument::ETerm<String, String>>,
        lt_map: &Vec<argument::LTerm<String, String>>,
        eeaid_map: &BTreeMap<argument::ExistEArgId, std::string::String>,
        elaid_map: &BTreeMap<argument::ExistLArgId, std::string::String>,
    ) -> SymbolicAutomaticRelation<String, String> {
        let SymbolicAutomaticRelation {
            initial_states,
            states,
            final_states,
            arguments,
            transitions,
            repetitions,
            context,
        } = self;

        if context.earg_cnt != et_map.len() || context.larg_cnt != lt_map.len() {
            panic!("invalid arguments.");
        }

        let mut new_ctx = Context::new();
        new_ctx.sar_name = format!("{}_substituted", context.sar_name);
        new_ctx.param_cnt = context.param_cnt;
        new_ctx.repeat_cnt = context.repeat_cnt;

        SymbolicAutomaticRelation {
            states: states.clone(),
            initial_states: initial_states.clone(),
            final_states: final_states.clone(),
            arguments: arguments.substitute(et_map, lt_map, eeaid_map, elaid_map),
            repetitions: repetitions.substitute(et_map, lt_map, eeaid_map, elaid_map),
            transitions: transitions.clone(),
            context: new_ctx,
        }
    }
}

// #[test]
pub fn test() {}
